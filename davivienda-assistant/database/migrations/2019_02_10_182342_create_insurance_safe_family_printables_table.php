<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceSafeFamilyPrintablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('insurance_safe_family_printables', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('random', 150);
	        $table->string('identifier');
	        $table->longText('customer_information')->nullable()->default(null);
	        $table->longText('product_information')->nullable()->default(null);
	        $table->longText('user_information')->nullable()->default(null);
	        $table->longText('uploads')->nullable()->default(null);
	        $table->longText('audit')->nullable();
	        $table->integer('version')->nullable()->default(1);
	        $table->unsignedInteger('customer_id');
	        $table->foreign('customer_id')->references('id')->on('customers');
	        $table->unsignedInteger('productable_id')->nullable()->default(null);
	        $table->string('productable_type')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('insurance_safe_family_printables');
    }
}
