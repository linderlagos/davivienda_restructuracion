<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('customers', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('identifier')->unique();
	        $table->string('cif')->nullable();
	        $table->string('name')->nullable();
	        $table->string('birth')->nullable();
            $table->timestamps();
        });
    }

    /**
     *
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('customers');
    }
}
