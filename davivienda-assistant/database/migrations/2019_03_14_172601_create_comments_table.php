<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('comments', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('title')->nullable()->default(null);
	        $table->string('description');
	        $table->integer('commentable_id');
	        $table->string('commentable_type');
	        $table->unsignedInteger('created_by');
	        $table->unsignedInteger('solved_by')->nullable()->default(null);
	        $table->integer('solved')->nullable()->default(0);
	        $table->unsignedInteger('parent_id')->nullable()->default(null);
	        $table->foreign('parent_id')->references('id')->on('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('comments');
    }
}
