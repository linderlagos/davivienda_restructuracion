<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('products', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('identifier');
	        $table->string('random', 150);
	        $table->longText('customer_information')->nullable()->default(null);
	        $table->longText('product_information')->nullable()->default(null);
	        $table->longText('user_information')->nullable()->default(null);
	        $table->integer('version')->nullable()->default(1);
	        $table->unsignedInteger('customer_id');
	        $table->foreign('customer_id')->references('id')->on('customers');
	        $table->unsignedInteger('parent_id')->nullable()->default(null);
	        $table->foreign('parent_id')->references('id')->on('products');
	        $table->string('type', 100)->nullable()->default(null);
	        $table->string('company', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('products');
    }
}
