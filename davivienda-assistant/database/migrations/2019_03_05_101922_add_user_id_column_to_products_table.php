<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\DB;

class AddUserIdColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->table('products', function (Blueprint $table) {
	        $db = DB::connection('users')->getDatabaseName();


	        $table->unsignedInteger('user_id')->nullable()->after('customer_id');
//	        $table->foreign('user_id')->references('id')->on(new Expression($db . '.users'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->table('products', function (Blueprint $table) {
	        $table->dropColumn(['user_id']);
        });
    }
}
