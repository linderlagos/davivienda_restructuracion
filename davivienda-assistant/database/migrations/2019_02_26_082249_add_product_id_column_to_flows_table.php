<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductIdColumnToFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->table('flows', function (Blueprint $table) {
	        $table->unsignedInteger('product_id')->nullable()->default(null);
	        $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->table('flows', function (Blueprint $table) {
        	$table->dropColumn('product_id');
        });
    }
}
