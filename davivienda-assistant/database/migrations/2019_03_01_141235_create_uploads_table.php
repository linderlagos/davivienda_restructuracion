<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->text('url');
	        $table->string('type', 100)->nullable()->default(null);
	        $table->unsignedInteger('product_id')->nullable()->default(null);
	        $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('uploads');
    }
}
