<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('flows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier', 150)->nullable();
	        $table->longText('customer_information')->nullable();
	        $table->longText('product_information')->nullable();
	        $table->longText('user_information')->nullable();
	        $table->unsignedInteger('customer_id');
	        $table->foreign('customer_id')->references('id')->on('customers');
	        $table->string('type', 100)->nullable()->default(null);
	        $table->string('company', 100)->nullable()->default(null);
	        $table->string('step', 100)->nullable()->default(null);
	        $table->longText('error')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('flows');
    }
}
