<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdentifierToBankAutoQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->table('bank_auto_quotations', function (Blueprint $table) {
	        $table->string('identifier', 150)->nullable()->after('correlative');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->table('bank_auto_quotations', function (Blueprint $table) {
            $table->dropColumn('identifier');
        });
    }
}
