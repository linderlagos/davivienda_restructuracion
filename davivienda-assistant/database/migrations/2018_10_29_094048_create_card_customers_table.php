<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('card_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_name', 150)->nullable()->default(null);
            $table->string('card_code', 30)->nullable()->default(null);
            $table->string('id_type', 30)->nullable()->default(null);
            $table->string('identity', 30)->nullable()->default(null);
            $table->string('day', 20)->nullable()->default(null);
            $table->string('month', 30)->nullable()->default(null);
            $table->string('year', 10)->nullable()->default(null);
            $table->string('age', 10)->nullable()->default(null);
            $table->string('first_name', 50)->nullable()->default(null);
            $table->string('middle_name', 50)->nullable()->default(null);
            $table->string('last_name', 50)->nullable()->default(null);
            $table->string('second_last_name', 50)->nullable()->default(null);
            $table->string('phone', 20)->nullable()->default(null);
            $table->string('mobile', 20)->nullable()->default(null);
            $table->string('job', 100)->nullable()->default(null);
            $table->string('job_type', 30)->nullable()->default(null);
            $table->string('job_status', 30)->nullable()->default(null);
            $table->string('marital_status', 30)->nullable()->default(null);
            $table->string('income', 50)->nullable()->default(null);
            $table->string('email', 100)->nullable()->default(null);
            $table->string('address', 250)->nullable()->default(null);
            $table->string('city', 250)->nullable()->default(null);
            $table->string('state', 50)->nullable()->default(null);
            $table->string('municipality', 50)->nullable()->default(null);
            $table->string('colony', 50)->nullable()->default(null);
            $table->string('nationality', 30)->nullable()->default(null);
            $table->string('gender', 30)->nullable()->default(null);
            $table->string('receive_sms', 20)->nullable()->default(null);
            $table->string('receive_emails', 20)->nullable()->default(null);
            $table->string('bureau', 30)->nullable()->default(null);
            $table->string('employer_name', 50)->nullable()->default(null);
            $table->string('employer_type', 20)->nullable()->default(null);
            $table->string('employer_phone', 20)->nullable()->default(null);
            $table->string('employer_day', 20)->nullable()->default(null);
            $table->string('employer_month', 20)->nullable()->default(null);
            $table->string('employer_year', 20)->nullable()->default(null);
            $table->string('employer_seniority', 30)->nullable()->default(null);
            $table->string('employer_address', 250)->nullable()->default(null);
            $table->string('employer_city', 250)->nullable()->default(null);
            $table->string('employer_state', 50)->nullable()->default(null);
            $table->string('employer_municipality', 50)->nullable()->default(null);
            $table->string('employer_colony', 50)->nullable()->default(null);
            $table->string('terms', 20)->nullable()->default(null);
            $table->string('status', 30)->nullable()->default(null);
            $table->string('card_limit', 50)->nullable()->default(null);
            $table->string('card_type', 50)->nullable()->default(null);
            $table->string('card_request', 50)->nullable()->default(null);
            $table->string('spouse', 50)->nullable()->default(null);
            $table->string('profession', 10)->nullable()->default(null);
            $table->string('job_name', 50)->nullable()->default(null);
            $table->string('public_job', 10)->nullable()->default(null);
            $table->string('public_job_employer_name', 50)->nullable()->default(null);
            $table->string('public_job_name', 40)->nullable()->default(null);
            $table->string('public_job_active', 10)->nullable()->default(null);
            $table->string('public_job_from', 10)->nullable()->default(null);
            $table->string('public_job_to', 10)->nullable()->default(null);
            $table->string('dependants', 10)->nullable()->default(null);
            $table->string('dependant_name_1', 50)->nullable()->default(null);
            $table->string('dependant_name_2', 50)->nullable()->default(null);
            $table->string('dependant_name_3', 50)->nullable()->default(null);
            $table->string('dependant_name_4', 50)->nullable()->default(null);
            $table->string('dependant_name_5', 50)->nullable()->default(null);
            $table->string('dependant_name_6', 50)->nullable()->default(null);
            $table->string('assets', 10)->nullable()->default(null);
            $table->string('passive', 10)->nullable()->default(null);
            $table->string('expenses', 10)->nullable()->default(null);
            $table->string('other_income', 10)->nullable()->default(null);
            $table->string('reference_1_name', 50)->nullable()->default(null);
            $table->string('reference_1_type', 10)->nullable()->default(null);
            $table->string('reference_1_relationship', 10)->nullable()->default(null);
            $table->string('reference_1_mobile', 20)->nullable()->default(null);
            $table->string('reference_1_phone', 20)->nullable()->default(null);
            $table->string('reference_1_work_phone', 20)->nullable()->default(null);
            $table->string('reference_1_address', 250)->nullable()->default(null);
            $table->string('reference_2_name', 50)->nullable()->default(null);
            $table->string('reference_2_type', 10)->nullable()->default(null);
            $table->string('reference_2_relationship', 10)->nullable()->default(null);
            $table->string('reference_2_mobile', 20)->nullable()->default(null);
            $table->string('reference_2_phone', 20)->nullable()->default(null);
            $table->string('reference_2_work_phone', 20)->nullable()->default(null);
            $table->string('reference_2_address', 250)->nullable()->default(null);
            $table->string('peoplesoft', 20)->after('reference_2_address')->nullable()->default(null);
            $table->string('error', 20)->after('reference_2_address')->nullable()->default(null);
            $table->string('step', 50)->after('reference_2_address')->nullable()->default(null);
            $table->string('promotion', 20)->after('nationality')->nullable()->default(null);
            $table->string('customer_limit', 50)->after('card_limit')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('card_customers');
    }
}
