<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('account_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_type', 30)->nullable()->default(null);
            $table->string('identity', 30)->nullable()->default(null);
            $table->string('day', 20)->nullable()->default(null);
            $table->string('month', 30)->nullable()->default(null);
            $table->string('year', 10)->nullable()->default(null);
            $table->string('age', 10)->nullable()->default(null);
            $table->string('first_name', 50)->nullable()->default(null);
            $table->string('middle_name', 50)->nullable()->default(null);
            $table->string('last_name', 50)->nullable()->default(null);
            $table->string('second_last_name', 50)->nullable()->default(null);
            $table->string('phone', 20)->nullable()->default(null);
            $table->string('mobile', 20)->nullable()->default(null);
            $table->string('job', 100)->nullable()->default(null);
            $table->string('job_type', 30)->nullable()->default(null);
            $table->string('job_status', 30)->nullable()->default(null);
            $table->string('marital_status', 30)->nullable()->default(null);
            $table->string('income', 50)->nullable()->default(null);
            $table->string('email', 100)->nullable()->default(null);
            $table->string('address', 250)->nullable()->default(null);
            $table->string('city', 250)->nullable()->default(null);
            $table->string('state', 50)->nullable()->default(null);
            $table->string('municipality', 50)->nullable()->default(null);
            $table->string('colony', 50)->nullable()->default(null);
            $table->string('nationality', 30)->nullable()->default(null);
            $table->string('gender', 30)->nullable()->default(null);
            $table->string('receive_sms', 20)->nullable()->default(null);
            $table->string('receive_emails', 20)->nullable()->default(null);
            $table->string('bureau', 30)->nullable()->default(null);
            $table->string('employer_name', 50)->nullable()->default(null);
            $table->string('employer_type', 20)->nullable()->default(null);
            $table->string('employer_phone', 20)->nullable()->default(null);
            $table->string('employer_day', 20)->nullable()->default(null);
            $table->string('employer_month', 20)->nullable()->default(null);
            $table->string('employer_year', 20)->nullable()->default(null);
            $table->string('employer_seniority', 30)->nullable()->default(null);
            $table->string('employer_address', 250)->nullable()->default(null);
            $table->string('employer_city', 250)->nullable()->default(null);
            $table->string('employer_state', 50)->nullable()->default(null);
            $table->string('employer_municipality', 50)->nullable()->default(null);
            $table->string('employer_colony', 50)->nullable()->default(null);
            $table->string('terms', 20)->nullable()->default(null);
            $table->string('status', 30)->nullable()->default(null);
            $table->string('spouse', 50)->nullable()->default(null);
            $table->string('profession', 10)->nullable()->default(null);
            $table->string('job_name', 50)->nullable()->default(null);
            $table->string('public_job', 10)->nullable()->default(null);
            $table->string('public_job_employer_name', 50)->nullable()->default(null);
            $table->string('public_job_name', 40)->nullable()->default(null);
            $table->string('public_job_active', 10)->nullable()->default(null);
            $table->string('public_job_from', 10)->nullable()->default(null);
            $table->string('public_job_to', 10)->nullable()->default(null);
	        $table->string('assets', 10)->nullable()->default(null);
	        $table->string('passive', 10)->nullable()->default(null);
	        $table->string('expenses', 10)->nullable()->default(null);
	        $table->string('other_income', 10)->nullable()->default(null);
	        $table->text('references')->nullable()->default(null);
	        $table->text('dependants')->nullable()->default(null);
	        $table->text('professional_dependants')->nullable()->default(null);
	        $table->text('beneficiaries')->nullable()->default(null);
	        $table->text('national_institutions')->nullable()->default(null);
            $table->text('foreign_institutions')->nullable()->default(null);
            $table->text('international_operations_information')->nullable()->default(null);
            $table->string('account_information', 100)->nullable()->default(null);
            $table->string('card_holder', 100)->nullable()->default(null);
            $table->string('cif', 30)->nullable()->default(null);
            $table->string('cif_type', 10)->nullable()->default(null);
            $table->string('fatca', 10)->nullable()->default(null);
            $table->string('peoplesoft', 20)->nullable()->default(null);
            $table->string('error', 20)->nullable()->default(null);
            $table->string('step', 50)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('account_customers');
    }
}
