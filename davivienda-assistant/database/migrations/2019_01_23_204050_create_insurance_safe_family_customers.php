<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceSafeFamilyCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('insurance_safe_family_customers', function (Blueprint $table) {
            $table->increments('id');
//	        $table->string('account_id', 150);
//	        $table->string('random', 150);
	        $table->string('identifier');
	        $table->longText('customer_information')->nullable();
	        $table->longText('product_information')->nullable();
	        $table->longText('user_information')->nullable();
	        $table->longText('audit')->nullable();
	        $table->timestamp('approved_at')->nullable()->default(null);
//	        $table->integer('version')->nullable()->default(1);
	        $table->unsignedInteger('customer_id');
	        $table->foreign('customer_id')->references('id')->on('customers');
	        $table->unsignedInteger('productable_id')->nullable()->default(null);
	        $table->string('productable_type')->nullable()->default(null);
	        $table->integer('status')->nullable()->default(0);
	        $table->text('error')->nullable()->default(null);
	        $table->string('step', 50)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('insurance_safe_family_customers');
    }
}
