<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeoplesoftColumnToPrintablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->table('printables', function (Blueprint $table) {
            $table->string('peoplesoft', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->table('printables', function (Blueprint $table) {
            $table->dropColumn(['peoplesoft']);
        });
    }
}
