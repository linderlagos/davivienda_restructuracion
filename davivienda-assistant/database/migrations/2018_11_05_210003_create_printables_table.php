<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('printables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier');
            $table->string('product_identifier')->nullable();
            $table->string('random', 100);
            $table->text('fields');
            $table->integer('version')->unsigned();
            $table->string('product', '100');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('printables');
    }
}
