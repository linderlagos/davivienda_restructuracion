<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('customers')->create('bank_auto_quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correlative')->nullable()->default(null);
            $table->longText('customer_information')->nullable()->default(null);
            $table->longText('product_information')->nullable()->default(null);
            $table->longText('user_information')->nullable()->default(null);
            $table->unsignedInteger('flow_id')->nullable()->default(null);
            $table->unsignedInteger('product_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('customers')->dropIfExists('bank_auto_quotations');
    }
}
