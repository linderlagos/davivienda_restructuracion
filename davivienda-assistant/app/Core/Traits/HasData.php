<?php

namespace App\Core\Traits;

// Core
use App\Core\Data;

/**
 * Trait to check if parameter has data and
 * return correspondant web service structure
 *
 * Trait HasData
 * @package App\Core\Traits
 */
trait HasData
{
	/**
	 * Build Data object if it has value
	 *
	 * @param $field
	 * @param null $parameter
	 * @param null $dataList
	 * @return Data
	 */
	public function hasData($field, $parameter = null, $dataList = null)
	{
		if (empty($parameter) || $parameter === 'NULO') {
			return new Data(false, $field);
		}

		return new Data(true, $field, $dataList, $parameter);
	}
}