<?php

namespace App\Core\Traits;

// Core
use App\Core\Data;

// Helpers
use Illuminate\Support\Facades\Auth;

/**
 * Trait to recover peoplesoft from user and
 * generate webservice structure
 *
 * Trait Peoplesoft
 * @package App\Core\Traits
 */
trait Peoplesoft
{
	/**
	 * Return the PeopleSoft
	 *
	 * @return Data
	 */
	protected function peopleSoft() : Data
	{
		if (Auth::user()->peopleSoft === '504900025')
		{
			return new Data(true, 'PeopleSoft', null, '43478702');
		}

		return new Data(true, 'PeopleSoft', null, Auth::user()->peopleSoft);
	}
}