<?php

namespace App\Core\Traits;

/**
 * Trait to put flow id on session
 *
 * Trait PersistId
 * @package App\Core\Traits
 */
trait ForgetId
{
	/**
	 * Persist product flow id
	 *
	 * @param $productName
	 * @return bool
	 */
	protected function forgetId($productName)
	{
		session()->forget([
			$productName . '_id'
		]);

		return true;
	}
}