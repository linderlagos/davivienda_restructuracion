<?php

namespace App\Core\Traits;

/**
 * Get or creates customer
 *
 * Trait UpdateStep
 * @package App\Core\Traits
 */
trait SearchValueFromLists
{
	use GetLists;

	/**
	 * Get or create customer by identifier
	 *
	 * @param $productName
	 * @param $code
	 * @param $list
	 * @param $array
	 * @return mixed
	 */
	protected function searchValueFromList($productName, $code, $list, $array = true)
	{
		if($code)
		{
			if ($list === 'confirmation')
			{
				$value = $this->confirmation($code);

				return $this->constructResponse($code, $value, $array);
			}

			if ($productName)
			{
				$lists = $this->getLists($productName);

				$value = get_json($lists, [$list, $code]);
			} else {
				$value = null;
			}

			return $this->constructResponse($code, $value, $array);
		} else {
			return $this->constructResponse($code, null, $array);
		}
	}

	private function constructResponse($code, $value, $array)
	{
		if ($array)
		{
			return [
				'code' => $code,
				'value' => $value
			];
		} else {
			return $value;
		}
	}

	private function confirmation($code)
	{
		$list = [
			'N' => 'No',
			'S' => 'Si'
		];

		return $list[$code];
	}
}