<?php

namespace App\Core\Traits;

/**
 * Trait to update flow step
 *
 * Trait UpdateStep
 * @package App\Core\Traits
 */
trait UpdateStep
{
	/**
	 * @param $flow
	 * @param $action
	 * @param $default
	 * @return bool
	 */
	protected function updateStep($flow, $action, $default = null)
	{
		$flowStep = ((int) $flow->step);

		if ($default)
		{
			$step = $this->update($action, $flowStep, $default);

		} else {
			$step = $this->update($action, $flowStep, 1);
		}

		$flow->update([
			'step' =>$step
		]);

		return true;
	}

	private function update($action, $flowStep, $number)
	{
		if ($action === 'next')
		{
			$step = $flowStep + $number;
		} else {
			$step = $flowStep - $number;
		}

		return $step;
	}

}