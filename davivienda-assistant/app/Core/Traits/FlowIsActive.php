<?php

namespace App\Core\Traits;

/**
 * Check if session has an active flow
 *
 * Trait RecentFlow
 * @package App\Core\Traits
 */
trait FlowIsActive
{
	/**
	 * Check if session has a flow id
	 *
	 * @param $products
	 * @return bool
	 */
	function flowIsActive($products)
	{
		if (is_array($products))
		{
			foreach ($products as $product)
			{
				if (session()->has($product . '_id'))
				{
					$this->message();

					return $product;
				}
			}

			return false;
		} else {
			if (session()->has($products . '_id'))
			{
				$this->message();

				return $products;
			}
		}

		return false;
	}

	/**
	 * Create flash error message
	 *
	 * @return mixed
	 */
	private function message()
	{
		return $this->errorMessage('<p>Ya tiene una solicitud en trámite, favor finalice la solicitud actual antes de crear una nueva</p>');
	}
}