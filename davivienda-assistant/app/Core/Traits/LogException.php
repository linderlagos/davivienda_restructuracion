<?php

namespace App\Core\Traits;

// Helpers
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Trait to log exception on Bugsnag
 *
 * Trait LogException
 * @package App\Core\Traits
 */
trait LogException
{
	/**
	 * Log exception on Bugsnag
	 *
	 * @param $e
	 * @return bool
	 */
	protected function logException($e)
	{
		Bugsnag::notifyException($e);

		return true;
	}
}