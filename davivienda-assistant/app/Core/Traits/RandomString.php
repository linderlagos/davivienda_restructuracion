<?php

namespace App\Core\Traits;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Class RandomString
 * @package App\Core
 */
trait RandomString
{
	/**
	 * Generate a random string, using a cryptographically secure
	 * pseudorandom number generator (random_int)
	 *
	 * For PHP 7, random_int is a PHP core function
	 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
	 *
	 * @param int $length      How many characters do we want?
	 * @param string $keyspace A string of all possible characters
	 *                         to select from
	 *
	 * @return string
	 */
	public function generateRandomString($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$pieces = [];

		$max = mb_strlen($keyspace, '8bit') - 1;

		for ($i = 0; $i < $length; ++$i)
		{
			try {
				$pieces []= $keyspace[random_int(0, $max)];
			} catch (\Exception $e)
			{
				Bugsnag::notifiyException($e);

				return abort(500);
			}
		}

		return implode('', $pieces);
	}
}
