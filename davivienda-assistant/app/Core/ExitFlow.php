<?php

namespace App\Core;

use App\Core\Traits\Bridge;
use App\Core\Traits\ForgetId;
use App\Core\Traits\HasData;
use App\Core\Traits\LogException;
use App\Core\Traits\Message;
use App\Core\Traits\Peoplesoft;
use App\Core\Traits\ProductParameters;

/**
 * Exit product flow
 *
 * Class ExitFlow
 * @package App\Core
 */
class ExitFlow
{
	use Bridge,
		Message,
		HasData,
		Peoplesoft,
		ForgetId,
		LogException,
		ProductParameters;

	/**
	 * Call process to exit product flow
	 *
	 * @param $flow
	 * @param $productName
	 * @param $request
	 * @return bool|\Illuminate\Http\RedirectResponse
	 */
	public function exit($flow, $productName, $request)
	{
		$productParameters = $this->productParameters($productName);

		$response = $this->soap(
			$productName,
			$productParameters['company'],
			$productParameters['exit_process'],
			$this->parameters($flow, $productName, $request),
			$productParameters['exit_process_params'],
			$flow
		);

		return $response;
	}

	/**
	 * Construct de data object for exit process
	 *
	 * @param $flow
	 * @param $productName
	 * @param $request
	 * @return array
	 */
	public function parameters($flow, $productName, $request) : array
	{
		$productParameters = $this->productParameters($productName);

		if (in_array($productName, ['bank_account', 'bank_auto']))
		{
			$rejectionType = null;
		} else {
			$rejectionType = $this->rejectionType($request->reject);
		}

		$code = $productParameters['product_code'];

//		$channel = $productParameters['user']['agent'];
		$channel = $productParameters['channel'];

		return [
			new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
			$this->hasData('Identificacion', $flow->customer->identifier),

			/*
			 * User information
			 */
			$this->peopleSoft(),
			new Data(true, 'Ip', null, request()->ip()),

			/*
			 * Static fields
			 */
			$this->hasData('ProductoOrigenRegistro', $code),
			$this->hasData('CanalOrigenRegistro', $channel),
            $this->hasData('TipoRechazo', $rejectionType),
			$this->hasData('CodigoError', $request->reject),
		];
	}

	/**
	 * Return the rejection code
	 *
	 * @param $code
	 * @return mixed
	 */
	private function rejectionType($code)
	{
		$codes = [
			'TU15' => 'T',
			'TA01' => 'R'
		];

		return $codes[$code];
	}
}