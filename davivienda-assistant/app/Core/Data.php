<?php

namespace App\Core;

class Data
{
    /**
     * @var string
     */
    protected $HasData;

    /**
     * @var string
     */
    protected $Field;

    /**
     * @var string
     */
    protected $Id;

    /**
     * @var string
     */
    protected $Value;

    /**
     * @var string
     */
    protected $Error;

    /**
     * @var string
     */
    protected $ExternalError;

    /**
     * @var string
     */
    protected $InternalError;

    /**
     * @var array
     */
    protected $DataList;

    /**
     * Constructor
     *
     * @param string $HasData
     * @param string $Field
     * @param string $Id
     * @param string $Value
     * @param string $Error
     * @param string $ExternalError
     * @param string $InternalError
     * @param object $DataList
     */
    public function __construct($HasData, $Field = null, $DataList = null, $Value = null, $Id = null, $Error = null, $ExternalError = null, $InternalError = null)
    {
        $this->HasData = $HasData;
        $this->Field = $Field;
        $this->DataList = $DataList;
        $this->Value = $Value;
        $this->Id = $Id;
        $this->Error = $Error;
        $this->ExternalError = $ExternalError;
        $this->InternalError = $InternalError;
    }

    /**
     * @return string
     */
    public function getHasData() : string
    {
        return $this->HasData;
    }

    /**
     * @return string
     */
    public function getField() : string
    {
        return $this->Field;
    }

    /**
     * @return array
     */
    public function getDataList() : array
    {
        return $this->DataList;
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->Value;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->Id;
    }

    /**
     * @return string
     */
    public function getError() : string
    {
        return $this->Error;
    }

    /**
     * @return string
     */
    public function getExternalError() : string
    {
        return $this->ExternalError;
    }

    /**
     * @return string
     */
    public function getInternalError() : string
    {
        return $this->InternalError;
    }
}
