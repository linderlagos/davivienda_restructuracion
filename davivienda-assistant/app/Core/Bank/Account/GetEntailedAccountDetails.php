<?php

namespace App\Core\Bank\Account;

use App\Core\GetResponse;

/**
 * Class GetApprovedCardDetails
 * @package App\Core
 */
class GetEntailedAccountDetails
{
    public function details($response, $flow)
    {
        $getValueFromResponse = new GetResponse();

        // Customer information
	    $customer = $flow->customer_information;

        $customer['cif']['value'] = $getValueFromResponse->result($response, 'CIF');

        $customer['economic_activity']['code'] = $getValueFromResponse->result($response, 'ActividadEconomicaCodigo');
        $customer['economic_activity']['value'] = $getValueFromResponse->result($response, 'ActividadEconomicaDescripcion');

        $customer['customer_risk'] = $getValueFromResponse->result($response, 'NivelRiesgoCliente');

	    $customer['birth_country']['code'] = $getValueFromResponse->result($response, 'PaisNacimientoCodigo');
	    $customer['birth_country']['value'] = $getValueFromResponse->result($response, 'PaisNacimientoDescripcion');

	    $customer['birthplace'] = $getValueFromResponse->result($response, 'LugarNacimiento');

	    $customer['income_range']['code'] = $getValueFromResponse->result($response, 'RangoIngresosCodigo');
	    $customer['income_range']['initial'] = $getValueFromResponse->result($response, 'RangoIngresosLimiteInicial');
	    $customer['income_range']['end'] = $getValueFromResponse->result($response, 'RangoIngresosLimiteFinal');

	    $customer['income_source'] = $getValueFromResponse->result($response, 'CuentaFuenteIngresos');

	    // Product information
	    $product = $flow->product_information;

	    $accountNumber = $getValueFromResponse->result($response, 'NumeroCuenta');

	    $product['product']['value'] = $accountNumber;
	    $product['debit_card']['code'] = $getValueFromResponse->result($response, 'NumeroTarjetaDebito');
	    $product['debit_card']['value'] = $getValueFromResponse->result($response, 'NombreTarjetaDebito');
	    $product['purpose'] = $getValueFromResponse->result($response, 'CuentaProposito');
	    $product['expected_deposits'] = $getValueFromResponse->result($response, 'CuentaDepositosEsperados');
	    $product['expected_withdrawals'] = $getValueFromResponse->result($response, 'CuentaRetirosEsperados');

	    // User information
	    $user = $flow->user_information;

	    $user['branch']['code'] = $getValueFromResponse->result($response, 'ContratoAgenciaCodigo');
	    $user['branch']['value'] = $getValueFromResponse->result($response, 'ContratoAgenciaDescripcion');
	    $user['branch']['city'] = $getValueFromResponse->result($response, 'ContratoCiudad');

	    $user['agent']['code'] = $getValueFromResponse->result($response, 'GestorCodigo');
	    $user['agent']['value'] = $getValueFromResponse->result($response, 'GestorNombre');

        $flow->update([
        	'identifier' => $accountNumber,
            'customer_information' => $customer,
	        'product_information' => $product,
	        'user_information' => $user
        ]);
    }
}
