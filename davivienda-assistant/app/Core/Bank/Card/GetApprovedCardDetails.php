<?php

namespace App\Core\Bank\Card;

use App\Core\GetResponse;

/**
 * Class GetApprovedCardDetails
 * @package App\Core
 */
class GetApprovedCardDetails
{
    public function details($response, $flow)
    {
        $getValueFromResponse = new GetResponse();

        $product = $flow->product_information;

        $cardSegment = $product['product']['code'];

	    $product['request']['number'] = $getValueFromResponse->result($response, 'NumeroSolicitudCredito');

        if ($cardSegment === 'DDP') {
            $limit = $getValueFromResponse->result($response, 'TarjetaCreditoLimiteDADINEROPLUS');
            $type = $getValueFromResponse->result($response, 'TarjetaCreditoDescripcionTarjetaDADINEROPLUS');
        } else {
            $limit = $getValueFromResponse->result($response, 'TarjetaCreditoLimiteDAVIPUNTOS');
            $type = $getValueFromResponse->result($response, 'TarjetaCreditoDescripcionTarjetaDAVIPUNTOS');
        }

        $product['product']['limit'] = $limit;
        $product['product']['type'] = ucfirst(strtolower($type));

	    $flow->update([
        	'product_information' => $product
        ]);

	    return $flow;
    }

}
