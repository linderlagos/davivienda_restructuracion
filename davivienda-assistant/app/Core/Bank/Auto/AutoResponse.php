<?php

namespace App\Core\Bank\Auto;

use App\Core\GetResponse;
use App\Core\CustomerRegistry;
use App\Core\Traits\GetValueFromResponse;
use App\Models\Customer;

/**
 * Class GetApprovedAutoDetails
 * @package App\Core
 */
class AutoResponse extends CustomerRegistry
{
	use GetValueFromResponse;

	public function search($response, $flow)
	{
		$user = $flow->user_information;

		if(!env('BANK_AUTO_DEMO'))
		{
			$getValueFromResponse = new GetResponse();

			$requestNumber = $getValueFromResponse->result($response, 'NumeroSolicitudCredito');

			$concessionaire = $getValueFromResponse->result($response, 'CodigoConcesionaria');

			$seller = $getValueFromResponse->result($response, 'CodigoVendedor');

			$concessionaire_area = $getValueFromResponse->result($response, 'ZonaConcesionaria');
		} else {
			$customer = $flow->customer;

			if ($customer->flows()->get()->count() > 1)
			{
				$requestNumber = $customer->flows()->first();
			} else {
				$requestNumber = rand(100000, 999999);
			}

			$concessionaire = '1';

			$seller = '1';

			$concessionaire_area = 'cs';
		}

		$user['concessionaire'] = $this->codeValue($this->productName(), $user, ['concessionaire', 'code'],  $concessionaire, 'concessionaireOptions');

		$user['seller'] = $this->codeValue($this->productName(), $user, ['seller', 'code'], $seller, 'sellerOptions');

		$user['concessionaire_area'] = $this->codeValue($this->productName(), $user, ['concession_area', 'code'], $concessionaire_area, 'businessZoneOptions');

		$flow->update([
			'identifier' => $requestNumber,
			'user_information' => $user
		]);

		return $flow;
	}

    public function calculate($response, $flow)
    {
	    $product = $flow->product_information;

	    if(!env('BANK_AUTO_DEMO'))
	    {
		    $getValueFromResponse = new GetResponse();

		    $product['correlative_stage'] = $getValueFromResponse->result($response, 'CorrelativoEscenario');
		    $product['result_stage'] = $getValueFromResponse->result($response, 'RespuestaEscenario');
	    } else {
	    	$correlative = 1;

	    	if (isset($product['correlative_stage']))
		    {
			    $correlative = $product['correlative_stage'] + 1;
		    }

		    $result = rand(1,12);

		    if ($result <= 10)
		    {
		    	$stage = 'A';
		    } else {
			    $stage = 'D';
		    }

		    $product['correlative_stage'] = $correlative;
		    $product['result_stage'] = $stage;
	    }

        $flow->update([
            'product_information' => $product
        ]);

        return $flow;
    }

	/**
	 * @param $response
	 * @param $flow
	 * @return array
	 */
	public function evaluations($response, $flow)
	{
		if (!env('BANK_AUTO_DEMO'))
		{
			$data = $response->getCreditCardProcessResult()->Data->DataList->Data;

			$evaluationList = $this->getValueFromResponse($data, ['EVALUATIONS_LIST'], true);

			if ($evaluationList)
			{
				$evaluations = $evaluationList->DataList->Data;

				if (!is_array($evaluations))
				{
					$evaluations = [];

					$evaluations[] = $evaluationList->DataList->Data;
				}

				$lists = [];

				foreach ($evaluations as $key => $evaluation)
				{
					$lists[$key]['numero_solicitud'] = $flow->identifier;
                    $lists[$key]['codigo_concesionaria'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CodigoConcesionaria']);
					$lists[$key]['correlativo_escenario'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CorrelativoEscenario']);
					$lists[$key]['fecha_escenario'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'FechaEscenarioAAAAMMDD']);
                    $lists[$key]['hora_escenario'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'HoraEscenarioHHMMSS']);
					$lists[$key]['codigo_vendedor'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CodigoVendedor']);
					$lists[$key]['tipo_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'TipoAuto']);
					$lists[$key]['marca_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'MarcaAuto']);
					$lists[$key]['modelo_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'ModeloAuto']);
					$lists[$key]['estado_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'EstadoAuto']);
					$lists[$key]['anho_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'AnhoAuto']);
					$lists[$key]['cuota_seguro_danho'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CuotaSeguroDanho']);
					$lists[$key]['cuota_original_seguro_deuda'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CuotaOriginalSeguroDeuda']);
                    $lists[$key]['gastos_varios'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'GastosVarios']);
                    $lists[$key]['honorarios_legales'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'HonorariosLegales']);
                    $lists[$key]['gastos_legales'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'GastosLegales']);
                    $lists[$key]['gastos_cierre'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'GastosCierre']);
                    $lists[$key]['valor_financiar'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'ValorFinanciar']);
                    $lists[$key]['cuota_seguro_deuda'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CuotaSeguroDeuda']);
                    $lists[$key]['periodo_gracia'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'PeriodoGracia']);
                    $lists[$key]['plazo'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'Plazo']);
                    $lists[$key]['cuota_nivelada'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CuotaNivelada']);
                    $lists[$key]['cuota_total'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CuotaTotal']);
                    $lists[$key]['respuesta_motor_decisiones_credito'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'RespuestaMotorDecisionesCredito']);
                    $lists[$key]['destino_motor_decisiones_credito'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'DestinoMotorDecisionesCredito']);
                    $lists[$key]['precio_venta'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'PrecioVenta']);
                    $lists[$key]['precio_avaluo'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'PrecioAvaluo']);
                    $lists[$key]['prima'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'Prima']);
                    $lists[$key]['financiar_gastos_cierre'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'FinanciarGastosCierre']);
                    $lists[$key]['aplicar_periodo_gracia'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'AplicarPeriodoGracia']);
                    $lists[$key]['fecha_evaluacion_motor_decisiones'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'FechaEvaluacionMotorDecisionesAAAAMMDD']);
                    $lists[$key]['hora_evaluacion_motor_decisiones'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'HoraEvaluacionMotorDecisionesHHMMSS']);
                    $lists[$key]['color_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'ColorAuto']);
                    $lists[$key]['cilindraje_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'CilindrajeAuto']);
                    $lists[$key]['numero_serie_chasis_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'NumeroSerieChasisAuto']);
                    $lists[$key]['numero_serie_motor_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'NumeroSerieMotorAuto']);
                    $lists[$key]['numero_serie_vin_auto'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'NumeroSerieVINAuto']);

				}

				return $lists;
			}

			return [];
		}

		$customer = Customer::byIdentifier($flow->customer->identifier)->firstOrFail();

		$flows = $customer->flows()->where('type', $this->productName())->whereIn('step', ['4'])->get();

		if ($flows)
		{
			$lists = [];

			foreach ($flows as $evaluation) {
				$lists['concesionaria'] = get_json($evaluation->product_information, ['concesionaria']);

                $lists['numero_solicitud'] = $flow->identifier;
                $lists['codigo_concesionaria'] = get_json($evaluation->user_information, ['concessionaire', 'value']);
                $lists['correlativo_escenario'] = get_json($evaluation->product_information, ['result_stage']);
                //$lists[$key]['fecha_escenario'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'FechaEscenarioAAAAMMDD']);
                //$lists[$key]['hora_escenario'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'HoraEscenarioHHMMSS']);
                $lists['codigo_vendedor'] = get_json($evaluation->user_information, ['seller', 'value']);
                $lists['tipo_auto'] = get_json($evaluation->product_information, ['auto', 'type', 'value']);
                $lists['marca_auto'] = get_json($evaluation->product_information, ['auto', 'brand', 'value']);
                $lists['modelo_auto'] = get_json($evaluation->product_information, ['auto', 'model']);
                $lists['estado_auto'] = get_json($evaluation->product_information, ['auto', 'status', 'value']);
                $lists['anho_auto'] = get_json($evaluation->product_information, ['auto', 'year', 'value']);
                $lists['cuota_seguro_danho'] = get_json($evaluation->product_information, ['response_scenario', 'safe_insurance']);
                $lists['cuota_original_seguro_deuda'] = get_json($evaluation->product_information, ['response_scenario', 'original_debt_insurance']);
                $lists['gastos_varios'] = get_json($evaluation->product_information, ['response_scenario', 'miscellaneous_expenses']);
                //$lists[$key]['honorarios_legales'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'HonorariosLegales']);
                $lists['gastos_legales'] = get_json($evaluation->product_information, ['response_scenario', 'legal_expenses']);
                //$lists[$key]['gastos_cierre'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'GastosCierre']);
                $lists['valor_financiar'] = get_json($evaluation->product_information, ['response_scenario', 'value_finance']);
                $lists['cuota_seguro_deuda'] = get_json($evaluation->product_information, ['response_scenario', 'debt_insurance']);
                //$lists[$key]['periodo_gracia'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'PeriodoGracia']);
                $lists['plazo'] = get_json($evaluation->product_information, ['financing_term', 'value']);
                $lists['cuota_nivelada'] = get_json($evaluation->product_information, ['response_scenario', 'level_fee']);
                $lists['cuota_total'] = get_json($evaluation->product_information, ['response_scenario', 'total_quota']);
                $lists['respuesta_motor_decisiones_credito'] = get_json($evaluation->product_information, ['response_engine_evaluation', 'answer_motor_decisions_credit']);
                $lists['destino_motor_decisiones_credito'] = get_json($evaluation->product_information, ['response_engine_evaluation', 'destination_motor_decisions_credit']);
                $lists['precio_venta'] = get_json($evaluation->product_information, ['sale_price']);
                $lists['precio_avaluo'] = get_json($evaluation->product_information, ['appraisal']);
                $lists['prima'] = get_json($evaluation->product_information, ['premium']);
                $lists['financiar_gastos_cierre'] = get_json($evaluation->product_information, ['closing_costs', 'value']);
                $lists['aplicar_periodo_gracia'] = get_json($evaluation->product_information, ['grace_period', 'value']);
                //$lists[$key]['fecha_evaluacion_motor_decisiones'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'FechaEvaluacionMotorDecisionesAAAAMMDD']);
                //$lists[$key]['hora_evaluacion_motor_decisiones'] = $this->getValueFromResponse($evaluation, ['EVALUATION_' . ($key + 1), 'HoraEvaluacionMotorDecisionesHHMMSS']);
                $lists['color_auto'] = get_json($evaluation->product_information, ['auto', 'color']);
                $lists['cilindraje_auto'] = get_json($evaluation->product_information, ['auto', 'displacement']);
                $lists['numero_serie_chasis_auto'] = get_json($evaluation->product_information, ['auto', 'chassis']);
                $lists['numero_serie_motor_auto'] = get_json($evaluation->product_information, ['auto', 'motor']);
                $lists['numero_serie_vin_auto'] = get_json($evaluation->product_information, ['auto', 'vin']);
			}

			return $lists;
		}

		return [];
	}


    public function fourth($response, $flow)
    {
        $product = $flow->product_information;

        if(!env('BANK_AUTO_DEMO'))
        {
            $results = new GetResponse();

            $product['response_engine_evaluation']['answer_motor_decisions_credit'] = $results->result($response, 'RespuestaMotorDecisionesCredito');

            $product['response_engine_evaluation']['destination_motor_decisions_credit'] = $results->result($response, 'DestinoMotorDecisionesCredito');

        } else {
            $product['response_engine_evaluation']['answer_motor_decisions_credit'] = 'T';

            $product['response_engine_evaluation']['destination_motor_decisions_credit'] = 'G';
        }

        $flow->update([
            'product_information' => $product,
        ]);

        return $flow;
    }


    private function productName()
    {
        return 'bank_auto';
    }
}