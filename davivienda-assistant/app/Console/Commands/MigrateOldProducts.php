<?php

namespace App\Console\Commands;

// Core
use Illuminate\Console\Command;

// Models
use App\User;
use App\Models\Product;
use App\Models\Printable\Printable;
use App\Models\Insurance\SafeFamily\SafeFamilyPrintable;

// Traits
use App\Core\Traits\Bridge;
use App\Core\Traits\Message;
use App\Core\Traits\HasData;
use App\Core\Traits\ForgetId;
use App\Core\Traits\GetLists;
use App\Core\Traits\PersistId;
use App\Core\Traits\RecentFlow;
use App\Core\Traits\UpdateStep;
use App\Core\Traits\FlowIsActive;
use App\Core\Traits\LogException;
use App\Core\Traits\FilterProducts;
use App\Core\Traits\AuditParameters;
use App\Core\Traits\ProductParameters;
use App\Core\Traits\GetOrCreateCustomer;
use App\Core\Traits\StorePrintableProduct;

// Helpers
use Jenssegers\Date\Date;

class MigrateOldProducts extends Command
{
	use UpdateStep,
		GetLists,
		RecentFlow,
		FlowIsActive,
		ProductParameters,
		HasData,
		Bridge,
		PersistId,
		ForgetId,
		StorePrintableProduct,
		GetOrCreateCustomer,
		AuditParameters,
		FilterProducts;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'davivienda:migrate {product}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old product resources to new structure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $productName = $this->validateProduct($this->argument('product'));

	    if (!$productName) {
		    $this->error('El product ingresado como argumento no es válido');

		    return false;
	    }

	    try {
		    if ($this->argument('product') === 'bank_account')
		    {
		    	return $this->migrateBankAccount();
		    } elseif ($this->argument('product') === 'insurance_safe_family')
		    {
			    return $this->migrateInsuranceSafeFamily();
		    }
	    } catch (\Exception $e) {
		    $this->error('Se presentó el siguiente error: ' . $e->getMessage());

		    return false;
	    }
    }

	private function validateProduct($product)
	{
		$products = [
			'Tarjeta' => 'bank_card',
			'Cuenta' => 'bank_account',
			'Préstamo de auto' => 'bank_auto',
			'Familia Segura' => 'insurance_safe_family',
		];

		$key = array_search($product, $products, true);

		if ($key) {
			return $key;
		}

		return false;
	}

	private function migrateBankAccount()
	{
		$checkIfTableHasData = Product::where('type', 'bank_account')->first();

		if ($checkIfTableHasData)
		{
			$this->error('Ya existen cuentas en la tabla por lo que no se puede continuar con la migración');

			return false;
		}

		$printables = Printable::all();

		foreach ($printables as $printable)
		{
			$customer = $this->getOrCreateCustomer($printable->identifier);

			$user = User::where('peopleSoft', $printable->peoplesoft)->first();

			$product = $customer->products()->create([
				'id' => $printable->id,
				'identifier' => $printable->product_identifier,
				'random' => $printable->random,
				'customer_information' => $this->customerInformation($printable),
				'product_information' => $this->productInformation($printable),
				'user_information' => $this->userInformation($printable),
				'version' => $printable->version,
				'type' => 'bank_account',
				'company' => 'HN',
				'created_at' => $printable->created_at,
				'updated_at' => $printable->updated_at,
				'user_id' => $user->id
			]);

			$customer = $product->customer->update([
				'cif' => $this->updateCustomerIfValueIsDifferent($product, ['cif', 'value'], 'cif'),
				'cif_type' => $this->updateCustomerIfValueIsDifferent($product, ['cif', 'code'], 'cif_type'),
				'name' => $this->updateCustomerIfValueIsDifferent($product, ['name', 'fullname'], 'name'),
				'birth' => $this->updateCustomerIfValueIsDifferent($product, 'birth', 'birth'),
				'identifier_type' => '0',
			]);

			$audit = $product->audits()->create([
				'name' => 'creado',
				'type' => 'cif',
				'user_id' => $user->id,
				'created_at' => $product->created_at,
				'updated_at' => $product->updated_at,
			]);

			activity('product')
				->causedBy($user)
				->performedOn($product)
				->log('Se migró el producto a la nueva estructura');

			foreach ($printable->uploads as $key => $type)
			{
				foreach ($type as $upload)
				{
					$uploadUser = User::where('peopleSoft', $upload['peoplesoft'])->first();

					$date = Date::createFromTimestamp($upload['uploaded_at']);

					if ($key === 'identity')
					{
						$key = 'identidad';
					} elseif ($key === 'signature')
					{
						$key = 'firma';
					} elseif ($key === 'deed')
					{
						$key = 'escritura-de-comerciante';
					} else {
						$key = 'soporte';
					}

					$types = [
						'identidad' => 'Identidad',
						'firma' => 'Firma',
						'escritura-de-comerciante' => 'Escritura de Comerciante',
						'soporte' => 'Documento Soporte',
					];

					$newUpload = $product->uploads()->create([
						'url' => $upload['url'],
						'type' => $key,
						'user_id' => $uploadUser->id,
						'created_at' => $date,
						'updated_at' => $date
					]);

					$uploadAudit = $newUpload->audits()->create([
						'name' => 'creado',
						'type' => 'formalizacion',
						'user_id' => $user->id,
						'created_at' => $date,
						'updated_at' => $date,
					]);

					activity('upload')
						->causedBy($uploadUser)
						->performedOn($product)
						->log('Se migró el documento de tipo ' . $types[$key] . ' a la nueva estructura');
				}
			}
		}

		$this->info('Se generó correctamente la migración para el producto de Cuenta de Ahorro');

		return true;
	}

	private function migrateInsuranceSafeFamily()
	{
		$checkIfTableHasData = Product::where('type', 'insurance_safe_family')->first();

		if ($checkIfTableHasData)
		{
			$this->error('Ya existen pólizas de familia segura en la tabla por lo que no se puede continuar con la migración');

			return false;
		}

		$printables = SafeFamilyPrintable::all();

		foreach ($printables as $printable)
		{
			$user = User::where('peopleSoft', get_json($printable->user_information, ['agent', 'peoplesoft']))->first();

			$parent = Product::find($printable->productable_id);

			$product = $parent->children()->create([
				'identifier' => $printable->identifier,
				'random' => $printable->random,
				'customer_information' => $printable->customer_information,
				'product_information' => $printable->product_information,
				'user_information' => $printable->user_information,
				'version' => $printable->version,
				'type' => 'insurance_safe_family',
				'company' => 'HN-SEGUROS',
				'created_at' => $printable->created_at,
				'updated_at' => $printable->updated_at,
				'user_id' => $user->id,
				'customer_id' => $parent->customer_id
			]);

			$audit = $product->audits()->create([
				'name' => 'creado',
				'type' => 'cif',
				'user_id' => $user->id,
				'created_at' => $product->created_at,
				'updated_at' => $product->updated_at,
			]);

			activity('product')
				->causedBy($user)
				->performedOn($product)
				->log('Se migró el producto a la nueva estructura');
		}

		$this->info('Se generó correctamente la migración para el producto de Familia Segura');

		return true;
	}

	private function customerInformation($printable)
	{
		$data = [];

		$fields = $printable->fields;

		$data['name']['first'] = null;
		$data['name']['middle'] = null;
		$data['name']['last'] = null;
		$data['name']['second_last'] = null;

		$data['name']['fullname'] = get_json($fields, 'name');

		$data['gender'] = $fields['gender'];

		$data['phone'] = get_json($fields, 'phone');

		$data['mobile'] = get_json($fields, 'mobile');


		$data['email']['account'] = get_json($fields, 'email');

		if (get_json($fields, 'email') == 'notiene@davivienda.com.hn') {
			$data['email']['has_email'] = 'N';
		} else {
			$data['email']['has_email'] = 'S';
		}


		$data['marital_status'] = $fields['marital_status'];

		$data['spouse'] = get_json($fields, 'spouse');

		$data['birth'] = get_json($fields, 'birth');


		$data['address']['other_city'] = null;

		$address = get_json($fields, 'address');

		$data['address']['code'] = null;
		$data['address']['full'] = $address;

		$data['address']['first'] = $this->divideString($address, 40, 1);
		$data['address']['second'] = $this->divideString($address, 40, 2);
		$data['address']['third'] = $this->divideString($address, 40, 3);

		$data['address']['state'] = null;
		$data['address']['state_code'] = null;
		$data['address']['municipality'] = null;
		$data['address']['municipality_code'] = null;
		$data['address']['colony'] = null;
		$data['address']['colony_code'] = null;


		$data['nationality'] = $fields['nationality'];

		$data['profession'] = $fields['profession'];

		$data['receive_sms']['code'] = null;
		$data['receive_sms']['value'] = null;

		$data['receive_email']['code'] = null;
		$data['receive_email']['value'] = null;


		$data['citizenship'] = $fields['citizenship'];

		$data['income'] = get_json($fields, 'income');


		$employer = $fields['employer'];

		$data['employer']['name'] = get_json($employer, 'name');

		$data['employer']['phone'] = get_json($employer, 'phone');

		$data['employer']['started_at'] = get_json($employer, 'started_at');

		$data['employer']['type']['code'] = null;
		$data['employer']['type']['value'] = null;


		$data['employer']['job']['name'] = get_json($employer, 'job');
		$data['employer']['job']['type'] = $fields['job_type'];
		$data['employer']['job']['status'] = null;

		$data['employer']['address']['other_city'] = null;

		$address = get_json($employer, 'address');

		$data['employer']['address']['code'] = null;
		$data['employer']['address']['full'] = $address;

		$data['employer']['address']['first'] = $this->divideString($address, 40, 1);
		$data['employer']['address']['second'] = $this->divideString($address, 40, 2);
		$data['employer']['address']['third'] = $this->divideString($address, 40, 3);


		$data['employer']['address']['state'] = null;
		$data['employer']['address']['state_code'] = null;
		$data['employer']['address']['municipality'] = null;
		$data['employer']['address']['municipality_code'] = null;
		$data['employer']['address']['colony'] = null;
		$data['employer']['address']['colony_code'] = null;


		$data['other_income']['code'] = null;
		$dara['other_income']['vale'] = null;

		$data['assets'] = $fields['assets'];

		$data['passive'] = $fields['passive'];

		$data['expenses'] = $fields['expenses'];

		$data['fatca']['code'] = null;
		$data['fatca']['value'] = null;


		$publicJobs = $fields['public_job'];

		$publicJob = get_json($publicJobs, 'confirmation');

		$data['public_job']['code'] = $publicJob;
		$data['public_job']['value'] = $this->searchValueFromList($this->productName(), $publicJob, 'confirmation', false);

		$data['public_job']['employer_name'] = get_json($publicJobs, 'institution');

		$data['public_job']['job'] = get_json($publicJobs, 'job');

		$publicJobActive = get_json($publicJobs, 'active');

		$data['public_job']['active'] = $publicJobActive;
		$data['public_job']['active_value'] = $this->searchValueFromList($this->productName(), $publicJobActive, 'confirmation', false);

		$data['public_job']['from'] = get_json($publicJobs, 'from');

		$data['public_job']['to'] = get_json($publicJobs, 'to');


		//***** verificar la existencia en otros registros
		$data['dependants']['code'] = null;
		$data['dependants']['value'] = null;

		for ($i = 1; $i <= 10; $i++) {
			$data['dependants'][$i]['name'] = null;


		}

		$beneficiarie = $fields['beneficiaries'];

		foreach ($beneficiarie as $key => $beneficiary) {

			$data['beneficiaries'][$key]['name'] = get_json($beneficiary, 'beneficiary_' . $key . '_name');

			$relationship = get_json($beneficiary, 'beneficiary_' . $key . '_relationship');

			$data['beneficiaries'][$key]['relationship']['code'] = null;
			$data['beneficiaries'][$key]['relationship']['value'] = $relationship;

			$data['beneficiaries']['id_type'] = $beneficiary['beneficiary_' . $key . '_relationship'];

			$data['beneficiaries']['identity'] = get_json($beneficiary, 'beneficiary_' . $key . '_identity');

		}


		$professionalDependant = $fields['professional_dependant'];

		foreach ($professionalDependant as $key => $professional) {
			$data['professional_dependant']['name'] = get_json($professional, 'professional_dependant_' . $key . '_name');

			$data['professional_dependant']['relationship']['code'] = null;
			$data['professional_dependant']['relationship']['value'] = get_json($professional, 'professional_dependant_' . $key . '_relationship');

			$data['professional_dependant']['job_type'] = get_json($professional, 'professional_dependant_' . $key . '_job_type');

			$data['professional_dependant']['id_type'] = get_json($professional, 'professional_dependant_' . $key . '_id_type');

			$data['professional_dependant']['identity'] = get_json($professional, 'professional_dependant_' . $key . '_identity');
		}


		$cif = get_json($fields, 'cif');

		$data['cif']['code'] = 'P';
		$data['cif']['value'] = $cif;


		$references = $fields['references'];

		foreach ($references as $key => $reference) {
			$data['references'][$key]['name'] = get_json($reference, 'reference_' . $key . '_name');

			$data['references'][$key]['phone'] = get_json($reference, 'reference_' . $key . '_phone');
			$data['references'][$key]['mobile'] = get_json($reference, 'reference_' . $key . '_mobile');

			$data['references'][$key]['work_phone'] = null;
			$data['references'][$key]['address'] = null;
			$data['references'][$key]['relationship']['code'] = null;
			$data['references'][$key]['relationship']['value'] = null;

		}


		$international = $fields['foreign_currency_operations'];
		$foreignCurrency = get_json($international, 'foreign_currency');

		if ($foreignCurrency !== 'S')
		{
			$data['international_operations_information']['foreign_currency']['code'] = 'N';
			$data['international_operations_information']['foreign_currency']['value'] = 'No';
		} else {
			$data['international_operations_information']['foreign_currency']['code'] = 'S';
			$data['international_operations_information']['foreign_currency']['value'] = 'Si';
		}

		if ($foreignCurrency !== 'S') {
			$data['international_operations_information']['wire_transfers']['code'] = null;
			$data['international_operations_information']['wire_transfers']['value'] = null;
			$data['international_operations_information']['currency_purchase']['code'] = null;
			$data['international_operations_information']['currency_purchase']['value'] = null;
			$data['international_operations_information']['checks_in_dollars']['code'] = null;
			$data['international_operations_information']['checks_in_dollars']['value'] = null;
			$data['international_operations_information']['remittances']['code'] = null;
			$data['international_operations_information']['remittances']['value'] = null;
			$data['international_operations_information']['typical_monthly_income'] = null;
			$data['international_operations_information']['typical_monthly_currency']['code'] = null;
			$data['international_operations_information']['typical_monthly_currency']['value'] = null;
			$data['international_operations_information']['probable_monthly_income'] = null;
			$data['international_operations_information']['probable_monthly_currency'] = null;
			$data['international_operations_information']['received_transfer_country'] = null;
			$data['international_operations_information']['received_transfer_sender'] = null;
			$data['international_operations_information']['received_transfer_reason'] = null;
			$data['international_operations_information']['sent_transfer_country']['code'] = null;
			$data['international_operations_information']['sent_transfer_country']['value'] = null;
			$data['international_operations_information']['sent_transfer_sender'] = null;
			$data['international_operations_information']['sent_transfer_reason'] = null;
			$data['international_operations_information']['remittance_country']['code'] = null;
			$data['international_operations_information']['remittance_country']['value'] = null;
			$data['international_operations_information']['remittance_sender'] = null;
			$data['international_operations_information']['remittance_sender_relationship']['code'] = null;
			$data['international_operations_information']['remittance_sender_relationship']['value'] = null;
			$data['international_operations_information']['remittance_reason'] = null;
		} else {
			$wireTransfers = get_json($international, 'wire_transfers');

			if ($wireTransfers !== 'S')
			{
				$data['international_operations_information']['wire_transfers']['code'] = 'N';
				$data['international_operations_information']['wire_transfers']['value'] = 'No';
			} else {
				$data['international_operations_information']['wire_transfers']['code'] = 'S';
				$data['international_operations_information']['wire_transfers']['value'] = 'Si';
			}

			if ($wireTransfers !== 'S') {
				$data['international_operations_information']['received_transfer_country'] = null;
				$data['international_operations_information']['received_transfer_sender'] = null;
				$data['international_operations_information']['received_transfer_reason'] = null;
				$data['international_operations_information']['sent_transfer_country']['code'] = null;
				$data['international_operations_information']['sent_transfer_country']['value'] = null;
				$data['international_operations_information']['sent_transfer_sender'] = null;
				$data['international_operations_information']['sent_transfer_reason'] = null;
			} else {

				$receivedTransferCountry = get_json($international, 'received_transfer_country');

				if ($receivedTransferCountry === "No se encontró el valor en la lista")
				{
					$data['international_operations_information']['received_transfer_country']['code'] = null;
					$data['international_operations_information']['received_transfer_country']['value'] = null;
				} else {
					$data['international_operations_information']['received_transfer_country'] = get_json($international, 'received_transfer_country');
				}

				$data['international_operations_information']['received_transfer_sender'] = get_json($international, 'received_transfer_sender');

				$data['international_operations_information']['received_transfer_reason'] = get_json($international, 'received_transfer_reason');

				$sentTransferCountry = get_json($international, 'sent_transfer_country');

				if ($sentTransferCountry === "No se encontró el valor en la lista")
				{
					$data['international_operations_information']['sent_transfer_country']['code'] = null;
					$data['international_operations_information']['sent_transfer_country']['value'] = null;
				} else {
					$data['international_operations_information']['sent_transfer_country'] = get_json($international, 'sent_transfer_country');
				}

				$data['international_operations_information']['sent_transfer_sender'] = get_json($international, 'sent_transfer_sender');

				$data['international_operations_information']['sent_transfer_reason'] = get_json($international, 'sent_transfer_reason');
			}

			$currencyPurchase = get_json($international, 'currency_purchase');
			$data['international_operations_information']['currency_purchase'] = $this->codeValue($this->productName(), $fields, ['international_operations_information', 'currency_purchase', 'code'], $currencyPurchase, 'confirmation');

			$checksDollars = get_json($international, 'checks_in_dollars');
			$data['international_operations_information']['checks_in_dollars'] = $this->codeValue($this->productName(), $fields, ['international_operations_information', 'checks_in_dollars', 'code'], $checksDollars, 'confirmation');

			$data['international_operations_information']['typical_monthly_income'] = get_json($international, 'typical_monthly_income');

			$monthlyCurrency = get_json($international, 'typical_monthly_currency');

			if ($monthlyCurrency === "No se encontró el valor en la lista")
			{
				$data['international_operations_information']['typical_monthly_currency']['code'] = null;
				$data['international_operations_information']['typical_monthly_currency']['value'] = null;
			} else {
				$data['international_operations_information']['typical_monthly_currency'] =get_json($international, 'typical_monthly_currency');
			}

			$data['international_operations_information']['probable_monthly_income'] = get_json($international, 'probable_monthly_income');

			$probableCurrency = get_json($international, 'probable_monthly_currency');

			if ($probableCurrency === "No se encontró el valor en la lista")
			{
				$data['international_operations_information']['probable_monthly_currency']['code'] = null;
				$data['international_operations_information']['probable_monthly_currency']['value'] = null;
			} else {
				$data['international_operations_information']['probable_monthly_currency'] = get_json($international, ['probable_monthly_currency', 'code']);
			}

			$remittances = get_json($international, 'remittances');

			if ($remittances !== 'S')
			{
				$data['international_operations_information']['remittances']['code'] = 'N';
				$data['international_operations_information']['remittances']['value'] = 'No';
			} else {
				$data['international_operations_information']['remittances']['code'] = 'S';
				$data['international_operations_information']['remittances']['value'] = 'Si';
			}

			if ($remittances !== 'S') {
				$data['international_operations_information']['remittance_country']['code'] = null;
				$data['international_operations_information']['remittance_country']['value'] = null;
				$data['international_operations_information']['remittance_sender'] = null;
				$data['international_operations_information']['remittance_sender_relationship']['code'] = null;
				$data['international_operations_information']['remittance_sender_relationship']['value'] = null;
				$data['international_operations_information']['remittance_reason'] = null;
			} else {
				$remittanceCountry = get_json($international, 'remittance_country');
				if ($remittanceCountry === "No se encontró el valor en la lista")
				{
					$data['international_operations_information']['remittance_country']['code'] = null;
					$data['international_operations_information']['remittance_country']['value'] = null;
				} else {
					$data['international_operations_information']['remittance_country'] = get_json($international, 'remittance_country');
				}

				$data['international_operations_information']['remittance_sender'] = get_json($international, 'remittance_sender');

				$senderRelationship = get_json($international, 'remittance_sender_relationship');

				if ($senderRelationship === "No se encontró el valor en la lista")
				{
					$data['international_operations_information']['remittance_sender_relationship']['code'] = null;
					$data['international_operations_information']['remittance_sender_relationship']['value'] = null;
				} else {
					$data['international_operations_information']['remittance_sender_relationship'] = get_json($international, 'remittance_sender_relationship');
				}

				$data['international_operations_information']['remittance_reason'] = get_json($international, 'remittance_reason');
			}
		}

		for ($i = 1; $i <= 10; $i++) {
			$data['national_institutions'][$i]['name'] = null;
		}

		$data['economic_activity'] = $fields['customer_cif_information']['economic_activity'];

		$data['customer_risk'] = get_json($fields, ['customer_cif_information', 'customer_risk']);

		$data['birth_country'] = $fields['customer_cif_information']['birth_country'];

		$data['birthplace'] = get_json($fields, ['customer_cif_information', 'birthplace']);

		$incomeRange = explode(' - ', get_json($fields, ['customer_cif_information', 'income_range', 'value']));

		$data['income_range']['code'] = get_json($fields, ['customer_cif_information', 'income_range', 'code']);
		$data['income_range']['value'] = get_json($fields, ['customer_cif_information', 'income_range', 'value']);
		$data['income_range']['initial'] = $incomeRange[0];
		$data['income_range']['end'] = $incomeRange[1];

		$data['income_source'] = get_json($fields, ['customer_cif_information', 'income_source']);

		return $data;

	}


	public function productInformation($printable)
	{
		$data = [];

		$fields = $printable->fields;

		$data['product']['code'] = get_json($fields, 'account_type');
		$data['product']['value'] = get_json($fields, 'account_number');

		//este campos no estan
		$data['debit_card']['code'] = null;
		$data['debit_card']['value'] = null;

		$cifInfomation = $fields['customer_cif_information'];

		$data['purpose'] = get_json($fields, ['customer_cif_information', 'purpose']);

		$data['expected_deposits'] = get_json($fields, ['customer_cif_information', 'expected_deposits']);

		$data['expected_withdrawals'] = get_json($fields, ['customer_cif_information', 'expected_withdrawals']);

		return $data;
	}


	public function userInformation($printable)
	{
		$data = [];

		$fields = $printable->fields;

		$data['peoplesoft'] = $printable->peoplesoft;

		$data['branch'] = $fields['customer_cif_information']['branch'];

		$data['agent'] = $fields['customer_cif_information']['agent'];

		return $data;
	}


	private function productName()
	{
		return 'bank_account';
	}


	private function divideString($column, $length, $section)
	{
		$columnLength = \strlen($column);

		if ($section === 1) {
			return trim(mb_substr($column, 0, $length), " ");
		} elseif ($columnLength <= $length && $section !== 1) {
			return null;
		} elseif ($columnLength > $length && $section === 2) {
			return trim(mb_substr($column, $length, $length + 1), " ");
		} elseif ($columnLength <= ($length * 2) && $section === 3) {
			return null;
		} elseif ($columnLength > ($length * 2) && $section === 3) {
			return trim(mb_substr($column, ($length * 2) + 1, $length), " ");
		}

		return null;
	}


	private function searchValueFromList($productName, $code, $list, $array = true)
	{
		if ($code) {
			if ($list === 'confirmation') {
				$value = $this->confirmation($code);

				return $this->constructResponse($code, $value, $array);
			}

			if ($productName) {
				$lists = $this->getLists($productName);

				$value = get_json($lists, [$list, $code]);
			} else {
				$value = null;
			}

			return $this->constructResponse($code, $value, $array);
		} else {
			return $this->constructResponse($code, null, $array);
		}
	}

	private function constructResponse($code, $value, $array)
	{
		if ($array) {
			return [
				'code' => $code,
				'value' => $value
			];
		} else {
			return $value;
		}
	}

	private function confirmation($code)
	{
		$list = [
			'N' => 'No',
			'S' => 'Si'
		];

		return $list[$code];
	}

	private function codeValue($productName, $data, $values, $field, $lists, $array = true)
	{
		return $this->searchValueFromList(
			$productName,
			$this->updateIfInRequest(
				get_json($data, $values),
				$field
			),
			$lists,
			$array
		);
	}

	private function updateIfInRequest($field, $parameter)
	{
		if (isset($parameter) && $parameter !== 'NULO')
		{
			return $parameter;
		}

		return $field;
	}

	/**
	 * Checks if value exists in product, is not nullable and is different than customer's column value
	 *
	 * @param $product
	 * @param $field
	 * @param $column
	 * @return mixed
	 */
	private function updateCustomerIfValueIsDifferent($product, $field, $column)
	{
		$getProductField = get_json($product->customer_information, $field);

		$value = $product->customer->$column;

		if ($getProductField !== 'NULO' && $getProductField !== $value)
		{
			$value = $getProductField;
		}

		return $value;
	}
}
