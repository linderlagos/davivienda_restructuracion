<?php

namespace App\Console\Commands;

use App\Core\GetPreFilledData;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CacheData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'davivienda:lists {product}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache Pre-Filled Data by product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $productName = $this->validateProduct($this->argument('product'));

	    if (!$productName) {
		    $this->error('El product ingresado como argumento no es válido');

		    return false;
	    }

	    $object = new GetPreFilledData();

	    try {
	    	if ($this->argument('product') === 'insurance_safe_family')
		    {
			    $lists = $object->insuranceSafeFamilyData();
		    } elseif ($this->argument('product') === 'bank_auto')
		    {
			    $lists = $object->bankAutoData();
		    } else {
			    $lists = $object->data();
		    }

		    Storage::disk('local')->put('public/' . $this->argument('product') .'/' . env(strtoupper($this->argument('product')) . '_PRINTABLES_VERSION') . '/lists.txt', json_encode($lists));

		    $this->info('Se generó correctamente las listas prellenadas del producto ' . $productName);

		    return true;
	    } catch (\Exception $e) {
		    $this->error('Se presentó el siguiente error: ' . $e->getMessage());

		    return false;
	    }
    }

    private function validateProduct($product)
    {
    	$products = [
    		'Tarjeta' => 'bank_card',
		    'Cuenta' => 'bank_account',
		    'Préstamo de auto' => 'bank_auto',
		    'Familia Segura' => 'insurance_safe_family',
	    ];

	    $key = array_search($product, $products, true);

	    if ($key) {
		    return $key;
	    }

	    return false;
    }
}
