<?php

namespace App\Mail\Account;

use App\Models\Insurance\SafeFamily\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPolicy extends Mailable
{
    use Queueable, SerializesModels;

    protected $customer;

    /**
     * Create a new message instance.
     *
     * @param Customer $customer
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$fullname = get_json($this->customer->customer_information, 'name');

    	$name = ucwords(strtolower($fullname));

    	$id = $this->customer->identifier;

    	$random = $this->customer->random;

        return $this
	        ->subject('Póliza de su nuevo seguro Familia Segura')
	        ->view('insurance.safeFamily.mail.policy', compact('name', 'id', 'random'));
    }
}
