<?php

namespace App\Mail\Account;

use App\Models\Printable\Printable;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContract extends Mailable
{
    use Queueable, SerializesModels;

    protected $printable;

    /**
     * Create a new message instance.
     *
     * @param Printable $printable
     * @return void
     */
    public function __construct(Printable $printable)
    {
        $this->printable = $printable;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$fullname = get_json($this->printable->fields, 'name');

    	$name = ucwords(strtolower($fullname));

    	$id = $this->printable->identifier;

    	$random = $this->printable->random;

        return $this
	        ->subject('Contrato de su nueva Cuenta de Ahorro Davivienda')
	        ->view('account.mail.contract', compact('name', 'id', 'random'));
    }
}
