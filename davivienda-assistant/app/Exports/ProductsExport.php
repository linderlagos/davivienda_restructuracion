<?php

namespace App\Exports;

use App\Core\Traits\AuditParameters;
use App\Core\Traits\FilterProducts;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ProductsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithEvents, WithColumnFormatting
{
	use FilterProducts;
	use AuditParameters;

	protected $collection;

	public function __construct($collection)
	{
		$this->collection = $collection;
	}

	/**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$products = $this->collection;

        return $products;
    }

	public function headings(): array
	{
		return $this->headingsByRole();
	}

	/**
	 * @var Product $product
	 * @return array
	 */
	public function map($product): array
	{
		return $this->mapByRole($product);
	}

	/**
	 * @return array
	 */
	public function columnFormats(): array
	{
		return [
			'A' => NumberFormat::FORMAT_TEXT,
			'C' => NumberFormat::FORMAT_TEXT,
			'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,
		];
	}

	/**
	 * @return array
	 */
	public function registerEvents(): array
	{

		$user = Auth::user();

		if ($user->hasRole(env('ROL_FIRMAS')))
		{
			$cellRange = 'A1:F1';
		} else {
			$cellRange = 'A1:E1';
		}

		return [
			AfterSheet::class    => function(AfterSheet $event) use ($cellRange) {

				$event->sheet->getDelegate()->setAutoFilter($cellRange);
				$event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
					'font' => [
						'bold' => true,
						'color' => [
							'argb' => 'FFFFFF',
						]
					],
					'fill' => [
						'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
						'color' => [
							'argb' => 'ed1c27',
						]
					]
				]);
			},
		];
	}

	private function headingsByRole()
	{
		$user = Auth::user();

		if ($user->hasRole(env('ROL_FIRMAS')))
		{
			$headings = [
				'No. de Producto',
				'Nombre del cliente',
				'Identidad',
				'Tipo de producto',
				'Fecha de apertura',
				'Documento de firma'
			];
		} else {
			$headings = [
				'No. de Producto',
				'Nombre del cliente',
				'Identidad',
				'Tipo de producto',
				'Fecha de apertura'
			];
		}

		return $headings;
	}

	private function mapByRole($product)
	{
		$user = Auth::user();

		if ($user->hasRole(env('ROL_FIRMAS')))
		{
			$signatures = $product->uploads()->where('type', 'firma')->get();

			if ($signatures->count() === 0)
			{
				$signatureStatus = 'El asesor no ha subido firma';
			} else {
				$signatureStatus = '';

				foreach ($signatures as $key => $signature)
				{
					if ($key === 0)
					{
						$signatureStatus .= ' 1. ';
					} else {
						$number = $key + 1;

						$signatureStatus .= ' | '  . $number . '. ';
					}

					$signatureStatus .= $this->auditParameters()['formalization_upload_types'][$signature->audits()->first()->name];
				}
			}

			$map = [
				$product->identifier,
				$product->customer->name,
				$product->customer->identifier,
				product_name($product->type),
				$product->created_at->format('d/m/Y'),
				$signatureStatus
			];
		} else {
			$map = [
				$product->identifier,
				$product->customer->name,
				$product->customer->identifier,
				product_name($product->type),
				$product->created_at->format('d/m/Y')
			];
		}

		return $map;
	}
}
