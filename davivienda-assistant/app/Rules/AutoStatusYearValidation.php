<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class AutoStatusYearValidation
 * @package App\Rules
 */
class AutoStatusYearValidation implements Rule
{
	/**
	 * Get field where car status is defined
	 *
	 * @var
	 */
	protected $status;

    /**
     * Create a new rule instance.
     *
     * @param $status
     * @return void
     */
    public function __construct($status)
    {
        $this->status = $status;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = request()->get($this->status);

        if ($status == '016') // New car
        {
        	return $this->inBetween($value, 4);
        } elseif ($status == '017') // Used car
        {
	        return $this->inBetween($value, 9);
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
	    return trans('validation.auto_status_year_validation');
    }

	/**
	 * Get range of years
	 *
	 * @param $value
	 * @param $endRange
	 * @return bool
	 */
	private function inBetween($value, $endRange)
    {
	    $start = date('Y') + 1;
	    $end = date('Y') - $endRange;

	    if ($value <= $start && $value >= $end)
	    {
		    return true;
	    }

	    return false;
    }
}
