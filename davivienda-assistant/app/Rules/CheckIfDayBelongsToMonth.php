<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Jenssegers\Date\Date;

class CheckIfDayBelongsToMonth implements Rule
{
    protected $day;

    protected $month;

    /**
     * Create a new rule instance.
     *
     * @param $day
     * @param $month
     * @return void
     */
    public function __construct($day, $month)
    {
        $this->day = $day;
        $this->month = $month;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $day = request()->get($this->day);
        $month = request()->get($this->month);

        if (request()->has($this->day)) {
            if (in_array($month, [1,3,5,7,8,10,12])) {
                return $day <= 31;
            } elseif (in_array($month, [4,6,9,11])) {
                return $day <= 30;
            } else {
                return $day <= 29;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_if_day_belongs_to_month');
    }
}
