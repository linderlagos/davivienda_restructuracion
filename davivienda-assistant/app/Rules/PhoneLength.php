<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneLength implements Rule
{
    protected $length;

    /**
     * Create a new rule instance.
     *
     * @param int $length
     * @return void
     */
    public function __construct($length)
    {
        $this->length = $length;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) : bool
    {
    	if ($value)
	    {
		    $value = str_replace('-', '', $value);

		    return \strlen($value) === $this->length;
	    }

	    return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El campo :attribute solo debe tener ' . $this->length . ' dígitos.';
    }
}
