<?php

namespace App\Rules\hn;

use Illuminate\Contracts\Validation\Rule;

class RequiredIfAndWithoutAll implements Rule
{
	protected $ifField;

	protected $ifValue;

	protected $allFields;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($ifField, $ifValue, $allFields)
    {
        $this->ifField = $ifField;
        $this->ifValue = $ifValue;
        $this->allFields = $allFields;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (request()->get($this->ifField) === $this->ifValue)
        {
        	$i = 0;

        	foreach ($this->allFields as $field)
	        {
	        	if (in_array(request()->get($field), ['N', null]))
		        {
		        	$i++;
		        }
	        }

	        if (count($this->allFields) === $i)
	        {
	        	return false;
	        }

	        return true;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
	    return trans('validation.required_if_and_without_all');
    }
}
