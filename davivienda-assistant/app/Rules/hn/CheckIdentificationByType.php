<?php

namespace App\Rules\hn;

use Illuminate\Contracts\Validation\Rule;

class CheckIdentificationByType implements Rule
{
	protected $field;

    /**
     * Create a new rule instance.
     *
     * @param $field
     * @return void
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	if (empty($value))
	    {
	    	return true;
	    }

        $value = str_replace('-', '', $value);

        if (in_array(request()->get($this->field), [0, 1]))
        {
	        $state = substr($value, 0, 2);

	        $city = substr($value, 2, 2);

	        $year = substr($value, 4, 4);

	        $stateValidation = ('01' <= $state) && ($state <= '18');

	        $cityValidation = ('01' <= $city) && ($city <= '28');

	        $yearValidation = ((date('Y')-100) <= $year) && ($year <= date('Y'));

	        $length = \strlen($value) === 13;

	        if (request()->get($this->field) === 1)
	        {
	        	$length = \strlen($value) === 15;
	        }

        } else {
	        return preg_match('/^[a-zA-Z0-9]+/', $value);
        }

        return $stateValidation && $cityValidation && $yearValidation && $length;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_identification_by_type');
    }
}
