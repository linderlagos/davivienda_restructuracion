<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RequiredIfOtherCity implements Rule
{
    protected $parameter;

    protected $parameterName;

    /**
     * Create a new rule instance.
     *
     * @param $parameter
     * @param $parameterName
     * @return void
     */
    public function __construct($parameter, $parameterName)
    {
        $this->parameter = $parameter;
        $this->parameterName = $parameterName;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value && strpos(request()->get($this->parameter), 'OTRA COLONIA') !== false) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.other_city', ['parameter' => $this->parameterName]);
    }
}
