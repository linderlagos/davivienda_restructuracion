<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FirstNumber implements Rule
{
    protected $parameters;

    /**
     * Create a new rule instance.
     *
     * @param array $parameters
     * @return void
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	if (isset($value))
	    {
		    $value = str_replace('-', '', $value);

		    $firstNumber = substr($value, 0, 1);

		    return in_array($firstNumber, $this->parameters);
	    }

	    return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $numbers = implode(', ', $this->parameters);

        return 'El primer número del campo :attribute debe ser uno de los siguientes números ' . $numbers;
    }
}
