<?php

namespace App\Models\Printable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class InsurancePrintable extends Model
{
	/**
	 * @var string
	 */
	protected $connection = 'customers';

	/**
	 * @var string
	 */
	protected $table = 'insurance_printables';

	/**
	 * Protect fields for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
		'created_at',
		'updated_at'
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function scopeFindData($query, $identifier, $random)
	{
		return $query->where('identifier', $identifier)->where('random', $random);
	}

	public function scopeAuthorized($query)
	{
		if (cifUsers(Auth::user()->peopleSoft))
		{
			return $query->where('id', '>', 0);
		}

		return $query->where('peoplesoft', Auth::user()->peopleSoft);
	}
}
