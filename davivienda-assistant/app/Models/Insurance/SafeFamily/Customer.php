<?php

namespace App\Models\Insurance\SafeFamily;

use App\Models\CustomerScopes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package App\Models\Account
 */
class Customer extends Model
{
    use CustomerScopes;

    /**
     * @var string
     */
    protected $connection = 'customers';

    /**
     * @var string
     */
    protected $table = 'insurance_safe_family_customers';

    protected $dates = [
    	'approved_at'
    ];

    /**
     * Protect fields for mass assignment
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'customer_information' => 'array',
		'product_information' => 'array',
		'user_information' => 'array',
		'status' => 'array'
	];
}
