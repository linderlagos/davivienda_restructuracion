<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Facades\Auth;

/**
 * Class Product
 * @package App\Models
 */
class Product extends Model
{
	/**
	 * @var string
	 */
	protected $connection = 'customers';

	/**
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * Protect fields for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
//		'created_at',
//		'updated_at'
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'customer_information' => 'array',
		'product_information' => 'array',
		'user_information' => 'array',
		'uploads' => 'array',
		'audit' => 'array',
	];

	/**
	 * A product belongs to a customers
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}

	/**
	 * A product belongs to a user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * A product may have a parent product
	 * where it was initially requested
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function parent()
	{
		return $this->belongsTo(Product::class, 'id', 'parent_id');
	}

	/**
	 * A product may have many child products
	 * that were request after parent was
	 * created
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function children() : HasMany
	{
		return $this->hasMany(Product::class, 'parent_id', 'id');
	}

	/**
	 * A product may have many uploads
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function uploads() : HasMany
	{
		return $this->hasMany(Upload::class);
	}

	/**
	 * Get all of the product's audits.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphMany
	 */
	public function audits() : MorphMany
	{
		return $this->morphMany(Audit::class, 'auditable');
	}

	/**
	 * Get all of the products's comments.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphMany
	 */
	public function comments() : MorphMany
	{
		return $this->morphMany(Comment::class, 'commentable');
	}

	/**
	 * Get customer by identifier and random number
	 *
	 * @param $query
	 * @param $identifier
	 * @param $random
	 * @return mixed
	 */
	public function scopeFindData($query, $identifier, $random)
	{
		return $query->where('identifier', $identifier)->where('random', $random);
	}

	/**
	 * @param $query
	 * @return mixed
	 */
	public function scopeAuthorized($query)
	{
		if (cifUsers(Auth::user()->peopleSoft))
		{
			return $query->where('id', '>', 0);
		}

		return $query->where('peoplesoft', Auth::user()->peopleSoft);
	}

	public function scopeFilterByIds($query, $ids)
	{
		if (count($ids) === 0)
		{
			return $query;
		}

		return $query->whereIn('id', $ids);
	}

	public function scopeFilterByRole($query)
	{
		$user = Auth::user();

		if ($user->hasAnyRole([env('ROL_CIF'), env('ROL_FIRMAS')]))
		{
			return $query;
		}

		return $query->where('user_id', $user->id);
	}

	public function scopeDateRange($query, $from, $to)
	{
		return $query->whereBetween('created_at', [$from, $to]);
	}
}
