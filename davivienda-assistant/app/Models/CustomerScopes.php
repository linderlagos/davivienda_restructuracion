<?php

namespace App\Models;

/**
 * Trait CustomerScopes
 * @package App\Models
 */
trait CustomerScopes
{
    /**
     * Get customer by id saved in session
     *
     * @param $query
     * @param $label
     * @return mixed
     */
    public function scopeRecent($query, $label)
    {
        return $query->where('id', session()->get($label));
    }

	/**
	 * Get customer by identifier and random number
	 *
	 * @param $query
	 * @param $identifier
	 * @param $random
	 * @return mixed
	 */
	public function scopeFindData($query, $identifier, $random)
	{
		return $query->where('identifier', $identifier)->where('random', $random);
	}
}