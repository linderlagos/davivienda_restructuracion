<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Flow
 * @package App\Models
 */
class Flow extends Model
{
	/**
	 * @var string
	 */
	protected $connection = 'customers';

	/**
	 * @var string
	 */
	protected $table = 'flows';

	/**
	 * Protect fields for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
		'created_at',
		'updated_at'
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'customer_information' => 'array',
		'product_information' => 'array',
		'user_information' => 'array',
		'error' => 'array'
	];

	/**
	 * A product flow belongs to a customer
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}

	/**
	 * A product flow may belong to a parent product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	/**
	 * A product flow may have many quotations
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quotations()
	{
		return $this->hasMany(Quotations::class);
	}

	/**
	 * Get product flow by id saved in session
	 *
	 * @param $query
	 * @param $productName
	 * @return mixed
	 */
	public function scopeRecent($query, $productName)
	{
		return $query->where('id', session()->get($productName . '_id'));
	}
}
