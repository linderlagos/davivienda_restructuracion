<?php
// app/Extensions/MongoUserProvider.php
namespace App\Extensions;

use App\Core\Bridge;
use App\Core\Parameters;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class ActiveDirectoryProvider implements UserProvider
{
//	/**
//	 * The Mongo User Model
//	 */
//	private $model;
//
//	/**
//	 * Create a new mongo user provider.
//	 *
//	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
//	 * @return void
//	 */
//	public function __construct(User $userModel)
//	{
//		$this->model = $userModel;
//	}

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
//	public function retrieveByCredentials(array $credentials)
//	{
//		if (empty($credentials)) {
//			return;
//		}
//
//		$user = $this->model->fetchUserByCredentials(['peopleSoft' => $credentials['peopleSoft']]);
//
//		return $user;
//	}

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     */
    public function retrieveByCredentials(array $credentials)
    {
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials  Request credentials
     * @throws \Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $response = $this->bridge()->soap(
        	'HN',
	        'VALIDATE_USER_PROCESS',
	        $this->parameters()->user($credentials['peopleSoft'], $credentials['password'])
        );

        if ($this->error($response)) {
            return false;
        }

        return true;
    }

    public function retrieveById($identifier)
    {
    }

    public function retrieveByToken($identifier, $token)
    {
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
    }

    private function bridge()
    {
        return new Bridge();
    }

    private function parameters()
    {
        return new Parameters();
    }

    /**
     * If error is thrown by WSDL get the error and show it to the customer
     *
     * @param $response
     * @param $step
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    private function error($response, $step = null)
    {
        $error = (int) $response->getCreditCardProcessResult()->Data->Error;

        if ($error > 0) {
            return true;
        }

        return false;
    }
}
