<?php

namespace App\Http\Controllers\Insurance\SafeFamily;

use App\Http\Requests\Account\UploadRequest;
use App\Mail\Account\SendContract;
use App\Mail\SendPDFLink;
use App\Models\Customer;
use App\Models\Insurance\SafeFamily\SafeFamilyPrintable;
use App\Models\Printable\Printable;
use App\Models\Product;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;

/**
 * Class AccountPrintableController
 * @package App\Http\Controllers\Account
 */
class SafeFamilyPrintableController extends Controller
{
	/**
	 * Show printable policy document
	 *
	 * @param $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function policy($id)
    {
    	$print = Product::findOrFail($id);

    	$data = $print->customer_information;

    	$beneficiaries = get_json($print->customer_information, 'beneficiaries');

    	$premium = get_json($print->product_information, ['plans'])[get_json($print->product_information, ['amount', 'code'])][get_json($print->product_information, ['payment_type', 'value'])];

    	$date = Date::createFromTimestamp($print->created_at->timestamp)->format('d \d\e F \d\e Y');

    	return view('insurance.safeFamily.printable.' . $print->version . '.policy', compact('print', 'data', 'beneficiaries', 'premium', 'date'));
    }
}
