<?php

namespace App\Http\Controllers;

// Traits
use App\Core\Traits\Message;
use App\Core\Traits\PersistId;
use App\Core\Traits\FlowIsActive;
use App\Core\Traits\ProductParameters;
use App\Core\Traits\GetOrCreateCustomer;

// Core
use App\Core\SearchCustomer;
use App\Core\CustomerRegistry;

// Helpers
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class CoreController
 * @package App\Http\Controllers
 */
class SearchController extends CustomerRegistry
{
	use ProductParameters,
		GetOrCreateCustomer,
		PersistId,
		Message,
		FlowIsActive;

	public function search(Request $request, $productName)
	{
		$productParameters = $this->productParameters($productName);

		$request->validate($productParameters['search_rules']);

		$activeFlow = $this->flowIsActive($productParameters['active_flows']);

		if ($activeFlow)
		{
			$activeFlowParameters = $this->productParameters($activeFlow);

			return redirect()->to($activeFlowParameters['home_route']);
		}

		$search = new SearchCustomer();

		$searchCustomer = $search->searchCustomer(
			$request->identity,
			$productParameters['company'],
			$productParameters['search_process'],
			$productName
		);

		// If response could not be made redirect back
		if (in_array($searchCustomer['code'], [403, 500]))
		{
			return redirect()->back();
		}

		// Create product flow with retrieved info
		$flow = $this->store($request, $searchCustomer['response'], $productName);

		// Persist flow id in session
		$this->persistId($productName, $flow->id);

		return redirect()->to($productParameters['home_route']);
	}

	/**
	 * Store new resource in customer table
	 *
	 * @param $request
	 * @param $response
	 * @param $productName
	 * @return mixed
	 */
	public function store($request, $response, $productName)
	{
		$customer = $this->getOrCreateCustomer($request->identity);

		if ($productName === 'bank_account')
		{
			$productInformation = $this->firstBankAccountProductInformation($response);
		} else {
			$productInformation = $this->firstBankCardProductInformation($request, $response);
		}

		$customerInformation = $this->firstCustomerInformation($response, $productName);

		$customerStatus = 0;

		if ($customerInformation['name']['first'])
		{
			$customerStatus = 1;
		}

		return $customer->flows()->create([
			'customer_information' => $customerInformation,
			'product_information' => $productInformation,
			'user_information' => $this->firstUserInformation(),
			'user_id' => Auth::user()->id,
			'step' => '1',
			'type' => $productName,
			'customer_status' => $customerStatus
		]);
	}

	private function firstCustomerInformation($response, $productName)
	{
		return [
			'nationality' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Nacionalidad'), 'nations'),

			// CIF
			'cif' => [
				'code' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TipoCif'),
				'value' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CIF'),
			],

			//Address
			'address' => [
				'code' => null,
				'other_city' => null,
				'state' => null,
				'state_code' => null,
				'municipality' => null,
				'municipality_code' => null,
				'colony' => null,
				'colony_code' => null,
				'full' => $this->constructFullAddress($response, 'DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa', 'DireccionDomicilioCompletaPuntoReferencia', 'DireccionDomicilioCompletaPuntoReferenciaComplemento'),
				'first' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa'),
				'second' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaPuntoReferencia'),
				'third' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaPuntoReferenciaComplemento'),
			],

			'phone' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TelefonoDomicilio', true),
			'mobile' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Celular'),
			'email' => $this->checkIfCustomerHasEmail($response),

			'receive_sms' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'AutorizaEnvioMensajesCelular'), 'confirmation'),

			'receive_emails' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'AutorizaEnvioEstadosCuentaCorreoPersonal'), 'confirmation'),

			'marital_status' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'EstadoCivil'), 'maritalStatusOptions'),

			'spouse' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'ConyugeNombre'),

			'birth' => $this->generateDate('Ymd', $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'FechaNacimientoAAAAMMDD')),

			'gender' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Sexo'), 'genderOptions'),

			'profession' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Profesion'), 'professionOptions'),

			// Name
			'name' => [
				'first' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerNombre'),
				'middle' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoNombre'),
				'last' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerApellido'),
				'second_last' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoApellido'),
				'fullname' => $this->fullname(
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerNombre'),
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoNombre'),
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerApellido'),
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoApellido')
				)
			],

			// Employer
			'employer' => [
				'started_at' => $this->generateDate('Ymd', $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'FechaIngresoLaboralAAAAMMDD')),
				'name' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'NombreEmpleadorLugarTrabajo'),
				'phone' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TelefonoEmpleadorLaboral'),
				'type' => [
					'code' => null,
					'value' => null
				],
				'address' => [
					'code' => null,
					'other_city' => null,
					'state' => null,
					'state_code' => null,
					'municipality' => null,
					'municipality_code' => null,
					'colony' => null,
					'colony_code' => null,
					'full' => $this->constructFullAddress($response, 'DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa', 'DireccionEmpleadorCompletaPuntoReferencia', 'DireccionEmpleadorCompletaPuntoReferenciaComplemento'),
					'first' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa'),
					'second' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaPuntoReferencia'),
					'third' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaPuntoReferenciaComplemento'),
				],
				'job' => [
					'name' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CargoLaboralPuestoTrabajo'),

					'type' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'ActividadComercial'), 'jobTypeOptions'),

					'status' => [
						'code' => null,
						'value' => null
					],
				],
			],

			'income' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'IngresoMensual', true),
			'other_income' => null,

			'expenses' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CodigoRangoTotalEgresos'), 'expenseRangeOptions'),

			'assets' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CodigoRangoTotalActivos'), 'assetRangeOptions'),

			'passive' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CodigoRangoTotalPasivos'), 'liabilityRangeOptions'),

			'fatca' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'EsFatca'), 'confirmation'),

			// Public job
			'public_job' => [
				'code' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CargosPublicosUltimos4Anhos'),

				'value' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CargosPublicosUltimos4Anhos'), 'confirmation', false),

				'employer_name' => $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoInstitucion'),

				'job' => $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoCargo'),

				'active' => $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoEsActual'),
				'active_value' => $this->searchValueFromList($productName, $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoEsActual'), 'confirmation', false),

				'from' => $this->generateDate('Ymd', $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoFechaInicio')),
				'to' => $this->generateDate('Ymd', $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoFechaFinal')),
			],

			'dependants' => $this->dependants($response, $productName),
			'professional_dependant' => $this->storedProfessionalDependants($response),
			'international_operations_information' => $this->storedInternationalOperationsInformation($response, $productName),
			'beneficiaries' => $this->constructJson($response, 'storedBeneficiaries', 10, $productName),
			'references' => $this->constructJson($response, 'storedReferences', 2, $productName),
			'national_institutions' => $this->constructJson($response, 'storedNationalInstitutions', 3, $productName),
			'foreign_institutions' => $this->constructJson($response, 'storedForeignInstitutions', 3, $productName),
		];
	}

	private function firstBankAccountProductInformation($response)
	{
		return [
			'product' => [
				'code' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'TipoCuenta'),
				'value' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'NumeroCuenta'),
			]
		];
	}

	private function firstBankCardProductInformation($request, $response)
	{
		$productParameters = $this->productParameters('bank_card');

		return [
			'product' => [
				'code' => $productParameters['cards'][$request->card]['code'],
				'name' => $productParameters['cards'][$request->card]['name'],
				'limit' => null,
				'type' => null,
			],
			'request' => [
				'number' => null
			]
		];
	}

	private function firstUserInformation()
	{
		return [
			'peoplesoft' => Auth::user()->peopleSoft,
		];
	}

	private function checkIfCustomerHasEmail($response)
	{
		$email = $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CorreoPersonal');
		$hasEmail = 'N';

		if ($email) {
			$hasEmail = 'S';
		}

		return [
			'account' => $email,
			'has_email' => $hasEmail
		];
	}
}