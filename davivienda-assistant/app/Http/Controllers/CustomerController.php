<?php

namespace App\Http\Controllers;

use App\Models\Customer;

class CustomerController extends Controller
{
	public function show($id)
	{
		$customer = Customer::findOrFail($id);

		if ($customer->products()->count() === 0)
		{
			$this->errorMessage('Este cliente no tiene productos por este flujo');

			return redirect()->route('home');
		}

		return view('customer.show', compact('customer'));
	}
}