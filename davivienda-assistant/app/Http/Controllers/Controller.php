<?php

namespace App\Http\Controllers;

// Core
use App\Core\Traits\AuditParameters;
use App\Core\Traits\Bridge;
use App\Core\Traits\FilterProducts;
use App\Core\Traits\GetOrCreateCustomer;
use App\Core\Traits\GetValueFromResponse;
use App\Core\Traits\Message;
use App\Core\Traits\HasData;
use App\Core\Traits\ForgetId;
use App\Core\Traits\GetLists;
use App\Core\Traits\PersistId;
use App\Core\Traits\RecentFlow;
use App\Core\Traits\RedirectIfResponseHasError;
use App\Core\Traits\UpdateStep;
use App\Core\Traits\FlowIsActive;
use App\Core\Traits\LogException;
use App\Core\Traits\ProductParameters;
use App\Core\Traits\StorePrintableProduct;

// Foundation
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
	use AuthorizesRequests,
		DispatchesJobs,
		ValidatesRequests,
		UpdateStep,
		GetLists,
		RecentFlow,
		FlowIsActive,
		RedirectIfResponseHasError,
		HasData,
		Bridge,
		PersistId,
		ForgetId,
		StorePrintableProduct,
        GetOrCreateCustomer,
		AuditParameters,
		GetValueFromResponse,
		FilterProducts;
}
