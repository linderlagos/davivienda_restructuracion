<?php

namespace App\Http\Controllers\Bank\Account;

use App\Http\Requests\Bank\Account\UploadRequest;
use App\Mail\Account\SendContract;
use App\Models\Printable\Printable;
use App\Models\Product;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;

/**
 * Class AccountPrintableController
 * @package App\Http\Controllers\Account
 */
class AccountPrintableController extends Controller
{
	/**
	 * Show printable signature document
	 *
	 * @param $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function signature($id)
    {
    	$print = Product::where('type', 'bank_account')->findOrFail($id);

    	return view('bank.account.printable.' . $print->version . '.signature', compact('print'));
    }

	/**
	 * Show printable contract document
	 *
	 * @param $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function contract($id)
	{
		$print = Product::where('type', 'bank_account')->findOrFail($id);

		$yearsInLetters = $this->yearsInLetters();

		$month = Date::createFromTimestamp($print->created_at->timestamp)->format('F');

		return view('bank.account.printable.' . $print->version . '.contract', compact('print', 'yearsInLetters', 'month'));
	}

	/**
	 * Show printable cover document
	 *
	 * @param $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function cover($id)
	{
		$print = Product::where('type', 'bank_account')->findOrFail($id);

		$safeFamily = $print->children()->where('type', 'insurance_safe_family')->first();

		return view('bank.account.printable.' . $print->version . '.cover', compact('print', 'safeFamily'));
	}

	public function yearsInLetters()
	{
		return [
			'2018' => 'dos mil dieciocho',
			'2019' => 'dos mil diecinueve',
			'2020' => 'dos mil veinte',
			'2021' => 'dos mil veintiuno',
			'2022' => 'dos mil veintidos',
			'2023' => 'dos mil veintitres',
			'2024' => 'dos mil veinticuatro',
			'2025' => 'dos mil veinticinco',
		];
	}
}
