<?php

namespace App\Http\Controllers;

use App\Core\ExitFlow;
use Illuminate\Http\Request;

/**
 * Class CoreController
 * @package App\Http\Controllers
 */
class ExitController extends Controller
{
	/**
	 * Exit process and delete request on core
	 *
	 * @param Request $request
	 * @param $productName
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function exit(Request $request, $productName)
	{
		$productParameters = $this->productParameters($productName);

		$validatedData = $request->validate($productParameters['exit_rules']);

		$flow = $this->recentFlow($productName, true);

		$exit = new ExitFlow();

		$exitProcess = $exit->exit($flow, $productName, $request);

		if (in_array($exitProcess['code'], [403, 500]))
		{
			return $this->redirectIfResponseHasError($exitProcess['code'], $productName);
		}

		return redirect()->to($productParameters['home_route']);
	}
}