<?php

namespace App\Http\Controllers;

use App\Models\Flow;

/**
 * Class CoreController
 * @package App\Http\Controllers
 */
class ReturnController extends Controller
{
	public function return($flowId)
	{
		$flow = Flow::findOrFail($flowId);

		$productParamenters = $this->productParameters($flow->type);

		$this->updateStep($flow, 'previous');

		return redirect()->to($productParamenters['home_route']);
	}
}