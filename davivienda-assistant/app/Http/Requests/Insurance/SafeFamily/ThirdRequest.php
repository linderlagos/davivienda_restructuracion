<?php

namespace App\Http\Requests\Insurance\SafeFamily;

use App\Rules\hn\RequiredIfAndWithAll;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ApprovalProcessRequest
 * @package App\Http\Requests
 */
class ThirdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return $this->generalRules() + [
	        'second_md_5' => [
		        'required'
	        ],
	        'second_md_5_accident' => [
		        'required_if:second_md_5,S',
		        'max:25'
	        ],
	        'second_md_5_sequel' => [
		        'required_if:second_md_5,S',
		        'max:25'
	        ],
	        'second_md_5_date' => [
		        'required_if:second_md_5,S'
	        ],
	        'second_md_5_doctor' => [
		        'required_if:second_md_5,S',
		        'max:40'
	        ],
	        'second_md_5_hospital' => [
		        'required_if:second_md_5,S',
		        'max:40'
	        ],
//	        'second_md_5_other_hospital' => [
//	        	new RequiredIfAndWithAll('second_md_5_hospital', 'Otro', ['second_md_5']),
//	        ]
        ];
    }

    private function generalRules()
    {
    	$rules = [];

    	for ($i = 1; $i <= 4; $i++)
	    {
	    	$rules += [
	    		'second_md_' . $i => [
	    			'required'
			    ],
//			    'second_md_' . $i . '_diagnostic' => [
//			    	'required_if:second_md_' . $i . ',S'
//			    ],
			    'second_md_' . $i . '_date' => [
				    'required_if:second_md_' . $i . ',S'
			    ],
			    'second_md_' . $i . '_doctor' => [
				    'required_if:second_md_' . $i . ',S',
				    'max:40'
			    ],
			    'second_md_' . $i . '_hospital' => [
				    'required_if:second_md_' . $i . ',S',
				    'max:20'
			    ],
//			    'second_md_' . $i . '_other_hospital' => [
//				    new RequiredIfAndWithAll('second_md_' . $i . '_hospital', 'Otro', ['second_md_' . $i]),
//			    ]
		    ];
	    }

    	return $rules;
    }
}
