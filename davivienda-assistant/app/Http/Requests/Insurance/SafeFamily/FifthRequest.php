<?php

namespace App\Http\Requests\Insurance\SafeFamily;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ApprovalProcessRequest
 * @package App\Http\Requests
 */
class FifthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return $this->beneficiariesRules(1,5);
    }

	private function beneficiariesRules($start, $quantity)
	{
		$data = [];

		for ($i = $start; $i <= $quantity; $i++)
		{
			$data += [
				'beneficiary_' . $i . '_relationship' => [
					'sometimes',
					'required_with:beneficiary_' . $i . '_name',
				],
				'beneficiary_' . $i . '_participation' => [
					'sometimes',
					'required_with:beneficiary_' . $i . '_name',
					'max:6'
				]
			];
		}

		$data += [
			'beneficiary_1_name' => [
				'required',
				'max:40'
			],
			'beneficiary_2_name' => [
				'sometimes',
				'required_with:beneficiary_3_name,beneficiary_4_name,beneficiary_5_name',
				'max:40'
			],
			'beneficiary_3_name' => [
				'sometimes',
				'required_with:beneficiary_4_name,beneficiary_5_name',
				'max:40'
			],
			'beneficiary_4_name' => [
				'sometimes',
				'required_with:beneficiary_5_name',
				'max:40'
			],
			'beneficiary_5_name' => [
				'sometimes',
				'max:40'
			]
		];

		return $data;
	}
}
