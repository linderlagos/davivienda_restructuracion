<?php

namespace App\Http\Requests\Bank\Auto;

// Core
use App\Core\Traits\RecentFlow;
use App\Rules\CheckIfAdult;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\NumDash;
use App\Rules\Amount;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\hn\NameLength;
use App\Rules\MinIncome;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\CheckPublicJobToDate;
use App\Rules\CheckPublicJobFromDate;
use App\Rules\RequiredIfOtherCity;
use App\Rules\RequiredIfResourceNull;
use App\Rules\CheckIfDayBelongsToMonth;
use App\Rules\CheckPublicJobToDateToday;

// Models
use App\Models\Account\Customer;

class ThirdRequest extends FormRequest
{
    use RecentFlow;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {


        return [

        ];


    }
}
