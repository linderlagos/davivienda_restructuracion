<?php

namespace App\Http\Requests\Bank\Auto;

// Core
use App\Rules\CheckIfAdult;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\NumDash;
use App\Rules\Amount;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\hn\NameLength;
use App\Rules\MinIncome;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\CheckPublicJobToDate;
use App\Rules\CheckPublicJobFromDate;
use App\Rules\RequiredIfOtherCity;
use App\Rules\RequiredIfResourceNull;
use App\Rules\CheckIfDayBelongsToMonth;
use App\Rules\CheckPublicJobToDateToday;

// Models
use App\Models\Account\Customer;

class CalculateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'concessionaire' => [
                'required'
            ],
            'seller' => 'required',
            'type_auto' => 'required',
            'auto_brand' => 'required',
            'auto_model' => [
                'required',
                'max:30'
            ],
            'status_auto' => 'required',
            'year_auto' => 'required',
            'sale_price' => [
                'required',
                'max:11',
                new MinIncome(),
                new Amount(),
            ],
            'appraisal' => [
                'sometimes',
                'max:11',
            ],
            'premium' => [
                'sometimes',
                'required',
                'max:9',
            ],
            'financing_term' => 'required|max:2',
            'closing_costs' => 'required|max:2'
        ];
    }
}
