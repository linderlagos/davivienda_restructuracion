<?php

namespace App\Http\Requests\Bank\Account;

// Core
use App\Rules\CheckIfAdult;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\NumDash;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\hn\NameLength;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\RequiredIfOtherCity;
use App\Rules\CheckIfDayBelongsToMonth;

class SecondRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
        	'account_type' => [
        		'required'
	        ],
            'first_name' => [
                'sometimes',
                'required',
                'alpha',
                'max:15',
                new NameLength(),
                new RepeatedCharacters('2')
            ],
            'middle_name' => [
                'nullable',
                new AlphaSpace(),
                'max:15',
                new RepeatedCharacters('2')
            ],
            'last_name' => [
                'sometimes',
                'required',
                'max:15',
                new AlphaSpace(),
                new RepeatedCharacters('2')
            ],
            'second_last_name' => [
                'nullable',
                'max:15',
                new AlphaSpace(),
                new RepeatedCharacters('2')
            ],
            'nationality' => 'sometimes|required|max:5',
	        'job_type' => 'sometimes|required|max:4',
	        'fatca' => [
		        'sometimes',
		        'required'
	        ],
        ];
    }
}
