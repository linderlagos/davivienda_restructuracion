<?php

namespace App\Http\Requests\Bank\Account;

// Core
use App\Rules\CheckIfAdult;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\NumDash;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\hn\NameLength;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\RequiredIfOtherCity;
use App\Rules\CheckIfDayBelongsToMonth;

class ThirdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'day' => [
                'sometimes',
                'required',
                new CheckIfDayBelongsToMonth('day', 'month')
            ],
            'month' => [
                'sometimes',
                'required',
                new CheckIfFutureDate('day', 'month', 'year'),
	            new CheckIfAdult('day', 'month', 'year', 18)
            ],
            'year' => 'sometimes|required',
            'phone' => [
	            'nullable',
                new NumDash(),
                new FirstNumber([2,3,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],
            'mobile' => [
                'sometimes',
                'required',
                new NumDash(),
                new FirstNumber([3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],

            'city' => [
                'required',
            ],
            'other_city' => [
                'max:40',
                new RequiredIfOtherCity('city', 'colonia de domicilio'),
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],
            'address' => [
                'sometimes',
                'required',
                'max:120',
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],
	        'email' => 'sometimes|required|email|max:40',
	        'gender' => 'sometimes|required|max:1',
	        'marital_status' => 'sometimes|required|max:1',
//			'bureau' => 'required',
//		    'g-recaptcha-response' => 'required|captcha'
        ];
    }
}
