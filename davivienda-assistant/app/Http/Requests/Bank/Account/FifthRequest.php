<?php

namespace App\Http\Requests\Bank\Account;

// Core
use App\Rules\hn\CheckIdentificationByType;
use App\Rules\hn\RealId;
use App\Rules\hn\RequiredIfRelationship;
use App\Rules\SameValue;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\AlphaSpace;
use App\Rules\FirstNumber;
use App\Rules\NumDash;
use App\Rules\PhoneLength;
use App\Rules\RepeatedCharacters;
use App\Rules\SameDigitInput;

class FifthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'beneficiary_1_name' => [
        		'sometimes',
        		'required'
	        ],
	        'beneficiary_1_relationship' => [
		        'sometimes',
		        'required_unless:beneficiary_1_name,NO TIENE',
	        ],
	        'beneficiary_1_id_type' => [
		        'sometimes',
		        new RequiredIfRelationship('beneficiary_1_relationship'),
	        ],
	        'beneficiary_1_identity' => [
		        'sometimes',
		        new RequiredIfRelationship('beneficiary_1_relationship'),
//		        new CheckIdentificationByType('beneficiary_1_id_type'),
		        new RealId()
	        ],
        	'reference_1_name' => [
		        'sometimes',
		        'required',
		        new RepeatedCharacters('2'),
		        new AlphaSpace(),
		        new SameValue('reference_2_name')
	        ],
//	        'reference_1_relationship' => [
//		        'required',
//		        'sometimes'
//	        ],
	        'reference_1_mobile' => [
		        'nullable',
		        new NumDash(),
		        new FirstNumber([3,7,8,9]),
		        new SameDigitInput(),
		        new PhoneLength(8),
		        'required_without:reference_1_phone',
		        new SameValue('reference_2_mobile')
	        ],
	        'reference_1_phone' => [
		        'nullable',
		        new NumDash(),
		        new FirstNumber([2,3,8,9]),
		        new SameDigitInput(),
		        new PhoneLength(8),
		        'required_without:reference_1_mobile',
		        new SameValue('reference_2_phone')
	        ],
//	        'reference_1_work_phone' => [
//		        'nullable',
//		        new NumDash(),
//		        new FirstNumber([2,3,8,9]),
//		        new SameDigitInput(),
//		        new PhoneLength(8),
//		        'required_without_all:reference_1_phone,reference_2_phone'
//	        ],
	        'reference_2_name' => [
		        'sometimes',
		        'required',
		        new RepeatedCharacters('2'),
		        new AlphaSpace(),
		        new SameValue('reference_1_name')
	        ],
	        'reference_2_mobile' => [
		        'nullable',
		        new NumDash(),
		        new FirstNumber([3,7,8,9]),
		        new SameDigitInput(),
		        new PhoneLength(8),
		        'required_without:reference_2_phone',
		        new SameValue('reference_1_mobile')
	        ],
	        'reference_2_phone' => [
		        'nullable',
		        new NumDash(),
		        new FirstNumber([2,3,8,9]),
		        new SameDigitInput(),
		        new PhoneLength(8),
		        'required_without:reference_2_mobile',
		        new SameValue('reference_1_phone')
	        ]
//	        'reference_2_work_phone' => [
//		        'nullable',
//		        new NumDash(),
//		        new FirstNumber([2,3,8,9]),
//		        new SameDigitInput(),
//		        new PhoneLength(8),
//		        'required_without_all:reference_2_phone,reference_1_phone,reference_1_work_phone'
//	        ],
        ] + $this->beneficiariesRules(2, 10);
    }

    private function beneficiariesRules($start, $quantity)
    {
    	$data = [];

    	for ($i = $start; $i <= $quantity; $i++)
	    {
//	    	$next = [
//			    'sometimes',
//			    'required_without_all:beneficiary_' . ($i - 1) . '_name'
//		    ];
//
//		    if ($i === $quantity)
//		    {
//		    	$next = [
//				    'sometimes'
//			    ];
//		    }

	    	$data += [
//			    'beneficiary_' . $i . '_name' => $next,
			    'beneficiary_' . $i . '_relationship' => [
				    'sometimes',
				    'required_with:beneficiary_' . $i . '_name',
			    ],
			    'beneficiary_' . $i . '_id_type' => [
				    'sometimes',
				    new RequiredIfRelationship('beneficiary_' . $i . '_relationship'),
			    ],
			    'beneficiary_' . $i . '_identity' => [
				    'sometimes',
				    new RequiredIfRelationship('beneficiary_' . $i . '_relationship'),
//				    new CheckIdentificationByType('beneficiary_' . $i . '_id_type'),
				    new RealId()
			    ],
		    ];
	    }

	    $data += [
	    	'beneficiary_2_name' => [
	    		'sometimes',
			    'required_with:beneficiary_3_name,beneficiary_4_name,beneficiary_5_name,beneficiary_6_name,beneficiary_7_name,beneficiary_8_name,beneficiary_9_name,beneficiary_10_name'
		    ],
		    'beneficiary_3_name' => [
			    'sometimes',
			    'required_with:beneficiary_4_name,beneficiary_5_name,beneficiary_6_name,beneficiary_7_name,beneficiary_8_name,beneficiary_9_name,beneficiary_10_name'
		    ],
		    'beneficiary_4_name' => [
			    'sometimes',
			    'required_with:beneficiary_5_name,beneficiary_6_name,beneficiary_7_name,beneficiary_8_name,beneficiary_9_name,beneficiary_10_name'
		    ],
		    'beneficiary_5_name' => [
			    'sometimes',
			    'required_with:beneficiary_6_name,beneficiary_7_name,beneficiary_8_name,beneficiary_9_name,beneficiary_10_name'
		    ],
		    'beneficiary_6_name' => [
			    'sometimes',
			    'required_with:beneficiary_7_name,beneficiary_8_name,beneficiary_9_name,beneficiary_10_name'
		    ],
		    'beneficiary_7_name' => [
			    'sometimes',
			    'required_with:beneficiary_8_name,beneficiary_9_name,beneficiary_10_name'
		    ],
		    'beneficiary_8_name' => [
			    'sometimes',
			    'required_with:beneficiary_9_name,beneficiary_10_name'
		    ],
		    'beneficiary_9_name' => [
			    'sometimes',
			    'required_with:beneficiary_10_name'
		    ],
		    'beneficiary_10_name' => [
			    'sometimes',
		    ],
	    ];

    	return $data;
    }
}
