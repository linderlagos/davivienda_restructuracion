<?php

Route::get('/navegador-incompatible', 'CoreController@incompatibleBrowser')->name('incompatibleBrowser');

Route::get('/', 'CoreController@index')->name('home')->middleware(['block_users']);

Route::get('/banco/generar-lista', 'CoreController@dynamicList')->name('get.dynamic.list')->middleware(['role:' . env('ROL_GESTOR')]);

//Route::get('/prueba-envio-de-correo', 'CoreController@emailTest')->name('test.email');

Route::post('/buscar-cliente/{productName}', 'SearchController@search')->name('search.customer')->middleware(['role:' . env('ROL_GESTOR')]);

Route::post('/salir/{productName}', 'ExitController@exit')->name('exit.flow')->middleware(['role:' . env('ROL_GESTOR')]);

Route::post('/regresar-un-paso/{flow}', 'ReturnController@return')->name('return.step.in.flow')->middleware(['role:' . env('ROL_GESTOR')]);

Route::get('/otros-productos/{product}', 'CrossSellingController@index')->name('cross.sell.index')->middleware(['role:' . env('ROL_GESTOR')]);