<?php

//    Auth::routes();
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('/2fa', function () {
//		return redirect(URL()->previous());
		return redirect()->route('home');
	})->name('2fa')->middleware('2fa');
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');