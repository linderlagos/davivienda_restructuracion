<?php

/*
|--------------------------------------------------------------------------
| Card Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Card Process routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" and "auth" middleware group.
|
*/

Route::namespace('Insurance')->prefix('seguros')->group(function ()
{
	Route::namespace('SafeFamily')->prefix('familia-segura')->group(function ()
	{
		Route::get('/', 'SafeFamilyController@index')->name('insurance.safe.family.index')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/', 'SafeFamilyController@search')->name('insurance.safe.family.search')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/2', 'SafeFamilyController@second')->name('insurance.safe.family.second')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/3', 'SafeFamilyController@third')->name('insurance.safe.family.third')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/4', 'SafeFamilyController@fourth')->name('insurance.safe.family.fourth')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/5', 'SafeFamilyController@fifth')->name('insurance.safe.family.fifth')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/6', 'SafeFamilyController@sixth')->name('insurance.safe.family.sixth')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::get('/imprimir-poliza/{id}', 'SafeFamilyPrintableController@policy')->name('insurance.safe.family.print.policy')->middleware(['role:' . env('ROL_GESTOR') . '|' . env('ROL_CIF')]);

		Route::post('/producto-base/{id}', 'SafeFamilyController@first')->name('insurance.safe.family.first');
	});
});