<?php

/*
|--------------------------------------------------------------------------
| Account Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Account Process routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" and "auth" middleware group.
|
*/

Route::namespace('Bank')->prefix('banco')->group(function ()
{
	Route::namespace('Account')->prefix('cuenta-de-ahorro')->group(function ()
	{
		Route::get('/', 'AccountController@index')->name('bank.account.index')->middleware(['block_users', 'role:' . env('ROL_GESTOR')]);

		Route::post('/2', 'AccountController@second')->name('bank.account.second')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/3', 'AccountController@third')->name('bank.account.third')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/4', 'AccountController@fourth')->name('bank.account.fourth')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/5', 'AccountController@fifth')->name('bank.account.fifth')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/6', 'AccountController@sixth')->name('bank.account.sixth')->middleware(['role:' . env('ROL_GESTOR')]);
		Route::get('/imprimir-firma/{id}', 'AccountPrintableController@signature')->name('bank.account.print.signature')->middleware(['role:' . env('ROL_GESTOR') . '|' . env('ROL_CIF') . '|' . env('ROL_FIRMAS')]);

		Route::get('/imprimir-contrato/{id}', 'AccountPrintableController@contract')->name('bank.account.print.contract')->middleware(['role:' . env('ROL_GESTOR') . '|' . env('ROL_CIF')]);

		Route::get('/imprimir-caratula/{id}', 'AccountPrintableController@cover')->name('bank.account.print.cover')->middleware(['role:' . env('ROL_GESTOR') . '|' . env('ROL_CIF')]);
	});
});