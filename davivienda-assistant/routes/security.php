<?php

Route::prefix('seguridad')->middleware(['role:' . env('ROL_SEGURIDAD')])->group(function ()
{
	Route::get('/', 'SecurityController@index')->name('security.index');

	Route::get('/usuario/{user}', 'SecurityController@user')->name('security.user');

	Route::post('/usuario/{user}', 'SecurityController@generateNewSecretKey')->name('security.generate.new.secret.key');

});