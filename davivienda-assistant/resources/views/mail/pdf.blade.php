@extends('layouts.mail')

@section('title')
    Hola {{ $name }}
@stop

@section('content')
    {!! $content !!}

    <a href="{{ $url }}" class="btn" target="_blank" style="color: #fff; text-decoration: none;">
        Generar PDF
    </a>
@stop