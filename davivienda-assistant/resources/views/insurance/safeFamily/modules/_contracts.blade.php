<div class="row">
    <div class="col-12 form-group">
        <h2><i class="fas fa-print"></i> Seleccione el documento a generar:</h2>
    </div>
</div>

<div class="row" style="margin-bottom: 2rem">
    <div class="col-6 col-lg-4">
        <a href="#" class="btn davivienda-file" target="_blank">
            <i class="fas fa-file-signature"></i>
            <p>Póliza</p>
        </a>
    </div>
</div>