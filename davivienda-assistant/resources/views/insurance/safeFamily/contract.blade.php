@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropzone/dropzone.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/safe-family.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Documentos de seguro Familia Segura</h1>
                </div>
                <div class="banner-description">
                    <p>Imprima los documentos del cliente con ID No. {{ $customer->identifier }} y póliza No. X</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop

@section('content')

    @include('insurance.safeFamily.modules._contracts')

@endsection

@section('scripts')
    @include('insurance.safeFamily.partials._contractScripts')
    {{--@include('account.partials._scripts')--}}
@stop
