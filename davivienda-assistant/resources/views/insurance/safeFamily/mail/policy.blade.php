@extends('layouts.mail')

@section('title')
    Hola {{$name}}
@stop

@section('content')
    <p>Para revisar su póliza dele click al siguiente botón:</p>

    <a href="{{ env('PDF_GENERATOR_URL') . $id . '/' .$random }}" class="btn" target="_blank" style="color: #fff; text-decoration: none;">
        Ver póliza
    </a>
@stop