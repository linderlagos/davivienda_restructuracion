@include('modules._progress', [
    'steps' => 5,
    'step' => 2,
    'title' => 'Información médica'
])

<form class="form-horizontal" role="form" method="POST" action="{{ route('insurance.safe.family.third') }}" id="form">
    {!! csrf_field() !!}

    <div class="row align-items-baseline">
        <div class="col-12 form-group">
            <h2>Pregunta No. 1</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user-md"></i>
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Ha padecido de otra enfermedad no mencionada anteriormente?</span>
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_1') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_1',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 1, 'question']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_1">
        <div class="col-2">
        </div>

        <div class="col-10" id="second_md_1_disease_container">
            <div class="form-group{{ $errors->has('second_md_1_disease') ? ' has-error' : '' }}">

                @include('modules._singleSelect', [
                    'name' => 'second_md_1_disease',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 1, 'disease']),
                    'classes' => 'davivienda-select',
                    'placeholder' => 'Seleccione la enfermedad...',
                    'options' => $lists['diseases'],
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-6 show_if_second_md_1_other_disease" id="second_md_1_diagnostic_container">
            <div class="form-group{{ $errors->has('second_md_1_diagnostic') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_1_diagnostic',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 1, 'diagnostic']),
                    'placeholder' => 'Diagnóstico',
                    'label' => 'Diagnóstico',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_1">
        <div class="col-2"></div>

        <div class="col-2">
            <div class="form-group{{ $errors->has('second_md_1_date') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_1_date',
                    'resource' => $flow,
                    'json' => get_json_date(get_json($flow->product_information, ['second_medical_questionary', 1, 'date']), 'd/m/Y'),
                    'placeholder' => 'DD/MM/AAAA',
                    'label' => 'Fecha',
                    'length' => '10',
                    'classes' => 'date',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-4">
            <div class="form-group{{ $errors->has('second_md_1_doctor') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_1_doctor',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 1, 'doctor']),
                    'placeholder' => 'Médico tratante',
                    'label' => 'Médico tratante',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-4">
            <div class="form-group{{ $errors->has('second_md_1_hospital') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_1_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 1, 'hospital']),
                    'classes' => 'davivienda-select',
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['hospitals'],
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_1_other_hospital">
        <div class="col-2"></div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('second_md_1_other_hospital') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_1_other_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 1, 'other_hospital']),
                    'placeholder' => 'Otro hospital o clínica',
                    'label' => 'Otro hospital o clínica',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-12 form-group">
            <h2>Pregunta No. 2</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user-md"></i>
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Ha sido o va a ser intervenido quirúrgicamente?</span>
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_2') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_2',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 2, 'question']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_2">
        <div class="col-2">
        </div>

        <div class="col-7">
            <div class="form-group{{ $errors->has('second_md_2_diagnostic') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_2_diagnostic',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 2, 'diagnostic']),
                    'placeholder' => 'Diagnóstico',
                    'label' => 'Diagnóstico',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_2_date') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_2_date',
                    'resource' => $flow,
                    'json' => get_json_date(get_json($flow->product_information, ['second_medical_questionary', 2, 'date']), 'd/m/Y'),
                    'placeholder' => 'DD/MM/AAAA',
                    'label' => 'Fecha',
                    'length' => '10',
                    'classes' => 'date',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_2">
        <div class="col-2"></div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_2_doctor') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_2_doctor',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 2, 'doctor']),
                    'placeholder' => 'Médico tratante',
                    'label' => 'Médico tratante',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_2_hospital') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_2_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 2, 'hospital']),
                    'classes' => 'davivienda-select',
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['hospitals'],
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_2_other_hospital">
        <div class="col-2"></div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('second_md_2_other_hospital') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_2_other_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 2, 'other_hospital']),
                    'placeholder' => 'Otro hospital o clínica',
                    'label' => 'Otro hospital o clínica',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-12 form-group">
            <h2>Pregunta No. 3</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user-md"></i>
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Se encuentra en tratamiento médico actualmente?</span>
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_3') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_3',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 3, 'question']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_3">
        <div class="col-2">
        </div>

        <div class="col-7">
            <div class="form-group{{ $errors->has('second_md_3_diagnostic') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_3_diagnostic',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 3, 'diagnostic']),
                    'placeholder' => 'Diagnóstico',
                    'label' => 'Diagnóstico',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_3_date') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_3_date',
                    'resource' => $flow,
                    'json' => get_json_date(get_json($flow->product_information, ['second_medical_questionary', 3, 'date']), 'd/m/Y'),
                    'placeholder' => 'DD/MM/AAAA',
                    'label' => 'Fecha',
                    'length' => '10',
                    'classes' => 'date',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_3">
        <div class="col-2"></div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_3_doctor') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_3_doctor',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 3, 'doctor']),
                    'placeholder' => 'Médico tratante',
                    'label' => 'Médico tratante',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_3_hospital') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_3_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 3, 'hospital']),
                    'classes' => 'davivienda-select',
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['hospitals'],
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_3_other_hospital">
        <div class="col-2"></div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('second_md_3_other_hospital') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_3_other_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 3, 'other_hospital']),
                    'placeholder' => 'Otro hospital o clínica',
                    'label' => 'Otro hospital o clínica',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-12 form-group">
            <h2>Pregunta No. 4</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user-md"></i>
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Usted tiene algún impedimento físico?</span>
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_4') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_4',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 4, 'question']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_4">
        <div class="col-2">
        </div>

        <div class="col-7">
            <div class="form-group{{ $errors->has('second_md_4_diagnostic') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_4_diagnostic',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 4, 'diagnostic']),
                    'placeholder' => 'Descríbalo',
                    'label' => 'Descríbalo',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_4_date') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_4_date',
                    'resource' => $flow,
                    'json' => get_json_date(get_json($flow->product_information, ['second_medical_questionary', 4, 'date']), 'd/m/Y'),
                    'placeholder' => 'DD/MM/AAAA',
                    'label' => 'Fecha',
                    'length' => '10',
                    'classes' => 'date',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_4">
        <div class="col-2"></div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_4_doctor') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_4_doctor',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 4, 'doctor']),
                    'placeholder' => 'Médico tratante',
                    'label' => 'Médico tratante',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_4_hospital') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_4_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 4, 'hospital']),
                    'classes' => 'davivienda-select',
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['hospitals'],
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_4_other_hospital">
        <div class="col-2"></div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('second_md_4_other_hospital') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_4_other_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 4, 'other_hospital']),
                    'placeholder' => 'Otro hospital o clínica',
                    'label' => 'Otro hospital o clínica',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-12 form-group">
            <h2>Pregunta No. 5</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user-md"></i>
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Usted ha sufrido algún tipo de accidente?</span>
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('second_md_5') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_5',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 5, 'question']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_5">
        <div class="col-2">
        </div>

        <div class="col-4">
            <div class="form-group{{ $errors->has('second_md_5_accident') ? ' has-error' : '' }}">

                @include('modules._singleSelect', [
                    'name' => 'second_md_5_accident',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 5, 'accident']),
                    'classes' => 'davivienda-select',
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['accidents'],
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-6 show_if_second_md_5_other_accident">
            <div class="form-group{{ $errors->has('second_md_5_other_accident') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_5_other_accident',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 5, 'other_accident']),
                    'placeholder' => 'Describa el accidente',
                    'label' => 'Describa el accidente',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-2 show_if_second_md_5_other_accident"></div>

        <div class="col-4" id="second_md_5_sequel_container">
            <div class="form-group{{ $errors->has('second_md_5_sequel') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_5_sequel',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 5, 'sequel']),
                    'placeholder' => 'Secuelas',
                    'label' => 'Secuelas',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-2" id="second_md_5_date_container">
            <div class="form-group{{ $errors->has('second_md_5_date') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_5_date',
                    'resource' => $flow,
                    'json' => get_json_date(get_json($flow->product_information, ['second_medical_questionary', 5, 'date']), 'd/m/Y'),
                    'placeholder' => 'DD/MM/AAAA',
                    'label' => 'Fecha',
                    'length' => '10',
                    'classes' => 'date',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_5">
        <div class="col-2"></div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_5_doctor') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_5_doctor',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 5, 'doctor']),
                    'placeholder' => 'Médico tratante',
                    'label' => 'Médico tratante',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('second_md_5_hospital') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'second_md_5_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 5, 'hospital']),
                    'classes' => 'davivienda-select',
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['hospitals'],
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline show_if_second_md_5_other_hospital">
        <div class="col-2"></div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('second_md_5_other_hospital') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_md_5_other_hospital',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['second_medical_questionary', 5, 'other_hospital']),
                    'placeholder' => 'Otro hospital o clínica',
                    'label' => 'Otro hospital o clínica',
                    'length' => '100',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                SIGUIENTE
            </button>
        </div>
    </div>
</form>