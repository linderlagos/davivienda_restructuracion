@include('modules._progress', [
    'steps' => 5,
    'step' => 5,
    'title' => 'Revisión de la solicitud'
])

<form class="form-horizontal" role="form" method="POST" action="{{ route('insurance.safe.family.sixth') }}" id="form">
    {!! csrf_field() !!}

    <div class="row" style="margin-bottom: 3rem">
        <div class="col-12">
            <h3 class="review-table-titles">Información del cliente</h3>
        </div>

        <div class="col-12">
            <table class="customer-information-table">
                <tr>
                    <td class="title">Nombre:</td>
                    <td colspan="2" class="description">{{ get_json($flow->customer_information, ['name', 'fullname']) }}</td>
                    <td class="title">CIF:</td>
                    <td class="description">{{ get_json($flow->customer_information, ['cif', 'value']) }}</td>
                    <td colspan="2" class="title">Fecha de Nacimiento:</td>
                    <td colspan="2" class="description">{{ date('d/m/Y', get_json($flow->customer_information, 'birth')) }}</td>
                </tr>

                <tr>
                    <td class="title">Edad:</td>
                    <td class="description">{{ $flow->created_at->format('Y') - date('Y', get_json($flow->customer_information, 'birth')) }}</td>
                    <td class="title">Altura:</td>
                    <td class="description">{{ get_json($flow->customer_information, 'height') }} metros</td>
                    <td class="title">Peso:</td>
                    <td class="description">{{ get_json($flow->customer_information, 'weight') }} libras</td>
                    <td class="title">Masa corporal:</td>
                    <td class="description">{{ number_format(get_json($flow->customer_information, 'imc'),0) }}</td>
                </tr>
                <tr>
                    <td class="title">No. de cuenta:</td>
                    <td colspan="3" class="description">{{ get_json($flow->product_information, ['account', 'value']) }}</td>
                    <td class="title">Tipo de cuenta:</td>
                    <td colspan="3" class="description">{{ get_json($flow->product_information, ['account', 'code']) }}</td>
                </tr>
            </table>
        </div>

        <hr>

        <div class="col-12">
            <h3 class="review-table-titles">Información del plan Familia Segura</h3>
        </div>

        <div class="col-12">
            <table class="product-information-table">
                <tr>
                    <td class="title">Emisión:</td>
                    <td class="description">{{ $todayDate->format('d/m/Y') }}</td>
                    <td class="title">Vigencia desde:</td>
                    <td class="description">{{ $todayDate->add('1 day')->format('d/m/Y') }}</td>
                    <td class="title">Vigencia hasta:</td>
                    <td class="description">{{ $todayDate->add('1 year')->format('d/m/Y') }}</td>
                </tr>

                <tr>
                    <td class="title">Prima anual:</td>
                    <td colspan="2" class="description">{{ get_json($flow->product_information, 'plans')[get_json($flow->product_information, ['amount', 'code'])]['Anual'] }}</td>
                    <td class="title">Prima mensual:</td>
                    <td colspan="2" class="description">{{ get_json($flow->product_information, 'plans')[get_json($flow->product_information, ['amount', 'code'])]['Mensual'] }}</td>
                </tr>
            </table>
            <table class="coverage-information-table">
                <tr>
                    <th>Coberturas</th>
                    <th class="text-right">Valores asegurados</th>
                </tr>

                <tr>
                    <td class="title">Vida:</td>
                    <td class="description">L {{ number_format(get_json($flow->product_information, ['coverage', 1, 'value']), 2) }}</td>
                </tr>

                <tr>
                    <td class="title">Incapacidad total y permanente:</td>
                    <td class="description">L {{ number_format(get_json($flow->product_information, ['coverage', 2, 'value']), 2) }}</td>
                </tr>

                <tr>
                    <td class="title">Enfermedades graves:</td>
                    <td class="description">L {{ number_format(get_json($flow->product_information, ['coverage', 3, 'value']), 2) }}</td>
                </tr>

                <tr>
                    <td class="title">Gastos fúnebres:</td>
                    <td class="description">L {{ number_format(get_json($flow->product_information, ['coverage', 4, 'value']), 2) }}</td>
                </tr>

                <tr>
                    <td class="title">Renta diaria por hospitalización:</td>
                    <td class="description">L {{ number_format(get_json($flow->product_information, ['coverage', 5, 'value']), 2) }}</td>
                </tr>
            </table>
        </div>

        <hr>

        <div class="col-12">
            <h3 class="review-table-titles">Información del asesor</h3>
        </div>

        <div class="col-12">
            <table class="user-information-table" width="100%">
                <tr>
                    <td class="title">Asesor:</td>
                    <td class="description">{{ get_json($flow->user_information, ['agent', 'value']) }}</td>
                    <td class="title">Código del Asesor:</td>
                    <td class="description">{{ get_json($flow->user_information, ['agent', 'code']) }}</td>
                </tr>
                <tr>
                    <td class="title">Agencia:</td>
                    <td class="description">{{ get_json($flow->user_information, ['branch', 'value']) }}</td>
                    <td class="title">Código de la agencia:</td>
                    <td class="description">
                        {{ get_json($flow->user_information, ['branch', 'code']) }}
                        {{--@include('modules._singleSelect', [--}}
                            {{--'name' => 'new_branch',--}}
                            {{--'resource' => $flow,--}}
                            {{--'json' => 'user_information',--}}
                            {{--'parent' => 'branch',--}}
                            {{--'field' => 'code',--}}
                            {{--'disableDefaultValue' => true,--}}
                            {{--'default' => get_json($flow->user_informacion, ['branch', 'code']),--}}
                            {{--'classes' => 'davivienda-select',--}}
                            {{--'placeholder' => 'Seleccione la agencia...',--}}
                            {{--'options' => $lists['branches'],--}}
                            {{--'enable' => true,--}}
                        {{--])--}}
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row align-items-center" style="margin-bottom: 2rem">
        <div class="col-12 form-group">
            <h2>Confirmación</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-signature"></i>
        </div>

        <div class="col-7">
            <div>
                <span class="questions">Confirmo que toda la información ingresada está correcta y deseo contratar el seguro Familia Segura bajo estas condiciones.</span>
            </div>
        </div>

        <div class="col-3">
            <div class="form-group{{ $errors->has('confirmation') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'confirmation',
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true
                ])
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                FINALIZAR
            </button>
        </div>
    </div>
</form>