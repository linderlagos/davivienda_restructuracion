<div class="btn-printables-container">
    @if(isset($sendEmail))
        @if($data['email'] !== 'notiene@davivienda.com.hn')
            <a href="{{ $route }}" class="btn btn-copy-link no-print" id="btn-copy-link">Enviar por correo</a>
        @endif
    @endif

    <button class="btn btn-print no-print" onclick="window.print()">Imprimir</button>
</div>

<script src="{{ mix('js/app.js') }}" type="application/javascript"></script>
<script>
    $(document).ready(function () {
        window.print();
    });
</script>
<script>
    $("#btn-copy-link").on("click",function(e) {
        $(this).html('<i class="fas fa-spinner fa-spin"></i> ENVIANDO...');
    });
</script>