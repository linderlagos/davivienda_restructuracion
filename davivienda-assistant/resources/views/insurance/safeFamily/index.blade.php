@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/insurance_safe_family.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Formulario de Solicitud de Familia Segura</h1>
                </div>
                <div class="banner-description">
                    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
                        <p>Continúe completando la información del cliente {{ get_json($flow->customer_information, ['name', 'fullname']) }} con ID No. {{ $flow->customer->identifier }} y póliza No. {{ get_json($flow->product_information, ['policy', 'working']) }}</p>
                    @else
                        <p>Complete la información del cliente para terminar el proceso de solicitud del seguro Familia Segura</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    @if(checkStep($flow, ['2', '3', '4', '5']))
        @include('modules._return', [
            'id' => $flow->id
        ])
    @elseif(checkStep($flow, ['1']))
        @include('modules._returnToProduct', ['id' => $parent->id])
    @endif
@stop

{{--@section('reject')--}}
    {{--@if(in_array(session()->get('account-step'), ['review', 1,2,3,4]))--}}
        {{--@include('modules._reject', [--}}
            {{--'route' => route('exit.account.process'),--}}
            {{--'options' => [--}}
                {{--'TA01' => 'Asesor'--}}
            {{--]--}}
        {{--])--}}
    {{--@endif--}}
{{--@stop--}}

@section('content')

    @if(checkStep($flow, '1'))
        @include('insurance.safeFamily.form._first')
    @elseif(checkStep($flow, '2'))
        @include('insurance.safeFamily.form._second')
    @elseif(checkStep($flow, '3'))
        @include('insurance.safeFamily.form._third')
    @elseif(checkStep($flow, '4'))
        @include('insurance.safeFamily.form._fourth')
    @elseif(checkStep($flow, '5'))
        @include('insurance.safeFamily.form._fifth')
    @else
        @include('insurance.safeFamily.form._search')
    @endif

@endsection

@section('scripts')
    @include('partials._scripts')
    @include('insurance.safeFamily.partials._scripts')
@stop
