@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Bienvenido al Asistente Davivienda</h1>
                </div>
                <div class="banner-description">
                    <p>Seleccione el producto que desea solicitar para su cliente</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    @if(session()->has('bank_card_id'))
        <a href="{{ route('bank.card.index') }}" class="btn-back">
            Solicitud en trámite <i class="fas fa-forward" style="margin-left: 20px"></i>
        </a>
    @elseif(session()->has('bank_account_id'))
        <a href="{{ route('bank.account.index') }}" class="btn-back">
            Solicitud en trámite <i class="fas fa-forward" style="margin-left: 20px"></i>
        </a>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12 form-group">
            <h2>Seleccione el producto:</h2>
        </div>
    </div>

    <div class="row justify-content-center" style="margin-bottom: 2rem">
        <div class="col-6">
            <a href="{{ route('bank.card.index') }}" class="btn davivienda-file">
                <img src="{{ asset('design/index/bank_card.png') }}">
                <p>Tarjeta de Crédito</p>
            </a>
        </div>

        <div class="col-6">
            <a href="{{ route('bank.account.index') }}" class="btn davivienda-file">
                <img src="{{ asset('design/index/bank_account.png') }}">
                <p>Cuenta de Ahorro</p>
            </a>
        </div>

        @if (env('ENABLE_AUTO_FOR_EVERYONE'))
            <div class="col-6">
                <a href="{{ route('bank.auto.index') }}" class="btn davivienda-file">
                    <img src="{{ asset('design/index/bank_auto.png') }}">
                    <p>Préstamo de Auto</p>
                </a>
            </div>
        @endif
    </div>
@endsection
