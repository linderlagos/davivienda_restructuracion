@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Administración de usuarios</h1>
                </div>
                <div class="banner-description">
                    <p>En esta sección puede administrar las llaves de autenticación de los usuarios</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content-fluid')
    <div class="row">
        <div class="col-12">
            <table id="account-customers" class="davivienda-datatables">
                <thead>
                    <tr>
                        <th style="width:12%">Peoplesoft</th>
                        <th style="width:12%">Nombre</th>
                        <th style="width:12%">Fecha de creación</th>
                        <th style="width:12%">Fecha de último ingreso</th>
                        <th style="width:12%">Links</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td class="text-center">
                                {{ $user->peopleSoft }}
                            </td>
                            <td class="text-center">
                                {{ $user->name }}
                            </td>
                            <td class="text-center">
                                {{ $user->created_at->format('Y/m/d') }}
                            </td>
                            <td class="text-center">
                                {{ $user->updated_at->format('Y/m/d') }}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('security.user', $user->id) }}" class="btn btn-red">
                                    <i class="fas fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    @include('partials._scripts')
    @include('security.partials._queryScripts')
@stop
