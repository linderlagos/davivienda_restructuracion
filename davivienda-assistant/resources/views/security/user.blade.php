@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }}">
                </div>
                <div class="banner-title">
                    <h1>{{ $user->name }}</h1>
                </div>
                <div class="banner-description">
                    <p>Perfil del usuario con peoplesoft No. {{ $user->peopleSoft }}</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop

@section('content')
    <div class="row justify-content-center">
        <div class="col-8">
            <table class="standard-table">
                <thead>
                    <tr>
                        <th>Campo</th>
                        <th>Descripción</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="standard-table-title">Nombre:</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td class="standard-table-title">Peoplesoft:</td>
                        <td>{{ $user->peopleSoft }}</td>
                    </tr>
                    <tr>
                        <td class="standard-table-title">Fecha de creación:</td>
                        <td>{{ $user->created_at->format('d/m/Y') }}</td>
                    </tr>
                    <tr>
                        <td class="standard-table-title">Último ingreso:</td>
                        <td>{{ $user->updated_at->format('d/m/Y') }}</td>
                    </tr>
                    <tr>
                        <td class="standard-table-title">Llave secreta:</td>
                        <td>{{ $user->google2fa_secret }}</td>
                    </tr>
                    <tr>
                        <td class="standard-table-title">QR:</td>
                        <td class="text-center">
                            <img src="{{ $qrUrl }}">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row justify-content-center mt-4">
        <div class="col-8">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('security.generate.new.secret.key', $user->id) }}" id="form">
                {!! csrf_field() !!}

                <div class="row text-center">
                    <div class="col-12">
                        <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                            Generar nuevo llave secreta
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
    @include('partials._scripts')
@stop
