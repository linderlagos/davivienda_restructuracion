<div class="container-fluid" style="background-color: {{ isset($bgColor) ? $bgColor : '#EBEBEB' }}">
    <div class="container">
        <div class="{{ isset($class) ? $class : 'banner' }}">
            <div class="banner-image">
                <img src="{{ isset($image) ? $image : asset('design/index/bank_card.png') }}">
            </div>
            <div class="banner-title">
                <h1>{{ $title }}</h1>
            </div>
            <div class="banner-description">
                <p>{!! $description !!}</p>
            </div>
        </div>
    </div>
</div>