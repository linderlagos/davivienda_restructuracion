<select name="{{$name}}" class="form-control
        @if(isset($classes))
            {{ $classes }}
        @endif"

        placeholder="{{ $placeholder }}"

        @if(isset($id))
            id="{{ $id }}"
        @else
            id="{{ $name }}"
        @endif

        @if(isset($required))
            required
        @endif

        @if(isset($json))
            @if($json !== 'NULO')
                @include('modules._disabled')
            @endif
        @elseif(isset($resource->$name))
            @include('modules._disabled')
        @endif
    >

    @if (empty($disableDefaultValue))
        <option value="">{{ $placeholder }}</option>
    @endif

    @foreach($options as $key => $value)
        <option value="{{ $key }}"
            @if ($key == old($name))
                selected="selected"
            @elseif(isset($resource))
                @if(isset($json))
                    @if ($json !== 'NULO')
                        @if ($key == $json)
                            selected="selected"
                        @endif
                    @endif
                @elseif (isset($resource->$name))
                    @if ($key == $resource->$name)
                        selected="selected"
                    @endif
                @elseif (isset($default))
                    @if ($key === $default)
                        selected="selected"
                    @endif
                @endif
            @else
                @if (isset($default))
                    @if ($key === $default)
                        selected="selected"
                    @endif
                @endif
            @endif
        >{{ $value }}</option>
    @endforeach
</select>

@if ($errors->has($name))
    <span class="help-block {{ $name }}-error">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
@else
    <span class="help-block {{ $name }}-error">
        <strong></strong>
    </span>
@endif