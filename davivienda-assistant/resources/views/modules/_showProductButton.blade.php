<div class="col-4">
    <a href="{{ route('show.product', $product->id) }}" class="btn davivienda-file">
        <img src="{{ asset('design/index/' . $product->type . '.png') }}">
        <p>{{ product_name($product->type) }}</p>
        <p>{{ $product->identifier }}</p>
    </a>
</div>