@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Ingresa al Asistente Davivienda</h1>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form method="POST" action="{{ route('2fa') }}" aria-label="{{ __('Login') }}">
                @csrf

                <div class="form-group row">
                    <div class="col-12 form-group">
                        <label class="control-label" for="one_time_password">Token</label>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon fas fa-key"></span>
                    </div>

                    <div class="col-10">
                        <input id="one_time_password" type="text" class="form-control{{ $errors->has('one_time_password') ? ' is-invalid' : '' }}" name="one_time_password" placeholder="Token" required>

                        @if ($errors->has('message'))
                            <span class="help-block">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row justify-content-center">
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-davivienda-red btn-loading">
                            Autenticar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
