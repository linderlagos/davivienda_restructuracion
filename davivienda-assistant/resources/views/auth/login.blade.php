@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Ingresa al Asistente Davivienda</h1>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-12 form-group">
                                <label class="control-label" for="peopleSoft">PeopleSoft</label>
                            </div>

                            <div class="col-2 form-group text-center">
                                <span class="davivienda-icon icon-id"></span>
                            </div>

                            <div class="col-10">
                                <input id="peopleSoft" type="text" class="form-control{{ $errors->has('peopleSoft') ? ' is-invalid' : '' }}" name="peopleSoft" placeholder="PeopleSoft" value="{{ old('peopleSoft') }}" required autofocus>

                                {{--@if ($errors->has('peopleSoft'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('peopleSoft') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12 form-group">
                                <label class="control-label" for="password">Contraseña</label>
                            </div>

                            <div class="col-2 form-group text-center">
                                <span class="davivienda-icon icon-edit"></span>
                            </div>

                            <div class="col-10">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row davivienda-checkbox form-group terms align-items-center">
                            <div class="col-2 text-center">
                                <input id="remember" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                            </div>
                            <div class="col-10">
                                <label class="standard-label" for="remember">Recordar mi sesión</label>
                                @if ($errors->has('remember'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('remember') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--<div class="form-group row">--}}
                            {{--<div class="col-12">--}}
                                {{--<div class="form-check">--}}
                                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                    {{--<label class="form-check-label" for="remember">--}}
                                        {{--Recordarme--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group row justify-content-center">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-davivienda-red btn-loading">
                                    Ingresar
                                </button>
                            </div>
                        </div>
                    </form>
        </div>
    </div>
</div>
@endsection
