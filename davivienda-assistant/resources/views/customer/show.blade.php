@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Productos de {{ $customer->name }}</h1>
                </div>
                <div class="banner-description">
                    <p>Seleccione el producto que desea revisar del cliente {{ $customer->name }},  con ID No. {{ $customer->identifier }} y CIF No. {{ $customer->cif }}</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content-fluid')
    <div class="row">
        <div class="col-12 form-group">
            <h2>Productos:</h2>
        </div>
    </div>

    <div class="row justify-content-center" style="margin-bottom: 2rem">
        @foreach ($customer->products()->get() as $product)
            @include('modules._showProductButton')
        @endforeach
    </div>
@endsection
