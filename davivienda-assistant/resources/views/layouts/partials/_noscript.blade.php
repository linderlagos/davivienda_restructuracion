<noscript>
    <style>
        #app, .navbar-container {
            display:none;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center" style="font-size: 3rem; margin-top: 5rem">
                Necesita habilitar su javascript para poder continuar
            </div>
        </div>
    </div>
</noscript>