{{-- Scripts --}}
<script src="{{ mix('js/app.js') }}" type="application/javascript"></script>
<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('js/plugins/autoNumeric.js') }}" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        let alert = $('.alert-user-incomplete-process');

        alert.toggle("slide");

        $('.alert-user-close-btn').click(function () {
            $(".alert-user-incomplete-process").toggle("slide");
        });
    });
</script>

<script>
    $("#form").submit("click",function(e) {
        $(this).find(".btn-loading").html('<i class="fas fa-spinner fa-spin"></i> ENVIANDO...');
        $(this).find(".btn-loading").prop('disabled',true);
    });
</script>

<script>
    $(document).ready(function() {
        @if($errors->has('reject'))
            $(".rejection-content").show();
        @else
            $(".rejection-content").hide();
        @endif

        $( "#rejection-toggle" ).click(function() {
            $( ".rejection-content" ).slideToggle( "slow" );

            let icon = $("#rejection-icon");

            if(icon.hasClass('open-icon')) {
                icon.removeClass('open-icon');
                icon.addClass('close-icon');
            } else {
                icon.removeClass('close-icon');
                icon.addClass('open-icon');
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#rejection-submit').on("click", function(e){
            e.preventDefault();

            swal({
                title: '¿Está seguro?',
                text: "Una vez rechazada la solicitud debe ingresarla de nuevo",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#ED1C27',
                cancelButtonColor: '#959595',
                confirmButtonText: 'Si, rechazarla',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.value) $('#rejection-form').submit();
            });
        });
    });
</script>

{{--<script>--}}
    {{--$(document).ready(function preventLogout() {--}}
        {{--$('#logout').on("click", function(e){--}}
            {{--e.preventDefault();--}}

            {{--swal({--}}
                {{--title: '¿Está seguro?',--}}
                {{--text: "Al salir, no podrá continuar con la solicitud actual. Se recomienda que rechace la solicitud primero antes de salir.",--}}
                {{--type: 'warning',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonColor: '#ED1C27',--}}
                {{--cancelButtonColor: '#959595',--}}
                {{--confirmButtonText: 'Si, estoy seguro',--}}
                {{--cancelButtonText: 'No saldré',--}}
            {{--}).then((result) => {--}}
                {{--if (result.value) $('#logout-form').submit();--}}
            {{--});--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}

{{--<script>--}}
    {{--$(document).ready(function preventReturnHome() {--}}
        {{--$('#home').on("click", function(e){--}}
            {{--e.preventDefault();--}}

            {{--swal({--}}
                {{--title: '¿Está seguro?',--}}
                {{--text: "Al regresar a la página principal, no podrá continuar con la solicitud actual. Se recomienda que rechace la solicitud primero antes de salir.",--}}
                {{--type: 'warning',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonColor: '#ED1C27',--}}
                {{--cancelButtonColor: '#959595',--}}
                {{--confirmButtonText: 'Si, estoy seguro',--}}
                {{--cancelButtonText: 'No saldré',--}}
            {{--}).then((result) => {--}}
                {{--if (result.value) window.location.href = "/";--}}
            {{--});--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}

<script>
    $('.smooth-scroll').click(function(){

        var the_id = $(this).attr("href");

        $('html, body').animate({
            scrollTop:$(the_id).offset().top
        }, 'slow');

        return false;
    });
</script>

@yield('scripts')