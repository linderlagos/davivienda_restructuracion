<div class="navbar-container" id="logo-container">
    @if(!Auth::guest())
        @if(session()->has('card-step') || session()->has('account-step'))
        @else
            <div class="logout-container">
                <div class="btn-group">
                    <button class="btn btn-menu dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menú
                    </button>
                    <div class="dropdown-menu">
                        @role(env('ROL_GESTOR'))
                            <a class="dropdown-item" href="{{ route('bank.card.index') }}">
                                <i class="fas fa-credit-card"></i> Solicitud de tarjeta
                            </a>
                            <a class="dropdown-item" href="{{ route('bank.account.index') }}">
                                <i class="fas fa-hand-holding-usd"></i> Solicitud de cuenta
                            </a>
                        @endrole

                        @if (env('ENABLE_AUTO_FOR_EVERYONE'))
                            @role(env('ROL_GESTOR') . '|' . env('ROL_CONCESIONARIA'))

                                <a class="dropdown-item" href="{{ route('bank.auto.index') }}">
                                    <i class="fas fa-car"></i> Préstamo de auto
                                </a>
                            @endrole
                        @endif

                        @unlessrole(env('ROL_CONCESIONARIA') . '|' . env('ROL_SEGURIDAD'))
                            <a class="dropdown-item" href="{{ route('query.index') }}">
                                <i class="fas fa-database"></i> Consultas
                            </a>
                        @endunlessrole

                        @role(env('ROL_SEGURIDAD'))
                            <a class="dropdown-item" href="{{ route('bank.card.index') }}">
                                <i class="fas fa-user-shield"></i> Usuarios
                            </a>
                        @endrole

                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();" title="Cerrar sesión" style="text-decoration: none;">
                            <i class="fas fa-power-off"></i> Salir
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        @endif
    @endif
    <div class="navbar-overflow">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                @if(session()->has('card-step') || session()->has('account-step'))
                    <span class="navbar-brand">
                        <img src="{{ asset('design/header/logo.png') }}">
                    </span>
                @else
                    <a class="navbar-brand" href="{{ route('home') }}">
                        <img src="{{ asset('design/header/logo.png') }}">
                    </a>
                @endif
            </div>
        </nav>
    </div>
    <div class="navbar-motive">
        <img src="{{ asset('design/header/motive.png') }}">
    </div>
</div>