<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="initial-scale=1.0"/>
        <meta name="format-detection" content="telephone=no"/>
        <title>Asistente Davivienda</title>
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,100,400" rel="stylesheet" type="text/css" />
        <style type="text/css">



            /* Resets: see reset.css for details */
            .ReadMsgBody { width: 100%; background-color: #ffffff;}
            .ExternalClass {width: 100%; background-color: #ffffff;}
            .ExternalClass, .ExternalClass p, .ExternalClass span,
            .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}

            #outlook a{ padding:0;}

            html{width: 100%; }
            body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
            html,body {background-color: #ffffff; margin: 0; padding: 0; }
            table {border-spacing:0;}
            table td {border-collapse:collapse;}
            br, strong br, b br, em br, i br { line-height:100%; }
            h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
            img{height: auto !important;
                line-height: 100%;
                outline: none;
                text-decoration: none;
                display:block !important;
            }

            span a { text-decoration: none !important;}
            a{ text-decoration: none !important; }
            table p{margin:0;}

            .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited,
            .yshortcuts a:hover, .yshortcuts a span {
                text-decoration: none !important; border-bottom: none !important;
            }

            table{ mso-table-lspace:0pt; mso-table-rspace:0pt; }
            img{ -ms-interpolation-mode:bicubic; }
            body{ -webkit-text-size-adjust:100%;}
            body{-ms-text-size-adjust:100%;}




            /*mailChimp class*/
            .default-edit-image{
                height:20px;
            }

            .nav-ul{
                margin-left:-23px !important;
                margin-top:0px !important;
                margin-bottom:0px !important;
            }

            img{height:auto !important;}

            td[class="image-270px"] img{
                width:270px;
                height:auto !important;
                max-width:270px !important;
            }
            td[class="image-170px"] img{
                width:170px;
                height:auto !important;
                max-width:170px !important;
            }
            td[class="image-185px"] img{
                width:185px;
                height:auto !important;
                max-width:185px !important;
            }
            td[class="image-124px"] img{
                width:124px;
                height:auto !important;
                max-width:124px !important;
            }
            .btn{
                background-color:#b90000;
                -moz-border-radius:10px;
                -webkit-border-radius:10px;
                border-radius:10px;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-size:17px;
                padding:10px 25px;
                margin-top: 25px;
                text-decoration:none;
            }
            .btn:hover {
                background-color:#970000;
                text-decoration:none;
            }
            .btn:active {
                position:relative;
                top:1px;
            }
        </style>
    </head>
    <body  style="font-size:12px;">

        <table id="mainStructure" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#EAEAEA;">
            <!--START VIEW ONLINE AND ICON SOCAIL -->
            <tr>
                <td align="center" valign="top" style="background-color: #b90000; ">

                    <!-- start container 600 -->
                    <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #b90000; height: 85px">
                        <tr>
                            <td valign="top">

                                <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="background-color: #b90000; padding: 10px 0">
                                    <!-- start space -->
                                    <tr>
                                        <td valign="top" height="10">
                                        </td>
                                    </tr>
                                    <!-- end space -->
                                    <tr>
                                        <td valign="top">

                                            <!-- start container -->
                                            <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">

                                                <tr>
                                                    <td valign="top">

                                                        <!-- start view online -->
                                                        <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="container2">
                                                            <tr>
                                                                <td>
                                                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="text-align:center;">
                                                                                <img src="http://www.davivienda.com.hn/design/frontend/structure/logo-banco.png" width="350" alt="Logo de Banco Davivienda" style="max-width:300px;" border="0" hspace="0" vspace="0">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <!-- start space -->
                                                            <tr>
                                                                <td valign="top" class="increase-Height">
                                                                </td>
                                                            </tr>
                                                            <!-- end space -->
                                                        </table>
                                                        <!-- end view online -->

                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- end container  -->
                                        </td>
                                    </tr>

                                </table>
                                <!-- end container 600-->
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <!--END VIEW ONLINE AND ICON SOCAIL-->

            <!-- START LAYOUT-1/1 -->

            <tr>
                <td align="center" valign="top" class="fix-box">

                    <!-- start  container width 600px -->
                    <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; padding-top: 30px">


                        <tr>
                            <td valign="top">

                                <!-- start container width 560px -->
                                <table width="540" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">


                                    <!-- start text content -->
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                <tr>
                                                    <td valign="top" width="auto" align="center">
                                                        <!-- start button -->
                                                        <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="auto" align="center" valign="middle" height="28" style=" background-color:#ffffff; background-clip: padding-box; font-size:34px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; text-align:center;  color:#000; font-weight: 300; padding-left:18px; padding-right:18px; ">

                                     <span style="color: #000; font-weight: 400;">
                                         @yield('title')
                                     </span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- end button -->
                                                    </td>
                                                </tr>



                                            </table>
                                        </td>
                                    </tr>
                                    <!-- end text content -->


                                </table>
                                <!-- end  container width 560px -->
                            </td>
                        </tr>
                    </table>
                    <!-- end  container width 600px -->
                </td>
            </tr>

            <!-- END LAYOUT-1/1 -->



            <!-- START HEIGHT SPACE 20PX LAYOUT-1 -->
            <tr>
                <td valign="top" align="center" class="fix-box">
                    <table width="600" height="20" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #ffffff;" class="full-width">
                        <tr>
                            <td valign="top" height="20">
                                <img src="http://www.davivienda.com.hn/design/frontend/mail/space.png" width="20" alt="space" style="display:block; max-height:20px; max-width:20px;"> </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- END HEIGHT SPACE 20PX LAYOUT-1-->



            <!-- START LAYOUT-1/2 -->

            <tr>
                <td align="center" valign="top" class="fix-box">

                    <!-- start  container width 600px -->
                    <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; ">


                        <tr>
                            <td valign="top">

                                <!-- start container width 560px -->
                                <table width="540" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">


                                    <!-- start text content -->
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">


                                                <!-- start text content -->
                                                <tr>
                                                    <td valign="top">
                                                        <table border="0" cellspacing="0" cellpadding="0" align="center">


                                                            <!--start space height -->
                                                            <tr>
                                                                <td height="15"></td>
                                                            </tr>
                                                            <!--end space height -->

                                                            <tr>
                                                                <td style="font-size: 24px; line-height: 30px; font-family:Arial,Tahoma, Helvetica, sans-serif; color:#000; font-weight:400; text-align:center; ">
                                                                    @yield('content')
                                                                </td>
                                                            </tr>

                                                            <!--start space height -->
                                                            <tr>
                                                                <td height="15"></td>
                                                            </tr>
                                                            <!--end space height -->



                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- end text content -->

                                            </table>
                                        </td>
                                    </tr>
                                    <!-- end text content -->

                                    <!--start space height -->
                                    <tr>
                                        <td height="20"></td>
                                    </tr>
                                    <!--end space height -->


                                </table>
                                <!-- end  container width 560px -->
                            </td>
                        </tr>
                    </table>
                    <!-- end  container width 600px -->
                </td>
            </tr>



            <!-- START LAYOUT-1/2 -->

            <tr>
                <td align="center" valign="top" class="fix-box">

                    <!-- start  container width 600px -->
                    <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; ">


                        <tr>
                            <td valign="top">

                                <!-- start container width 560px -->
                                <table width="540" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">


                                    <!-- start text content -->
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">


                                                <!-- start text content -->
                                                <tr>
                                                    <td valign="top">
                                                        <table border="0" cellspacing="0" cellpadding="0" align="center">


                                                            <!--start space height -->
                                                            <tr>
                                                                <td height="15"></td>
                                                            </tr>
                                                            <!--end space height -->

                                                            <tr>
                                                                <td style="font-size: 16px; line-height: 30px; font-family:Arial,Tahoma, Helvetica, sans-serif; color:#000; font-weight:400; text-align:center; ">


                                                                    Si desea comunicarse con nosotros puede hacerlo al 2268-1919 en Tegucigalpa, al 2514-1919 en San Pedro Sula y al 2480-1919 en La Ceiba o acérquese a cualquiera de nuestra oficinas.


                                                                </td>
                                                            </tr>

                                                            <!--start space height -->
                                                            <tr>
                                                                <td height="15"></td>
                                                            </tr>
                                                            <!--end space height -->



                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- end text content -->

                                            </table>
                                        </td>
                                    </tr>
                                    <!-- end text content -->

                                    <!--start space height -->
                                    <tr>
                                        <td height="20"></td>
                                    </tr>
                                    <!--end space height -->


                                </table>
                                <!-- end  container width 560px -->
                            </td>
                        </tr>
                    </table>
                    <!-- end  container width 600px -->
                </td>
            </tr>

            <!-- END LAYOUT-1/2 -->


            <!--  START FOOTER COPY RIGHT -->
            <tr>
                <td align="center" valign="top" style="background-color:#b90000;">
                    <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:#b90000;">
                        <tr>
                            <td valign="top">
                                <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:#b90000;">

                                    <!--start space height -->
                                    <tr>
                                        <td height="10"></td>
                                    </tr>
                                    <!--end space height -->

                                    <tr>
                                        <!-- start COPY RIGHT content -->
                                        <td valign="top" style="font-size: 13px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#ffffff; font-weight:300; text-align:center; ">
                                            <a href="http://www.davivienda.com.hn" target="_blank" style="color: #fff; text-decoration:none;">www.davivienda.com.hn</a>
                                        </td>
                                        <!-- end COPY RIGHT content -->
                                    </tr>

                                    <!--start space height -->
                                    <tr>
                                        <td height="10"></td>
                                    </tr>
                                    <!--end space height -->


                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--  END FOOTER COPY RIGHT -->



        </table>
    </body>
</html>