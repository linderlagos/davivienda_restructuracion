<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('layouts.partials._meta')
        @include('layouts.partials._noscript')
    </head>
    <body>
        @include('sweetalert::alert')

        <div id="app">
            @include('layouts.partials._header')

            @yield('banner')

            <div class="container-fluid" style="position:relative; min-height: 60px">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            @yield('back')
                        </div>
                        <div class="col-6">
                            @yield('reject')
                        </div>
                    </div>
                </div>
            </div>

            @if (isset($pendingUploads))
                <div class="alert-user-incomplete-process" style="display: none">
                    <div class="alert-user-close">
                        <button class="alert-user-close-btn">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                    <div class="alert-user-title">
                        <h2>¡Cuenta incompleta!</h2>
                    </div>
                    <div class="alert-user-content">
{{--                            <p>Tiene {{ $pendingUploads->count() }} cuentas asistidas pendiente de documentación:</p>--}}
                        <p>Por favor cargue los documentos pendientes para terminar con el proceso de creación de la cuenta
                            <a href="{{ route('show.product', $pendingUploads->id) }}" target="_blank" style="color: white; text-decoration: underline">
                                <span style="word-break: keep-all">No. {{ $pendingUploads->identifier }}</span>
                            </a>
                        </p>
                        {{--<ul>--}}
                            {{--@foreach($pendingUploads  as $account)--}}
                                {{--<li>--}}
                                    {{--<a href="{{ route('account.contracts', [$account->identifier, $account->random]) }}" target="_blank">--}}
                                        {{--{{ $account->product_identifier }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    </div>
                </div>
            @endif

            <div class="container-fluid" style="padding: 50px 15px 8rem">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-9">
                            @yield('content')
                        </div>
                        <div class="col-12">
                            @yield('content-fluid')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.partials._footer')

        @include('layouts.partials._scripts')
    </body>
</html>