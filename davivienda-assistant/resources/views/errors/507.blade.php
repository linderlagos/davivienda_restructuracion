@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-error">
                    500
                </div>
                <div class="banner-title">
                    <h1>Error con el servidor</h1>
                </div>
                <div class="banner-description">
                    <p>Le pedimos disculpas, hemos encontrado un error con la conexión al servidor. Por favor intente de nuevo desde la página principal.</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row justify-content-center">
        {{--<div class="col-12">--}}
            {{----}}
        {{--</div>--}}

        <div class="col-12 text-center">
            <a href="{{ route('home') }}" class="btn btn-davivienda-red">Ir a la página principal</a>
        </div>
    </div>
@endsection