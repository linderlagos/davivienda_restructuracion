<script type="text/javascript" src="{{ asset('plugins/dropzone/dropzone.js') }}"></script>

@if($safeFamilyStatus !== 3  && !cifUsers($peoplesoft))
    <script>
        $(document).ready(function () {
            let status = '{{ $safeFamilyStatus }}';

            if (status == 1 || status == 2) {
                $('.hide-insurance-safe-family').hide();
            } else {
                $('.hide-documents').hide();
                $('.hide-safe-family-toggle').hide();
            }

            let form = $('#reject-insurance-safe-family');

            form.submit(function (e) {
                e.preventDefault();

                form.find("button[type='submit']").html('Enviando...');
                form.find("button[type='submit']").prop('disabled',true);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url     : form.attr('action'),
                    type    : form.attr('method'),
                    data    : form.serialize(),
                    dataType: 'json',
                    success : onSuccessStoreIdentity,
                    error: onErrorStoreIdentity
                });
            });

            function onSuccessStoreIdentity(data, status, requ) {
                form.find("button[type='submit']").html('No');
                form.find("button[type='submit']").prop('disabled',false);

                $('.hide-documents').slideDown();
                $('.hide-insurance-safe-family').slideUp();
                $('.hide-safe-family-toggle').slideDown();
            }

            function onErrorStoreIdentity(json) {
                console.log('error');

                form.find("button[type='submit']").html('Error...');
                form.find("button[type='submit']").prop('disabled',false);
            }
        });

        function showSafeFamily() {
            $('.hide-insurance-safe-family').slideDown();
            $('.hide-safe-family-toggle').slideUp();
        }
    </script>
@endif

<script>
    Dropzone.autoDiscover = false;
    // $(document).ready(function(){
    //     $(".user-dropzone").dropzone({
    //         paramName: "file", // The name that will be used to transfer the file
    //         maxFilesize: 10,
    //         maxFiles: 1,
    //         acceptedFiles: 'application/pdf'
    //     });
    // });

    $(document).ready(function() {
        if ($('#identityForm').length)
        {
            let identityDropzone = new Dropzone("#identityForm", {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 10,
                maxFiles: 20,
                autoProcessQueue: false,
                acceptedFiles: 'application/pdf'
            });

            identityDropzone.on('addedfile', function(file, xhr, formData) {
                swal({
                    title: '¿Está seguro que está subiendo la identidad?',
                    text: "Si no es el documento de identidad, la creación de la cuenta no podrá ser completada",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '¡Si, estoy seguro!',
                    cancelButtonText: '¡No!',
                }).then((result) => {
                    if (result.value) {
                        identityDropzone.processQueue();
                    } else {
                        identityDropzone.cancelUpload(file);
                    }
                });
            });
        }

        if ($('#signatureForm').length) {
            let signatureDropzone = new Dropzone("#signatureForm", {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 10,
                maxFiles: 20,
                autoProcessQueue: false,
                acceptedFiles: 'application/pdf'
            });

            signatureDropzone.on('addedfile', function (file, xhr, formData) {
                swal({
                    title: '¿Está seguro que está subiendo el documento de firma?',
                    text: "Si no es el documento de firma, la creación de la cuenta no podrá ser completada",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '¡Si, estoy seguro!',
                    cancelButtonText: '¡No!',
                }).then((result) => {
                    if (result.value) {
                        signatureDropzone.processQueue();
                    } else {
                        signatureDropzone.cancelUpload(file);
                    }
                });
            });
        }


        if ($('#deedForm').length) {
            let deedDropzone = new Dropzone("#deedForm", {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 10,
                maxFiles: 20,
                autoProcessQueue: false,
                acceptedFiles: 'application/pdf'
            });

            deedDropzone.on('addedfile', function (file, xhr, formData) {
                swal({
                    title: '¿Está seguro que está subiendo la escritura de comerciante individual?',
                    text: "Si no es el documento de firma, la creación de la cuenta no podrá ser completada",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '¡Si, estoy seguro!',
                    cancelButtonText: '¡No!',
                }).then((result) => {
                    if (result.value) {
                        deedDropzone.processQueue();
                    } else {
                        deedDropzone.cancelUpload(file);
                    }
                });
            });
        }

        let myDropzone = new Dropzone("#supportForm", {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10,
            maxFiles: 20,
            // autoProcessQueue: false,
            acceptedFiles: 'application/pdf'
        });

        myDropzone.on('sending', function(file, xhr, formData) {


            // if (formData.description) {
            //     swal({
            //         input: 'textarea',
            //         inputPlaceholder: 'Describa el documento que está subiendo...',
            //         showCancelButton: true
            //     }).then((result) => {
            //         if (result.value) {
            //             formData.append('description', result.value);
            //             myDropzone.processQueue();
            //         } else {
            //             myDropzone.cancelUpload(file);
            //         }
            //     });
            // }

            let description = prompt("Ingrese una descripción", "Descripción del archivo");

            if (description != null) {
                formData.append('description', description);
            } else {
                myDropzone.cancelUpload(file);
            }

            // const {value: text} = swal({
            //     input: 'textarea',
            //     inputPlaceholder: 'Type your message here...',
            //     showCancelButton: true
            // }).then((result) => {
            //     if (result.value) {
            //         formData.append('description', result.value);
            //
            //         console.log(formData);
            //         myDropzone.enqueue(file);
            //     }
            // });

            // if (text) {
            //     console.log(text);
            //     formData.append('description', text);
            //     myDropzone.enqueue(file);
            // }
        });

    });
</script>

<script type="text/javascript" src="{{ asset('plugins/datatables/datatables.js') }}"></script>
<script>
    $(document).ready( function () {
        $('#support-documents').DataTable({
            // "info":     false,
            responsive: false,
            "lengthChange": false,
            "pageLength": 5,
            "pagingType": "full_numbers",
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    } );
</script>