<script>
    $(document).ready(function() {

        $('.show-if-foreign-currency').hide();

        $('.show-if-transfers-or-remittances').hide();

        $('.show-if-foreign-transfers').hide();

        $('.show-if-remittances').hide();

        if ($('#foreign_currency option:selected').val() === "S") {
            $('.show-if-foreign-currency').fadeIn();
        }

        $('#foreign_currency').change(function () {
            if ($('#foreign_currency option:selected').val() === "S") {
                $('.show-if-foreign-currency').fadeIn();
            } else {
                $('.show-if-foreign-currency').fadeOut();
                $('.show-if-transfers-or-remittances').fadeOut();
                $('.show-if-foreign-transfers').fadeOut();
                $('.show-if-remittances').fadeOut();
                $('#wire_transfers').val('');
                $('#currency_purchase').val('');
                $('#checks_in_dollars').val('');
                $('#remittances').val('');
            }
        });

        if ($('#wire_transfers option:selected').val() === 'S') {
            $('.show-if-transfers-or-remittances').fadeIn();
            $('.show-if-foreign-transfers').fadeIn();
        }

        $('#wire_transfers').change(function () {
            if ($('#wire_transfers option:selected').val() == 'S') {
                $('.show-if-transfers-or-remittances').fadeIn();
                $('.show-if-foreign-transfers').fadeIn();
            } else if ($('#wire_transfers option:selected').val() !== 'S' && $('#remittances option:selected').val() !== 'S') {
                $('.show-if-transfers-or-remittances').fadeOut();
                $('.show-if-foreign-transfers').fadeOut();
            } else {
                $('.show-if-foreign-transfers').fadeOut();
            }
        });

        if ($('#remittances option:selected').val() === 'S') {
            $('.show-if-transfers-or-remittances').fadeIn();
            $('.show-if-remittances').fadeIn();
        }


        $('#remittances').change(function () {
            if ($('#remittances option:selected').val() === 'S') {
                $('.show-if-transfers-or-remittances').fadeIn();
                $('.show-if-remittances').fadeIn();
            }  else if ($('#wire_transfers option:selected').val() !== 'S' && $('#remittances option:selected').val() !== 'S') {
                $('.show-if-transfers-or-remittances').fadeOut();
                $('.show-if-remittances').fadeOut();
            } else {
                $('.show-if-remittances').fadeOut();
            }
        });
    });
</script>

<script>
    let dependantClass = $('.show-if-dependant');
    let independentClass = $('.show-if-independent');
    let income = $('#income');

    dependantClass.hide();
    independentClass.hide();
    income.attr("placeholder", "Ingrese su salario mensual");

    let jobType = $('#job_type option:selected').val();

    if (jobType == 10 || jobType == 11 || jobType == 44) {
        dependantClass.show();
        independentClass.hide();
        income.attr("placeholder", "Salario mensual del dependiente");
    } else {
        dependantClass.hide();
        independentClass.show();
        income.attr("placeholder", "Ingrese su salario mensual");
    }

    $('#job_type').change(function () {

        let jobType = $('#job_type option:selected').val();

        if (jobType == 10 || jobType == 11 || jobType == 44) {
            dependantClass.fadeIn();
            independentClass.fadeOut();
            income.attr("placeholder", "Salario mensual del dependiente");
        } else {
            dependantClass.fadeOut();
            independentClass.fadeIn();
            income.attr("placeholder", "Ingrese su salario mensual");
        }
    });
</script>

@if(isset($flow))
    @if ($flow->step === '4')
        <script>
            $(document).ready(function() {
                for ($i = 2; $i < 11; $i++)
                {
                    let value = $('#beneficiary_' + $i + '_name').val();

                    if (value.length === 0) {
                        if ($('#beneficiary_' + $i).find('.has-error').length !== 0)
                        {
                            $('#beneficiary_' + $i).show();
                        } else {
                            $('#beneficiary_' + $i).hide();
                        }
                    }
                }
            });

            function addBeneficiary(i)
            {
                let previous = i - 1;

                $('#beneficiary_' + i).fadeIn();
                $('#add_beneficiary_' + previous + '_btn').html(
                    '<a onclick="removeBeneficiary(' + i + ')" href="javascript:void(0);" class="btn davivienda-remove-field">' +
                    '<i class="fas fa-minus"></i>' +
                    '</a>');
            }

            function removeBeneficiary(i)
            {
                let previous = i - 1;
                let next = i;

                $('#beneficiary_' + i).fadeOut();
                $('#beneficiary_' + i + '_name').val('');
                $('#beneficiary_' + i + '_relationship').val('');
                $('#beneficiary_' + i + '_id_type').val('');
                $('#beneficiary_' + i + '_identity').val('');
                $('#add_beneficiary_' + previous + '_btn').html(
                    '<a onclick="addBeneficiary(' + i + ')" href="javascript:void(0);" class="btn davivienda-add-field">' +
                    '<i class="fas fa-plus"></i>' +
                    '</a>');
            }
        </script>
    @endif
@endif