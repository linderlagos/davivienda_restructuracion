<script type="text/javascript" src="{{ asset('plugins/datatables/datatables.js') }}"></script>
<script>
    $(document).ready( function () {
        let table = $('.davivienda-datatables').DataTable({
            // "info":     false,
            // responsive: true,
            "lengthChange": false,
            "pageLength": 50,
            "searching": false,
            "order": [[ 4, "desc" ]],
            "dom": 'Bfrtip',
            "buttons": [],
            "columnDefs": [],
            "pagingType": "full_numbers",
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    });
</script>
<script>
    $(document).ready( function () {
        $('#submit').click(function(){
            let form = $('#form-query');

            form.attr('action', "{{ route('query.index') }}");

            form.submit("click",function(e) {
                $(this).find("#submit").html('<i class="fas fa-spinner fa-spin"></i>');
                $(this).find("#submit").prop('disabled',true);
            });
        });

        $('#export').click(function() {
            let form = $('#form-query');

            form.attr('action', "{{ route('query.export') }}");
        });
    });
</script>