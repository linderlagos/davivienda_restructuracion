@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/bank_account.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Formulario de Solicitud de Cuenta de Ahorro</h1>
                </div>
                <div class="banner-description">
                    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
                        <p>Continúe completando la información del cliente con ID No. {{ $flow->customer->identifier }}</p>
                    @elseif(checkStep($flow, 'contracts'))
                        <p>Imprima los documentos del cliente con ID No. {{ $flow->customer->identifier }} y cuenta No. {{ $flow->identifier }}</p>
                    @else
                        <p>Complete la información del cliente para terminar el proceso de solicitud de cuenta de ahorro asistida</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    @if(checkStep($flow, ['2', '3', '4', '5']))
        @include('modules._return', [
            'id' => $flow->id
        ])
    @elseif(!checkStep($flow, ['1', '2', '3', '4', '5']))
        <a href="{{ route('home') }}" class="btn-back">
            Inicio
        </a>
    @endif
@stop

@section('reject')
    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
        @include('modules._reject', [
            'route' => route('exit.flow', ['bank_account']),
            'options' => [
                'TA01' => 'Asesor'
            ]
        ])
    @endif
@stop

@section('content')

    @if(checkStep($flow, '1'))
        @include('bank.account.form._first')
    @elseif(checkStep($flow, '2'))
        @include('bank.account.form._second')
    @elseif(checkStep($flow, '3'))
        @include('bank.account.form._third')
    @elseif(checkStep($flow, '4'))
        @include('bank.account.form._fourth')
    @elseif(checkStep($flow, '5'))
        @include('bank.account.form._fifth')
    @else
        @include('bank.account.form._search')
    @endif

@endsection

@section('scripts')
    @include('partials._scripts')
    @include('bank.account.partials._scripts')
@stop
