<div id="identity" class="upload-dropzone">
    <form class="dropzone user-dropzone" action="{{ route('account.file.upload', [$customer->identifier, $customer->random, 'identity']) }}" id="identityForm">
        {!! csrf_field() !!}
        <div class="dz-message" data-dz-message>
            <div class="row align-items-center">
                <div class="col-12">
                    <h3><i class="fas fa-id-card"></i> Identidad del cliente</h3>
                    <p>Subir identidad del cliente escaneada al revés y derecho</p>
                    <span>
                        <i class="fas fa-cloud-upload-alt"></i>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>