<div id="signature" class="upload-dropzone">
    <form class="dropzone user-dropzone" action="{{ route('account.file.upload', [$customer->identifier, $customer->random, 'signature']) }}" id="signatureForm">
        {!! csrf_field() !!}
        <div class="dz-message" data-dz-message>
            <div class="row align-items-center">
                <div class="col-12">
                    <h3><i class="fas fa-signature"></i> Documento firmado por cliente</h3>
                    <p>Subir documento escaneado con la firma del cliente</p>
                    <span>
                        <i class="fas fa-cloud-upload-alt"></i>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>