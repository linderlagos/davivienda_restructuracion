<div id="deed" class="upload-dropzone">
    <form class="dropzone user-dropzone" action="{{ route('account.file.upload', [$customer->identifier, $customer->random, 'deed']) }}" id="deedForm">
        {!! csrf_field() !!}
        <div class="dz-message" data-dz-message>
            <div class="row align-items-center">
                <div class="col-12">
                    <h3><i class="fas fa-file-contract"></i> Escritura de constitución</h3>
                    <p>Subir escritura de constitución de comerciante individual</p>
                    <span>
                        <i class="fas fa-cloud-upload-alt"></i>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>