@include('modules._progress', [
    'steps' => 5,
    'step' => 5,
    'title' => 'Información operaciones internacionales'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.account.sixth') }}" id="form">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-12 form-group">
                    <h2>¿Realizará operaciones en moneda extranjera?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-card"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('foreign_currency') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'foreign_currency',
                            'placeholder' => 'Seleccione la respuesta...',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'foreign_currency', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('typical_monthly_income') ? '' : ' show-if-foreign-currency' }}">
                <div class="col-12 form-group">
                    <h2>Monto mensual típico estimado</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('typical_monthly_income') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'typical_monthly_income',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'typical_monthly_income']),
                            'placeholder' => 'Digite el monto',
                            'label' => 'Monto mensual típico estimado',
                            'classes' => 'format-amount',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('typical_monthly_currency') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'typical_monthly_currency',
                            'placeholder' => 'Moneda...',
                            'options' => $lists['transfersCurrency'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'typical_monthly_currency', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('probable_monthly_income') ? '' : ' show-if-foreign-currency' }}">
                <div class="col-12 form-group">
                    <h2>Monto mensual esperado eventual</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('probable_monthly_income') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'probable_monthly_income',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'probable_monthly_income']),
                            'placeholder' => 'Digite el monto',
                            'label' => 'Monto mensual esperado eventual',
                            'classes' => 'format-amount',
                            'autocomplete' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('probable_monthly_currency') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'probable_monthly_currency',
                            'placeholder' => 'Moneda...',
                            'options' => $lists['transfersCurrency'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'probable_monthly_currency', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('wire_transfers') ? '' : ' show-if-foreign-currency' }}">
                <div class="col-12 form-group">
                    <h2>¿Realizará Transferencias Internacionales?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-money-check-alt"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('wire_transfers') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'wire_transfers',
                            'placeholder' => 'Seleccione la respuesta...',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'wire_transfers', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('received_transfer_country') || $errors->has('received_transfer_sender') || $errors->has('received_transfer_reason') ? '' : ' show-if-foreign-transfers' }}">
                <div class="col-12 form-group">
                    <h2>Transferencias recibidas</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('received_transfer_country') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'received_transfer_country',
                            'placeholder' => 'País de procedencia...',
                            'options' => $lists['nations'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'received_transfer_country', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('received_transfer_sender') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'received_transfer_sender',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'received_transfer_sender']),
                            'label' => 'Nombre del remitente',
                            'placeholder' => 'Nombre del remitente',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-2 form-group text-center">
                    {{--<span class="davivienda-icon icon-id"></span>--}}
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('received_transfer_reason') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'received_transfer_reason',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'received_transfer_reason']),
                            'placeholder' => 'Motivo de transferencia',
                            'label' => 'Motivo de transferencia',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('sent_transfer_country') || $errors->has('sent_transfer_sender') || $errors->has('sent_transfer_reason') ? '' : ' show-if-foreign-transfers' }}">
                <div class="col-12 form-group">
                    <h2>Transferencias enviadas</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('sent_transfer_country') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'sent_transfer_country',
                            'placeholder' => 'País de envío...',
                            'options' => $lists['nations'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'sent_transfer_country', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('sent_transfer_sender') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'sent_transfer_sender',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'sent_transfer_sender']),
                            'placeholder' => 'Nombre del beneficiario',
                            'label' => 'Nombre del beneficiario',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-2 form-group text-center">
                    {{--<span class="davivienda-icon icon-id"></span>--}}
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('sent_transfer_reason') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'sent_transfer_reason',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'sent_transfer_reason']),
                            'placeholder' => 'Motivo de transferencia',
                            'label' => 'Motivo de transferencia',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('remittances') ? '' : ' show-if-foreign-currency' }}">
                <div class="col-12 form-group">
                    <h2>¿Recibirá Remesas Familiares?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-card"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('remittances') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'remittances',
                            'placeholder' => 'Seleccione la respuesta...',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'remittances', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('remittance_country') || $errors->has('remittance_sender') || $errors->has('remittance_sender_relationship') || $errors->has('remittance_reason') ? '' : ' show-if-remittances' }}">
                <div class="col-12 form-group">
                    <h2>Remesas recibidas</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('remittance_country') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'remittance_country',
                            'placeholder' => 'País de procedencia...',
                            'options' => $lists['nations'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'remittance_country', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-2 form-group text-center">
                    {{--<span class="davivienda-icon icon-id"></span>--}}
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('remittance_sender') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'remittance_sender',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'remittance_sender']),
                            'placeholder' => 'Nombre del remitente',
                            'label' => 'Nombre del remitente',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('remittance_sender_relationship') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'remittance_sender_relationship',
                            'placeholder' => 'Seleccione el parentesco...',
                            'options' => $lists['referenceOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'remittance_sender_relationship', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-2 form-group text-center">
                    {{--<span class="davivienda-icon icon-id"></span>--}}
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('remittance_reason') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'remittance_reason',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'remittance_reason']),
                            'placeholder' => 'Motivo de la remesa',
                            'label' => 'Motivo de la remesa',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('currency_purchase') ? '' : ' show-if-foreign-currency' }}">
                <div class="col-12 form-group">
                    <h2>¿Realizará Compra/Venta de divisas?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-card"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('currency_purchase') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'currency_purchase',
                            'placeholder' => 'Seleccione la respuesta...',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'currency_purchase', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row{{ $errors->has('checks_in_dollars') ? '' : ' show-if-foreign-currency' }}">
                <div class="col-12 form-group">
                    <h2>¿Realizará Emisión de Giros Dólar (Cheques en dólares)?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-card"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('checks_in_dollars') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'checks_in_dollars',
                            'placeholder' => 'Seleccione la respuesta...',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['international_operations_information', 'checks_in_dollars', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        FINALIZAR
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>