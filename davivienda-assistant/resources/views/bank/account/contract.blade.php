@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropzone/dropzone.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/bank_account.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Documentos de Cuenta de Ahorro Asistida</h1>
                </div>
                <div class="banner-description">
                    <p>Imprima los documentos del cliente {{ get_json($customer->fields, 'name') }} con ID No. {{ $customer->identifier }} y cuenta No. {{ $customer->product_identifier }}</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop

@section('reject')
    @if ($safeFamilyStatus !== 3 && !cifUsers($peoplesoft))
        <div class="text-right hide-safe-family-toggle">
            <a href="javascript:void(0)" onclick="showSafeFamily()" class="btn-back">
                Familia Segura
            </a>
        </div>
    @endif
@stop

@section('content')

    @include('bank.account.modules._contracts')

@endsection

@section('scripts')
    @include('account.partials._contractScripts')
    {{--@include('account.partials._scripts')--}}
@stop
