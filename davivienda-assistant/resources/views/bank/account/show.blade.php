@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropzone/dropzone.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/bank_account.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Documentos de Cuenta de Ahorro Asistida</h1>
                </div>
                <div class="banner-description">
                    <p>Imprima los documentos del cliente {{ get_json($product->customer_information, ['name', 'fullname']) }} con ID No. {{ $product->customer->identifier }} y cuenta No. {{ $product->identifier }}</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop

@section('content')

    <div class="row">
        <div class="col-12 form-group">
            <h2><i class="fas fa-print"></i> Seleccione el documento a generar:</h2>
        </div>
    </div>

    <div class="row" style="margin-bottom: 2rem">
        <div class="col-6 col-lg-3">
            <a href="{{ route('bank.account.print.signature', $product->id) }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-file-signature"></i>
                <p>Firma</p>
            </a>
        </div>

        <div class="col-6 col-lg-3">
            <a href="{{ route('bank.account.print.contract', $product->id) }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-file-alt"></i>
                <p>Contrato de cuenta</p>
            </a>
        </div>

        <div class="col-6 col-lg-3">
            <a href="{{ route('bank.account.print.cover', $product->id) }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-address-book"></i>
                <p>Solicitud de cuenta</p>
            </a>
        </div>

        <div class="col-6 col-lg-3">
            <a href="{{ $printDebitCardURL }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-credit-card"></i>
                <p>Embozar tarjeta</p>
            </a>
        </div>

        @if ($safeFamilyStatus === 3 && $customer->version !== '1')
            {{--        @if ($safeFamilyStatus === 3)--}}
            <div class="col-6 col-lg-3">
                <a href="{{ route('insurance.safe.family.print.policy', [$safeFamilyPrintable->customer->identifier, $safeFamilyPrintable->random]) }}" class="btn davivienda-file" target="_blank">
                    <i class="fas fa-user-lock"></i>
                    <p>Póliza</p>
                </a>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2><i class="fas fa-cloud-upload-alt"></i> Subir documentos:</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-6 col-lg-4">
            @if(isset($identity[0]['url']))
                {{--@if($canUpdateDocument)--}}
                {{--@include('account.modules._identityDropzone')--}}
                {{--@else--}}
                <a href="{{ asset($identity[0]['url']) }}" class="btn davivienda-upload" target="_blank">
                    <i class="fas fa-id-card"></i>
                    <p>Identidad</p>
                </a>
                {{--@endif--}}
            @else
                @include('account.modules._identityDropzone')
            @endif
        </div>
        <div class="col-6 col-lg-4">
            @if(isset($signature[0]['url']))
                {{--@if($canUpdateDocument)--}}
                {{--@include('account.modules._signatureDropzone')--}}
                {{--@else--}}
                <a href="{{ asset($signature[0]['url']) }}" class="btn davivienda-upload" target="_blank">
                    <i class="fas fa-signature"></i>
                    <p>Firma</p>
                </a>
                {{--@endif--}}
            @else
                @include('account.modules._signatureDropzone')
            @endif
        </div>
        <div class="col-6 col-lg-4">
            @if(isset($deed[0]['url']))
                {{--@if($canUpdateDocument)--}}
                {{--@include('account.modules._deedDropzone')--}}
                {{--@else--}}
                <a href="{{ asset($deed[0]['url']) }}" class="btn davivienda-upload" target="_blank">
                    <i class="fas fa-file-contract"></i>
                    <p>Escritura de comerciante</p>
                </a>
                {{--@endif--}}
            @else
                @include('account.modules._deedDropzone')
            @endif
        </div>
    </div>

    @if(cifUsers($peoplesoft))
        <div class="row" style="margin-top: 2rem">
            <div class="col-12 form-group">
                <h2><i class="fas fa-cloud-upload-alt"></i> Documentos soporte:</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div id="support" class="upload-dropzone">
                    <form class="dropzone" action="{{ route('account.file.upload', [$customer->identifier, $customer->random, 'support']) }}" id="supportForm">
                        {!! csrf_field() !!}
                        <div class="dz-message" data-dz-message>
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <h3><i class="fas fa-hands-helping"></i> Documentos soporte</h3>
                                    <p>Subir todos los documentos soporte que se consideren necesarios para la aplicación</p>
                                    <span>
                                    <i class="fas fa-cloud-upload-alt"></i>
                                </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <table id="support-documents" class="davivienda-datatables">
                    <thead>
                    <tr>
                        <th>Descripción</th>
                        <th>Peoplesoft</th>
                        <th>Link</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(is_array($support))
                        @foreach($support as $key => $value)
                            <tr>
                                <td class="text-center">
                                    {{ isset($value['description']) ? $value['description'] : 'No hay descripción' }}
                                </td>
                                <td class="text-center">
                                    {{ $value['peoplesoft'] }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ asset($value['url']) }}" class="btn btn-query" target="_blank">Ver documento</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

@section('scripts')
    @include('bank.account.partials._contractScripts')
    {{--@include('account.partials._scripts')--}}
@stop