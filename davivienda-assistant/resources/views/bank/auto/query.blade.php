@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/bank_auto.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Solicitudes de auto en trámite</h1>
                </div>
                <div class="banner-description">
                    <p>Seleccione la solicitud con la que el cliente desea para la compra de su nuevo vehículo</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop

@section('content-fluid')

    @include('bank.auto.form._query')

@endsection

@section('scripts')
{{--    @include('partials._scripts')--}}
    <script type="text/javascript" src="{{ asset('plugins/datatables/datatables.js') }}"></script>
    <script>
        $(document).ready( function () {
            let table = $('#auto-flows-in-transit').DataTable({
                // "info":     false,
                // responsive: true,
                "lengthChange": false,
                "pageLength": 25,
                "order": [[ 4, "desc" ]],
                "dom": 'Bfrtip',
                "buttons": [],
                "pagingType": "full_numbers",
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "<<",
                        "sLast":     ">>",
                        "sNext":     ">",
                        "sPrevious": "<"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>
    {{--@include('bank.auto.partials._scripts')--}}
@stop
