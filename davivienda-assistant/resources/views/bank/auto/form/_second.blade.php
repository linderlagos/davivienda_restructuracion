@include('modules._progress', [
    'steps' => 4,
    'step' => 2,
    'title' => 'Datos de la cotización'
])

<div class="row">
    <div class="col-12 identity-form" id="calculator-form-container">
        @include('bank.auto.form._calculator')
    </div>
</div>


<div class="row" >
    <div class="col-12 text-center">
        <div id="calculator-result-container" style="display: none"></div>
    </div>
</div>



