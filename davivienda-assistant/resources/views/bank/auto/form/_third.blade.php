@include('modules._progress', [
    'steps' => 4,
    'step' => 3,
    'title' => 'Información general'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.fourth') }}" id="form">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon fas fa-building"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('concessionaire') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'concessionaire',
                                    'placeholder' => 'Concesionaria...',
                                    'options' => $lists['concessionaireOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->user_information, ['concessionaire', 'code']),
                                    'enable' => false
                                ])
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group{{ $errors->has('seller') ? ' has-error' : '' }}" >
                                @include('modules._singleSelect', [
                                    'name' => 'seller',
                                    'placeholder' => 'Vendedor...',
                                    'options' => $lists['sellerOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->user_information, ['seller', 'code']),
                                    'enable' => false
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de correo electrónico</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-messages"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'email',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['email', 'account']),
                            'placeholder' => 'Ingrese su correo electrónico',
                            'label' => 'Correo eletrónico',
                            'length' => '40',
                            'required' => true
                        ])
                    </div>
                </div>
                <div class="col-3 text-right">
                    <label for="has_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                    @if ($errors->has('has_email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('has_email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-1 text-center davivienda-checkbox">
                    <input id="has_email" name="has_email" type="checkbox" value="N">
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Colonia de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <select class="form-control davivienda-select" name="city" id="city" value="{{ old('city') }}" placeholder="Seleccione la colonia..." required>
                            @include('modules._singleSelectOptGroup', [
                                'name' => 'city',
                                'options' => $lists['cities'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['address', 'code']),
                            ])
                        </select>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row hide-other-city">
                <div class="col-2 form-group"></div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('other_city') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'other_city',
                            'resource' => $flow,
                            'placeholder' => 'Nombre de la otra colonia',
                            'json' => get_json($flow->customer_information, ['address', 'other_city']),
                            'label' => 'Nombre de la otra colonia',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'address',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['address', 'full']),
                            'placeholder' => 'Avenida, Calle, Bloque, Casa',
                            'label' => 'Dirección de domicilio (Sin colonia)',
                            'length' => '120',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 form-group">
                    <h2>Lugar de trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-briefcase"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'employer_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'name']),
                            'placeholder' => 'Nombre de la empresa',
                            'label' => 'Nombre de la empresa',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

{{--            <div class="row show-if-independent">--}}
{{--                <div class="col-12 form-group">--}}
{{--                    <h2>Tipo de empleo</h2>--}}
{{--                </div>--}}

{{--                <div class="col-2 form-group text-center">--}}
{{--                    <i class="davivienda-icon fas fa-briefcase"></i>--}}
{{--                </div>--}}

{{--                <div class="col-10">--}}
{{--                    <div class="form-group{{ $errors->has('job') ? ' has-error' : '' }}">--}}
{{--                        @include('modules._singleSelect', [--}}
{{--                            'name' => 'job',--}}
{{--                            'placeholder' => 'Seleccione el tipo de empleo...',--}}
{{--                            'options' => $lists['jobOptions'],--}}
{{--                            'resource' => $flow,--}}
{{--                            'json' => get_json($flow->customer_information, ['employer', 'job', 'status', 'code']),--}}
{{--                            'enable' => true--}}
{{--                        ])--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

            @if (get_json($flow->customer_information, ['employer', 'job', 'status', 'code']) == '1')
                <div class="row hide-job-contract">
                    <div class="col-12 form-group">
                        <h2>Situación Laboral</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-briefcase"></span>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('job_contract') ? ' has-error' : '' }}">
                            @include('modules._singleSelect', [
                                'name' => 'job_contract',
                                'placeholder' => 'Seleccione su situación laboral...',
                                'options' => $lists['jobContractOptions'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'job', 'contract', 'code']),
                            ])
                        </div>
                    </div>
                </div>
            @endif

            <div class="row show-if-independent">
                <div class="col-12 form-group">
                    <h2>Teléfono del trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-phone"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'employer_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'phone']),
                            'placeholder' => 'Ingrese el teléfono del trabajo',
                            'label' => 'Teléfono del trabajo',
                            'length' => '9',
                            'classes' => 'validate-num-dash',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de correo electrónico</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-messages"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('job_email') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'job_email',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['job_email', 'account']),
                            'placeholder' => 'Ingrese su correo laboral',
                            'label' => 'Correo laboral',
                            'length' => '40',
                            'required' => true
                        ])
                    </div>
                </div>
                <div class="col-3 text-right">
                    <label for="has_job_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                    @if ($errors->has('has_job_email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('has_job_email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-1 text-center davivienda-checkbox">
                    <input id="has_job_email" name="has_job_email" type="checkbox" value="N">
                </div>
            </div>

            <div class="row show-if-independent">
                <div class="col-12 form-group">
                    <h2>Tipo de empresa</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-building"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_type') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'employer_type',
                            'placeholder' => 'Seleccione el tipo de empresa...',
                            'options' => $lists['employeeTypes'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'type', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Colonia de trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-gps"></span>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('employer_city') ? ' has-error' : '' }}">
                            <select class="form-control davivienda-select" name="employer_city" id="employer_city" value="{{ old('employer_city') }}" placeholder="Seleccione la colonia...">
                                @include('modules._singleSelectOptGroup', [
                                        'name' => 'employer_city',
                                        'options' => $lists['cities'],
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['employer', 'address', 'code']),
                                    ])
                            </select>
                            @if ($errors->has('employer_city'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('employer_city') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row hide-other-employer-city">
                    <div class="col-2 form-group"></div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('other_employer_city') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'other_employer_city',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'address', 'other_city']),
                                'placeholder' => 'Nombre de la otra colonia',
                                'label' => 'Nombre de la otra colonia',
                                'length' => '40',
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_address') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'employer_address',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'address', 'full']),
                            'placeholder' => 'Avenida, Calle, Bloque, Edificio, Local',
                            'label' => 'Dirección de trabajo',
                            'length' => '120',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Antiguedad laboral</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-calendar"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_day') ? ' has-error' : '' }}">
                        <div class="row birth-container">
                            <div class="col-4 birth-day">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_day',
                                    'placeholder' => 'Día',
                                    'options' => $lists['dayOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'd'),
                                ])
                            </div>
                            <div class="col-4 birth-month">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_month',
                                    'placeholder' => 'Mes',
                                    'options' => $lists['monthOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'm'),
                                ])
                            </div>
                            <div class="col-4 birth-year">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_year',
                                    'options' => $lists['employerYearOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'Y'),
                                    'default' => $lists['selectedEmployerYear'],
                                    'placeholder' => 'Año'
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 form-group">
                    <h2>Estado civil</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>


                <div class="col-10">
                    <div class="form-group{{ $errors->has('marital_status') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'marital_status',
                            'placeholder' => 'Seleccione su estado civil...',
                            'options' => $lists['maritalStatusOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['marital_status', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row hide-marital-status">
                <div class="col-12 form-group">
                    <h2>Cónyuge</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('spouse_id_type') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'spouse_id_type',
                                    'placeholder' => 'Seleccione el tipo de identificación...',
                                    'options' => $lists['idOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['spouse_information', 'id_type', 'code']),
                                    'enable' => true,
                                ])
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group{{ $errors->has('spouse_identity') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'spouse_identity',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['spouse_information', 'identity']),
                                    'placeholder' => 'No. de identificación',
                                    'label' => 'No. de identificación',
                                    'length' => '13',
                                    'classes' => 'validate-num-dash',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('spouse') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'spouse',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, 'spouse'),
                                    'placeholder' => 'Nombre completo del cónyuge',
                                    'label' => 'Nombre completo del cónyuge',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Nacionalidad</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-globe-americas"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'nationality',
                            'placeholder' => 'Seleccione su nacionalidad...',
                            'options' => $lists['nations'],
                            'classes' => 'davivienda-select',
                            'required' => true,
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['nationality', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>¿Tiene ciudadanía, residencia o pasaporte estadounidense?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-flag-usa"></i>
                </div>

                <div class="col-10 form-group{{ $errors->has('fatca') ? ' has-error' : '' }}">
                    @include('modules._singleSelect', [
                        'name' => 'fatca',
                        'placeholder' => 'Seleccione la respuesta...',
                        'options' => $lists['confirmation'],
                        'resource' => $flow,
                        'json' => get_json($flow->customer_information, ['fatca', 'code']),
                        'required' => true,
                        'enable' => true
                    ])
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia familiar</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_1_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'name']),
                            'placeholder' => 'Nombre de la referencia familiar',
                            'label' => 'Nombre de la referencia familiar',
                            'length' => '40',
                            'enable' => true,
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_relationship') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'reference_1_relationship',
                            'placeholder' => 'Seleccione el parentesco...',
                            'options' => $lists['referenceOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'relationship', 'code']),
                            'enable' => true,
                            'required' => true,
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_work_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_work_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'work_phone']),
                            'placeholder' => 'Teléfono de trabajo',
                            'label' => 'Teléfono de trabajo',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia personal</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_2_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'name']),
                            'placeholder' => 'Nombre de la referencia personal',
                            'label' => 'Nombre de la referencia personal',
                            'length' => '40',
                            'enable' => true,
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_work_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_work_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'work_phone']),
                            'placeholder' => 'Teléfono de trabajo',
                            'label' => 'Teléfono de trabajo',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row hide-job-contract">
                <div class="col-12 form-group">
                    <h2>Ejecutivo de auto</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('auto_executive') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'auto_executive',
                            'placeholder' => 'Seleccione el ejecutivo de auto...',
                            'options' => $lists['agentOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->user_information, ['auto_executive', 'code']),
                        ])
                    </div>
                </div>
            </div>

            <hr style="border: none;" class="mb-3">

            <div class="hide-if-no-email">
                @include('modules._checkbox', [
                         'name' => 'receive_emails',
                         'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico personal'
                     ]
                )
            </div>

            <div class="hide-if-no-job-email">
                @include('modules._checkbox', [
                        'name' => 'receive_emails_job',
                        'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico laboral'
                    ]
                )
            </div>

            {{--<div class="row justify-content-center" style="margin-bottom: 1.5rem">--}}
                {{--<div class="col-12 col-sm-6 text-center">--}}

                    {{--<form class="form-horizontal" role="form" method="" action="" id="">--}}
                        {{--<button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">--}}
                            {{--EVALUAR--}}
                        {{--</button>--}}
                    {{--</form>--}}

                {{--</div>--}}


                {{--<div class="col-12 col-sm-6 text-center">--}}

                    {{--<form class="form-horizontal" role="form" method="" action="" id="">--}}
                        {{--<button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">--}}
                            {{--EVALUAR--}}
                        {{--</button>--}}
                    {{--</form>--}}

                {{--</div>--}}
            {{--</div>--}}



            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        EVALUAR
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>