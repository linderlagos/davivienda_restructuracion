@extends('layouts.app')



@section('banner')
    <div class="container-fluid" style="background-color: #F7981B">
        <div class="container">
        </div>
    </div>
@stop

@section('reject')
    @include('modules._reject', [
        'route' => route('rejectionProcess'),
        'options' => [
            'TU15' => 'Solicitado por el cliente',
            'TA01' => 'Asesor'
        ]
    ])
@stop

@section('content')

                @include('card.form._entailmentForm')


@endsection

@section('scripts')
    <script>
        anElement = new AutoNumeric('#customer_limit', {
            modifyValueOnWheel: false,
            minimumValue: 0,
            maximumValue: "{{ $customer->card_limit }}"
        });
    </script>


@stop
