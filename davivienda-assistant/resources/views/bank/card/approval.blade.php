@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #fff">
        <div class="container">
            <div class="banner">
            </div>
        </div>
    </div>
@stop

@section('reject')
    @include('modules._reject', [
        'route' => route('exitProcess'),
        'options' => [
            'TU15' => 'Solicitado por el cliente',
            'TA01' => 'Asesor'
        ]
    ])
@stop

@section('content')

            @include('card.form._approvalForm')


@endsection

@section('scripts')

@stop
