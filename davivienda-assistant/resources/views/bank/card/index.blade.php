@extends('layouts.app')

@section('meta')
    <link href="{{ asset('css/plugins/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@stop

@section('banner')
    @if(checkStep($flow, ['1']))
        @include('modules._banner', [
            'image' => asset('design/' . get_json($flow->product_information, ['product', 'code']) . '.png'),
            'title' => 'Formulario de solicitud de Tarjeta de Crédito',
            'description' => '<p>Complete el formulario de solicitud de ' . get_json($flow->product_information, ['product', 'name']) . ' con los datos del cliente<br> ID No. ' . $flow->customer->identifier .'</p>'
        ])
    @elseif(checkStep($flow, ['2']))
        @include('modules._banner', [
            'class' => 'success-banner',
            'bgColor' => '#F7981B',
            'image' => asset('design/success.png'),
            'title' => 'Su solicitud de Tarjeta de Crédito ha sido aprobada',
            'description' => '<p style="font-size: 2.5rem">Se ha aprobado una <b>' . get_json($flow->product_information, ['product', 'name']) . ' ' . get_json($flow->product_information, ['product', 'type']) . '</b> con un límite de <span style="white-space: nowrap">L ' . get_json($flow->product_information, ['product', 'limit']) . '</span> del cliente con <br> ID No. ' . $flow->customer->identifier .'</p>'
        ])
    @else
        @include('modules._banner', [
            'title' => 'Formulario de solicitud de Tarjeta de Crédito',
            'description' => '<p>Para ingresar una nueva solicitud de Tarjeta de Crédito completa la siguiente información</p>'
        ])
    @endif
@stop

@section('back')
    @if(!checkStep($flow, ['1', '2']))
        <a href="{{ route('home') }}" class="btn-back">
            Inicio
        </a>
    @endif
@stop

@section('reject')
    @if(checkStep($flow, ['1']))
        @include('modules._reject', [
            'route' => route('exit.flow', ['bank_card']),
            'options' => [
                'TA01' => 'Asesor'
            ]
        ])
    @elseif (checkStep($flow, ['2']))
        @include('modules._reject', [
            'route' => route('exit.flow', ['bank_card']),
            'options' => [
                'TU15' => 'Solicitado por el cliente',
                'TA01' => 'Asesor'
            ]
        ])
    @endif
@stop

@section('content')

    @if(checkStep($flow, '1'))
        @include('bank.card.form._first')
    @elseif(checkStep($flow, '2'))
        @include('bank.card.form._second')
    @else
        @include('bank.card.form._search')
    @endif

@endsection

@section('scripts')
    @include('partials._scripts')
    @include('bank.card.partials._scripts')
@stop