
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Components to updated product and product upload attributes
Vue.component('update-product-status', require('./components/UpdateProductStatus.vue'));
Vue.component('update-upload-status', require('./components/UpdateUploadStatus.vue'));
Vue.component('update-upload-type', require('./components/UpdateUploadType.vue'));

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
// views Bank Auto
// Vue.component('bank-auto-search', require('./views/BankAutoSearch.vue'));
// Vue.component('bank-auto-first', require('./views/BankAutoFirst.vue'));
// Vue.component('bank-auto-second', require('./views/BankAutoSecond.vue'));
// Vue.component('bank-auto-third', require('./views/BankAutoThird.vue'));
// Vue.component('bank-auto-fourth', require('./views/BankAutoFourth'));

import Toasted from 'vue-toasted';

Vue.use(Toasted, {
    position: 'bottom-right',
    duration: 4000,
    action : {
        text : 'X',
        onClick : (e, toastObject) => {
            toastObject.goAway(0);
        }
    },
});

const app = new Vue({
    el: '#app'
});
